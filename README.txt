README for IFOS3D
=================

IFOS3D (Inversion of Full observed seismograms) is a 3D elastic full-waveform 
inversion code. The inversion problem is solved by a conjugate-gradient method. 
The gradients are computed in the frequency domain for discrete frequencies 
with the adjoint method. The forward modeling is done in the time domain with 
a finite-difference code on a standard staggered grid.

Brief installation instructions can be found in the file INSTALL.

If you use this code for your own research please cite at least one article
written by the developers of the package, for instance:

Butzer, S., Kurzmann, A. & Bohlen, T., 2013. 3D elastic full-waveform inversion 
of small-scale heterogeneities in transmission geometry, Geophysical Prospecting, 
61(6), 1238-1251.

The software is licensed under the GNU General Public License version 2, see
LICENSE for details.
