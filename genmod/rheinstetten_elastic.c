/*
 *   Model defined by flnode file. 
 */

#include "fd.h"

void model(st_model *mod){

	/*--------------------------------------------------------------------------*/
	/* extern variables */

	extern int NX, NY,NZ, NXG, NYG, NZG, POS[4], MYID;
	extern float DZ;
	extern FILE *FP;
	
		/* local variables */
	float muv, piv, vp, vs, rhov;
	int i, j,k, ii, jj,kk, l;
	
	FILE *flfile;
	int nodes;
	char cline[256];
	
	float *fldepth, *flrho, *flvp, *flvs;
	
	nodes=7;
	
	fldepth=vector(1,nodes);
	flrho=vector(1,nodes);
	flvp=vector(1,nodes);
	flvs=vector(1,nodes);
		
	
	
	/*read FL nodes from File*/
	
	flfile=fopen("flnodes/flnodes.rheinstetten","r");
	if (flfile==NULL) err(" FL-file could not be opened !");
	
	
	
	for (l=1;l<=nodes;l++){
		fgets(cline,255,flfile);
		if (cline[0]!='#'){
			sscanf(cline,"%f%f%f%f",&fldepth[l], &flrho[l], &flvp[l], &flvs[l]);
		}
		else l=l-1;
	
	}
	
	
	if(MYID==0){
	fprintf(FP," ------------------------------------------------------------------ \n\n");
	fprintf(FP," Information of FL nodes: \n\n");
	fprintf(FP," \t depth \t rho \t vp \t vs \n\n");
	
	for (l=1;l<=nodes;l++){
	fprintf(FP," \t %f \t %f \t %f \t %f\n\n",fldepth[l],flrho[l],flvp[l],flvs[l]);
	}
	fprintf(FP," ------------------------------------------------------------------ \n\n");
	}
	/*-----------------------------------------------------------------------*/
	
	
		
	/*-----------------------------------------------------------------------*/

	/* loop over global grid */
		for (i=1;i<=NXG;i++){
			for (k=1; k<=NZG; k++) {
			for (l=1;l<nodes;l++){
				if (fldepth[l]==fldepth[l+1]){
					if ((i==1) && (k==1) && (MYID==0)){
					fprintf(FP,"depth: %f m: double node\n",fldepth[l]);}}
				else{
					for (j=(int)(fldepth[l]/DZ)+1;j<=(int)(fldepth[l+1]/DZ);j++){
			
						vp=0.0; vs=0.0; rhov=0.0;
				  
						vp=(DZ*(j-1)-fldepth[l])*(flvp[l+1]-flvp[l])/(fldepth[l+1]-fldepth[l])+flvp[l];
						vp=vp*1000.0;
						vs=(DZ*(j-1)-fldepth[l])*(flvs[l+1]-flvs[l])/(fldepth[l+1]-fldepth[l])+flvs[l];
						vs=vs*1000.0;
						rhov=(DZ*(j-1)-fldepth[l])*(flrho[l+1]-flrho[l])/(fldepth[l+1]-fldepth[l])+flrho[l];
						rhov=rhov*1000.0;
						
						
						muv=vs*vs*rhov;
						piv=vp*vp*rhov;

						
						/* only the PE which belongs to the current global gridpoint 
				  		is saving model parameters in his local arrays */
						if ((POS[1]==((i-1)/NX)) &&
						(POS[2]==((j-1)/NY)) &&
						(POS[3]==((k-1)/NZ))) {
						ii=i-POS[1]*NX;
						jj=j-POS[2]*NY;
						kk=k-POS[3]*NZ;

						mod->u[jj][ii][kk]=muv;
						mod->rho[jj][ii][kk]=rhov;
						mod->pi[jj][ii][kk]=piv;
						}
					}
				}
			}
			
			for (j=(int)(fldepth[nodes]/DZ)+1;j<=NYG;j++){
			  
				vp=0.0; vs=0.0; rhov=0.0;
				vp=flvp[nodes]*1000.0; vs=flvs[nodes]*1000.0; rhov=flrho[nodes]*1000.0;
				
				muv=vs*vs*rhov;
				piv=vp*vp*rhov;

				/* only the PE which belongs to the current global gridpoint 
				  		is saving model parameters in his local arrays */
					if ((POS[1]==((i-1)/NX)) &&
					        (POS[2]==((j-1)/NY)) &&
					        (POS[3]==((k-1)/NZ))) {
						ii=i-POS[1]*NX;
						jj=j-POS[2]*NY;
						kk=k-POS[3]*NZ;

						mod->u[jj][ii][kk]=muv;
						mod->rho[jj][ii][kk]=rhov;
						mod->pi[jj][ii][kk]=piv;
						}
			}
		}
		}
		
		
	
	
	free_vector(fldepth,1,nodes);
	free_vector(flrho,1,nodes);
	free_vector(flvp,1,nodes);
	free_vector(flvs,1,nodes);

}



