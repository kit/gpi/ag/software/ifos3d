/*------------------------------------------------------------------------
 * Copyright (C) 2015 For the list of authors, see file AUTHORS.
 *
 * This file is part of IFOS3D.
 *
 * IFOS3D is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 2.0 of the License only.
 *
 * IFOS3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with IFOS3D. See file COPYING and/or
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
--------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
 Copying of model parmeters into a testmodel
 S. Butzer 2013
 *  ----------------------------------------------------------------------*/

#include "fd.h"
void cpmodel(int nx, int ny, int nz, st_model *mod,st_model *testmod) {
	extern int L;
	int j,i,k,l;

	for (j=1; j<=ny; j++) {
		for (i=1; i<=nx; i++) {
			for (k=1; k<=nz; k++) {

				testmod->rho[j][i][k]=0.0;
				testmod->rho[j][i][k]=mod->rho[j][i][k];

				testmod->u[j][i][k]=0.0;
				testmod->u[j][i][k]=mod->u[j][i][k];

				testmod->pi[j][i][k]=0.0;
				testmod->pi[j][i][k]=mod->pi[j][i][k];

				if (L) {
					testmod->taup[j][i][k]=0.0;
					testmod->taup[j][i][k]=mod->taup[j][i][k];
					testmod->taus[j][i][k]=0.0;
					testmod->taus[j][i][k]=mod->taus[j][i][k];
				}

			}
		}
	}

	if (L) {
		for (l=1; l<=L; l++) {
			testmod->eta[l]=0.0;
			testmod->eta[l]=mod->eta[l];
		}
	}

}
