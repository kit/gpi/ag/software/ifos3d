/* $Id: exchange_v.c,v 1.1.1.1 2009/12/09 14:29:22 sjetschny Exp $ */
/*------------------------------------------------------------------------
 * exchange of particle velocities at grid boundaries between processors
 * when using the standard staggered grid
 *
 *  ----------------------------------------------------------------------*/

#include "fd.h"

double exchange_v(st_velocity *vel,st_buffer *buffer, MPI_Request * req_send, MPI_Request * req_rec){

	extern int NX, NY, NZ, POS[4], NPROCX, NPROCY, NPROCZ, BOUNDARY,  FDORDER,  INDEX[7];  /*MYID,LOG,*/
	extern const int TAG1,TAG2,TAG3,TAG4,TAG5,TAG6;

	MPI_Status status;	
	int i, j, k, l, n, nf1, nf2;
	double time=0.0;  /*, time1=0.0;*/

        nf1=3*FDORDER/2-1;
	nf2=nf1-1; 
	 
	/*if (LOG){
	if (MYID==0) time1=MPI_Wtime();}*/

	/* top-bottom -----------------------------------------------------------*/	

	if (POS[2]!=0)	/* no boundary exchange at top of global grid */
	for (i=1;i<=NX;i++){
		for (k=1;k<=NZ;k++){

			/* storage of top of local volume into buffer */
			n=1;
			for (l=1;l<=FDORDER/2;l++){
			buffer->top_to_bot[i][k][n++]  =  vel->vx[l][i][k];
			buffer->top_to_bot[i][k][n++]  =  vel->vz[l][i][k];
			}
			for (l=1;l<=(FDORDER/2-1);l++)
			buffer->top_to_bot[i][k][n++]  =  vel->vy[l][i][k];			
		}
	}
        


	if (POS[2]!=NPROCY-1)	/* no boundary exchange at bottom of global grid */
	for (i=1;i<=NX;i++){
		for (k=1;k<=NZ;k++){

			/* storage of bottom of local volume into buffer */
			n=1;
			for (l=1;l<=FDORDER/2;l++)
			buffer->bot_to_top[i][k][n++]  =  vel->vy[NY-l+1][i][k];

			for (l=1;l<=(FDORDER/2-1);l++){
			buffer->bot_to_top[i][k][n++]  =  vel->vx[NY-l+1][i][k];
			buffer->bot_to_top[i][k][n++]  =  vel->vz[NY-l+1][i][k];
			}
		}
	}
	
	
	/* persistent communication see comm_ini.c*/
	/*for (i=4;i<=5;i++){*/
		/* send and reveive values at edges of the local grid */
	/*	MPI_Start(&req_send[i]);
		MPI_Wait(&req_send[i],&status);
		MPI_Start(&req_rec[i]);
		MPI_Wait(&req_rec[i],&status);
	}*/


	MPI_Sendrecv_replace(&buffer->top_to_bot[1][1][1],NX*NZ*nf1,MPI_FLOAT,INDEX[3],TAG5,INDEX[4],TAG5,MPI_COMM_WORLD,&status);
	MPI_Sendrecv_replace(&buffer->bot_to_top[1][1][1],NX*NZ*nf2,MPI_FLOAT,INDEX[4],TAG6,INDEX[3],TAG6,MPI_COMM_WORLD,&status);

/*	
	MPI_Bsend(&buffer->top_to_bot[1][1][1],NX*NZ*nf1,MPI_FLOAT,INDEX[3],TAG5,MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Recv(&buffer->top_to_bot[1][1][1], NX*NZ*nf1,MPI_FLOAT,INDEX[4],TAG5,MPI_COMM_WORLD,&status);
	MPI_Bsend(&buffer->bot_to_top[1][1][1],NX*NZ*nf2,MPI_FLOAT,INDEX[4],TAG6,MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Recv(&buffer->bot_to_top[1][1][1], NX*NZ*nf2,MPI_FLOAT,INDEX[3],TAG6,MPI_COMM_WORLD,&status);			
*/          
			
	if (POS[2]!=NPROCY-1)	/* no boundary exchange at bottom of global grid */
	for (i=1;i<=NX;i++){
		for (k=1;k<=NZ;k++){

			n=1;
			for (l=1;l<=FDORDER/2;l++){
			vel->vx[NY+l][i][k] = buffer->top_to_bot[i][k][n++];
			vel->vz[NY+l][i][k] = buffer->top_to_bot[i][k][n++];
			}
			
			for (l=1;l<=(FDORDER/2-1);l++)
			vel->vy[NY+l][i][k] = buffer->top_to_bot[i][k][n++];
			
		
		}
	}
	

	if (POS[2]!=0)	/* no boundary exchange at top of global grid */
	for (i=1;i<=NX;i++){
		for (k=1;k<=NZ;k++){

			n=1;
			for (l=1;l<=FDORDER/2;l++)
			vel->vy[1-l][i][k] = buffer->bot_to_top[i][k][n++];
			
			for (l=1;l<=(FDORDER/2-1);l++){
			vel->vx[1-l][i][k] = buffer->bot_to_top[i][k][n++];
			vel->vz[1-l][i][k] = buffer->bot_to_top[i][k][n++];
			}
		}
	}

	/* left-right -----------------------------------------------------------*/	
	
	
	if ((BOUNDARY) || (POS[1]!=0))	/* no boundary exchange at left edge of global grid */
	for (j=1;j<=NY;j++){
		for (k=1;k<=NZ;k++){

			/* storage of left edge of local volume into buffer */
			n=1;
			for (l=1;l<=FDORDER/2;l++){
			buffer->lef_to_rig[j][k][n++]  =  vel->vy[j][l][k];
			buffer->lef_to_rig[j][k][n++]  =  vel->vz[j][l][k];
			}

			for (l=1;l<=(FDORDER/2-1);l++)
			buffer->lef_to_rig[j][k][n++]  =  vel->vx[j][l][k];
		}
	}


						/* no exchange if periodic boundary condition is applied */
	if ((BOUNDARY) || (POS[1]!=NPROCX-1))	/* no boundary exchange at right edge of global grid */
	for (j=1;j<=NY;j++){
		for (k=1;k<=NZ;k++){
			/* storage of right edge of local volume into buffer */
			n=1;
			for (l=1;l<=FDORDER/2;l++)
			buffer->rig_to_lef[j][k][n++] =  vel->vx[j][NX-l+1][k];

			for (l=1;l<=(FDORDER/2-1);l++){
			buffer->rig_to_lef[j][k][n++] =  vel->vy[j][NX-l+1][k];
			buffer->rig_to_lef[j][k][n++] =  vel->vz[j][NX-l+1][k];
			}
		}
	}


	/* persistent communication see comm_ini.c*/
	/*for (i=0;i<=1;i++){*/
		/* send and reveive values at edges of the local grid */
	/*	MPI_Start(&req_send[i]);
		MPI_Wait(&req_send[i],&status);
		MPI_Start(&req_rec[i]);
		MPI_Wait(&req_rec[i],&status);
	}*/
	
	
	MPI_Sendrecv_replace(&buffer->lef_to_rig[1][1][1],NY*NZ*nf1,MPI_FLOAT,INDEX[1],TAG1,INDEX[2],TAG1,MPI_COMM_WORLD,&status);
	MPI_Sendrecv_replace(&buffer->rig_to_lef[1][1][1],NY*NZ*nf2,MPI_FLOAT,INDEX[2],TAG2,INDEX[1],TAG2,MPI_COMM_WORLD,&status);
	
	
/*	MPI_Bsend(&bufferlef_to_rig[1][1][1],NY*NZ*nf1,MPI_FLOAT,INDEX[1],TAG1,MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Recv(&bufferlef_to_rig[1][1][1], NY*NZ*nf1,MPI_FLOAT,INDEX[2],TAG1,MPI_COMM_WORLD,&status);
	MPI_Bsend(&bufferrig_to_lef[1][1][1],NY*NZ*nf2,MPI_FLOAT,INDEX[2],TAG2,MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Recv(&bufferrig_to_lef[1][1][1], NY*NZ*nf2,MPI_FLOAT,INDEX[1],TAG2,MPI_COMM_WORLD,&status);			
*/

	if ((BOUNDARY) || (POS[1]!=NPROCX-1))	/* no boundary exchange at right edge of global grid */
	for (j=1;j<=NY;j++){
		for (k=1;k<=NZ;k++){

			n=1;
			for (l=1;l<=FDORDER/2;l++){
			vel->vy[j][NX+l][k] = buffer->lef_to_rig[j][k][n++];
			vel->vz[j][NX+l][k] = buffer->lef_to_rig[j][k][n++];
			}

			for (l=1;l<=(FDORDER/2-1);l++)
			vel->vx[j][NX+l][k] = buffer->lef_to_rig[j][k][n++];		
		}
	}

						/* no exchange if periodic boundary condition is applied */
	if ((BOUNDARY) || (POS[1]!=0))	/* no boundary exchange at left edge of global grid */
	for (j=1;j<=NY;j++){
		for (k=1;k<=NZ;k++){

			n=1;
			for (l=1;l<=FDORDER/2;l++)
			vel->vx[j][1-l][k] = buffer->rig_to_lef[j][k][n++];

			for (l=1;l<=(FDORDER/2-1);l++){
			vel->vy[j][1-l][k] = buffer->rig_to_lef[j][k][n++];
			vel->vz[j][1-l][k] = buffer->rig_to_lef[j][k][n++];
			}

		
		}
	}	
	
		
		/* front-back -----------------------------------------------------------*/	

	
	if ((BOUNDARY) || (POS[3]!=0))	/* no boundary exchange at front side of global grid */
	for (i=1;i<=NX;i++){
		for (j=1;j<=NY;j++){

			/* storage of front side of local volume into buffer */
			n=1;
			for (l=1;l<=FDORDER/2;l++){
			buffer->fro_to_bac[j][i][n++]  =  vel->vx[j][i][l];
			buffer->fro_to_bac[j][i][n++]  =  vel->vy[j][i][l];
			}

			for (l=1;l<=(FDORDER/2-1);l++)
			buffer->fro_to_bac[j][i][n++]  =  vel->vz[j][i][l];			
		}
	}


						/* no exchange if periodic boundary condition is applied */
	if ((BOUNDARY) || (POS[3]!=NPROCZ-1))	/* no boundary exchange at back side of global grid */
	for (i=1;i<=NX;i++){
		for (j=1;j<=NY;j++){
			
			/* storage of back side of local volume into buffer */
			n=1;
			for (l=1;l<=FDORDER/2;l++)
			buffer->bac_to_fro[j][i][n++]  =  vel->vz[j][i][NZ-l+1];
			
			for (l=1;l<=(FDORDER/2-1);l++){
			buffer->bac_to_fro[j][i][n++]  =  vel->vx[j][i][NZ-l+1];
			buffer->bac_to_fro[j][i][n++]  =  vel->vy[j][i][NZ-l+1];
			}
			
		}
	}


	/* persistent communication see comm_ini.c*/
	/*for (i=2;i<=3;i++){*/
		/* send and reveive values at edges of the local grid */
	/*	MPI_Start(&req_send[i]);
		MPI_Wait(&req_send[i],&status);
		MPI_Start(&req_rec[i]);
		MPI_Wait(&req_rec[i],&status);
	}*/
	
	MPI_Sendrecv_replace(&buffer->fro_to_bac[1][1][1],NX*NY*nf1,MPI_FLOAT,INDEX[5],TAG3,INDEX[6],TAG3,MPI_COMM_WORLD,&status);
	MPI_Sendrecv_replace(&buffer->bac_to_fro[1][1][1],NX*NY*nf2,MPI_FLOAT,INDEX[6],TAG4,INDEX[5],TAG4,MPI_COMM_WORLD,&status);

/*

	MPI_Bsend(&buffer->fro_to_bac[1][1][1],NX*NY*nf1,MPI_FLOAT,INDEX[5],TAG3,MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Recv(&buffer->fro_to_bac[1][1][1], NX*NY*nf1,MPI_FLOAT,INDEX[6],TAG3,MPI_COMM_WORLD,&status);
	MPI_Bsend(&buffer->bac_to_fro[1][1][1],NX*NY*nf2,MPI_FLOAT,INDEX[6],TAG4,MPI_COMM_WORLD);
        MPI_Barrier(MPI_COMM_WORLD);
	MPI_Recv(&buffer->bac_to_fro[1][1][1], NX*NY*nf2,MPI_FLOAT,INDEX[5],TAG4,MPI_COMM_WORLD,&status);			
*/

	/* no exchange if periodic boundary condition is applied */
	if ((BOUNDARY) || (POS[3]!=NPROCZ-1))	/* no boundary exchange at back side of global grid */
	for (i=1;i<=NX;i++){
		for (j=1;j<=NY;j++){

			n=1;
			for (l=1;l<=FDORDER/2;l++){
			vel->vx[j][i][NZ+l] = buffer->fro_to_bac[j][i][n++];
			vel->vy[j][i][NZ+l] = buffer->fro_to_bac[j][i][n++];
			}
			
			for (l=1;l<=(FDORDER/2-1);l++)
			vel->vz[j][i][NZ+l] = buffer->fro_to_bac[j][i][n++];


		}
	}


						/* no exchange if periodic boundary condition is applied */
	if ((BOUNDARY) || (POS[3]!=0))	/* no boundary exchange at front side of global grid */
	for (i=1;i<=NX;i++){
		for (j=1;j<=NY;j++){
			n=1;
			for (l=1;l<=FDORDER/2;l++)
			vel->vz[j][i][1-l] = buffer->bac_to_fro[j][i][n++];
			
			for (l=1;l<=(FDORDER/2-1);l++){
			vel->vx[j][i][1-l] = buffer->bac_to_fro[j][i][n++];
			vel->vy[j][i][1-l] = buffer->bac_to_fro[j][i][n++];
			}
		}
	}


	/*if (LOG)
	if (MYID==0){
		time2=MPI_Wtime();
		time=time2-time1;
		fprintf(FP," Real time for particle velocity exchange: \t %4.2f s.\n",time);
	}*/
	return time;

}
