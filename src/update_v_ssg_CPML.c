/*------------------------------------------------------------------------
 * Copyright (C) 2015 For the list of authors, see file AUTHORS.
 *
 * This file is part of IFOS3D.
 * 
 * IFOS3D is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 2.0 of the License only.
 * 
 * IFOS3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with IFOS3D. See file COPYING and/or 
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
--------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
 * updating velocity values in the CPML-boundaries (4th order spatial FD sheme)
 * Implementation acoording to Komatitsch, D. and Martin, R.(2007): "An unsplit convolutional perfectly matched layer improved at grazing incidence for the seismic wave equation", geophysics, Vol.72, No.5 
 * similar to fdveps (2D) 
 * S.Dunkl (November 2010)
 *  ----------------------------------------------------------------------*/

#include "fd.h"


double update_v_CPML(int nx1, int nx2, int ny1, int ny2, int nz1, int nz2,
int nt, st_velocity *vel, st_stress *stress,  st_model_av *mod_av,
float **  srcpos_loc, st_signals *signals,int nsrc, float *** absorb_coeff, int back,
st_pml_coeff *pml_coeff, st_pml_wfd *pml_wfd){

	
	extern float DT, DX, DY, DZ;
	extern int  FDCOEFF; /*MYID, LOG,*/
	extern int FREE_SURF;
	extern int NPROCX, NPROCY, NPROCZ, POS[4];
        extern int FW, NY, NZ;

	int i, j, k, h1,l;
	float b1, b2, dx, dy, dz;
	float sxx_x=0.0, sxy_y=0.0, sxz_z=0.0, syy_y=0.0, sxy_x=0.0, syz_z=0.0;
	float szz_z=0.0, sxz_x=0.0, syz_y=0.0;
	double time=0.0; /*, time1=0.0;*/
	
		dx=DT/DX;
		dy=DT/DY;
		dz=DT/DZ;
		
		
	
	/*if (LOG)
	if (MYID==0) time1=MPI_Wtime();*/

       
		b1=9.0/8.0; b2=-1.0/24.0; /* Taylor coefficients*/
		if(FDCOEFF==2){
		b1=1.1382; b2=-0.046414;} /* Holberg coefficients E=0.1 %*/

		/* boundaries in x-direction */

	if (POS[1]==0){
		for (j=1;j<=NY;j++){
			for (i=1;i<=FW;i++){
				for (k=1;k<=NZ;k++){
				
				sxx_x = dx*(b1*(stress->sxx[j][i+1][k]-stress->sxx[j][i][k])+b2*(stress->sxx[j][i+2][k]-stress->sxx[j][i-1][k]));
				sxy_x = dx*(b1*(stress->sxy[j][i][k]-stress->sxy[j][i-1][k])+b2*(stress->sxy[j][i+1][k]-stress->sxy[j][i-2][k]));
				sxz_x = dx*(b1*(stress->sxz[j][i][k]-stress->sxz[j][i-1][k])+b2*(stress->sxz[j][i+1][k]-stress->sxz[j][i-2][k]));
				sxy_y = dy*(b1*(stress->sxy[j][i][k]-stress->sxy[j-1][i][k])+b2*(stress->sxy[j+1][i][k]-stress->sxy[j-2][i][k]));
				syy_y = dy*(b1*(stress->syy[j+1][i][k]-stress->syy[j][i][k])+b2*(stress->syy[j+2][i][k]-stress->syy[j-1][i][k]));
				syz_y = dy*(b1*(stress->syz[j][i][k]-stress->syz[j-1][i][k])+b2*(stress->syz[j+1][i][k]-stress->syz[j-2][i][k]));
				sxz_z = dz*(b1*(stress->sxz[j][i][k]-stress->sxz[j][i][k-1])+b2*(stress->sxz[j][i][k+1]-stress->sxz[j][i][k-2]));
				syz_z = dz*(b1*(stress->syz[j][i][k]-stress->syz[j][i][k-1])+b2*(stress->syz[j][i][k+1]-stress->syz[j][i][k-2]));
				szz_z = dz*(b1*(stress->szz[j][i][k+1]-stress->szz[j][i][k])+b2*(stress->szz[j][i][k+2]-stress->szz[j][i][k-1]));


				pml_wfd->psi_sxx_x[j][i][k] = pml_coeff->b_x_half[i] * pml_wfd->psi_sxx_x[j][i][k] + pml_coeff->a_x_half[i] * sxx_x;
                           	sxx_x = sxx_x / pml_coeff->K_x_half[i] + pml_wfd->psi_sxx_x[j][i][k];
				pml_wfd->psi_sxy_x[j][i][k] = pml_coeff->b_x[i] * pml_wfd->psi_sxy_x[j][i][k] + pml_coeff->a_x[i] * sxy_x;
                           	sxy_x = sxy_x / pml_coeff->K_x[i] + pml_wfd->psi_sxy_x[j][i][k];
				pml_wfd->psi_sxz_x[j][i][k] = pml_coeff->b_x[i] * pml_wfd->psi_sxz_x[j][i][k] + pml_coeff->a_x[i] * sxz_x;
                           	sxz_x = sxz_x / pml_coeff->K_x[i] + pml_wfd->psi_sxz_x[j][i][k];

			if((POS[2]==0 && FREE_SURF==0) && (j<=FW)){

				pml_wfd->psi_sxy_y[j][i][k] = pml_coeff->b_y[j] * pml_wfd->psi_sxy_y[j][i][k] + pml_coeff->a_y[j] * sxy_y;
                           	sxy_y = sxy_y / pml_coeff->K_y[j] + pml_wfd->psi_sxy_y[j][i][k]; 
		           	pml_wfd->psi_syy_y[j][i][k] = pml_coeff->b_y_half[j] * pml_wfd->psi_syy_y[j][i][k] + pml_coeff->a_y_half[j] * syy_y;
                           	syy_y = syy_y / pml_coeff->K_y_half[j] + pml_wfd->psi_syy_y[j][i][k]; 
                           	pml_wfd->psi_syz_y[j][i][k] = pml_coeff->b_y[j] * pml_wfd->psi_syz_y[j][i][k] + pml_coeff->a_y[j] * syz_y;
                           	syz_y = syz_y / pml_coeff->K_y[j] + pml_wfd->psi_syz_y[j][i][k]; 	 }


			if((POS[2]==NPROCY-1) && (j>=ny2+1)){
			h1 = (j-ny2+FW); 
  				pml_wfd->psi_sxy_y[h1][i][k] = pml_coeff->b_y[h1] * pml_wfd->psi_sxy_y[h1][i][k] + pml_coeff->a_y[h1] * sxy_y; 
                           	sxy_y = sxy_y / pml_coeff->K_y[h1] + pml_wfd->psi_sxy_y[h1][i][k]; 
                          	pml_wfd->psi_syy_y[h1][i][k] = pml_coeff->b_y_half[h1] * pml_wfd->psi_syy_y[h1][i][k] + pml_coeff->a_y_half[h1] * syy_y; 
			   	syy_y = syy_y / pml_coeff->K_y_half[h1] + pml_wfd->psi_syy_y[h1][i][k];
				pml_wfd->psi_syz_y[h1][i][k] = pml_coeff->b_y[h1] * pml_wfd->psi_syz_y[h1][i][k] + pml_coeff->a_y[h1] * syz_y;
                           	syz_y = syz_y / pml_coeff->K_y[h1] + pml_wfd->psi_syz_y[h1][i][k];
                	 }


			if((POS[3]==0) && (k<=FW)){
				pml_wfd->psi_sxz_z[j][i][k] = pml_coeff->b_z[k] * pml_wfd->psi_sxz_z[j][i][k] + pml_coeff->a_z[k] * sxz_z;
                           	sxz_z = sxz_z / pml_coeff->K_z[k] + pml_wfd->psi_sxz_z[j][i][k];
				pml_wfd->psi_syz_z[j][i][k] = pml_coeff->b_z[k] * pml_wfd->psi_syz_z[j][i][k] + pml_coeff->a_z[k] * syz_z;
                           	syz_z = syz_z / pml_coeff->K_z[k] + pml_wfd->psi_syz_z[j][i][k];
				pml_wfd->psi_szz_z[j][i][k] = pml_coeff->b_z_half[k] * pml_wfd->psi_szz_z[j][i][k] + pml_coeff->a_z_half[k] * szz_z;
                           	szz_z = szz_z / pml_coeff->K_z_half[k] + pml_wfd->psi_szz_z[j][i][k];}


			if((POS[3]==NPROCZ-1) && (k>=nz2+1)){
			h1 = (k-nz2+FW); 
				pml_wfd->psi_sxz_z[j][i][h1] = pml_coeff->b_z[h1] * pml_wfd->psi_sxz_z[j][i][h1] + pml_coeff->a_z[h1] * sxz_z; 
			  	sxz_z = sxz_z / pml_coeff->K_z[h1] + pml_wfd->psi_sxz_z[j][i][h1];
                           	pml_wfd->psi_syz_z[j][i][h1] = pml_coeff->b_z[h1] * pml_wfd->psi_syz_z[j][i][h1] + pml_coeff->a_z[h1] * syz_z;
                           	syz_z = syz_z / pml_coeff->K_z[h1] + pml_wfd->psi_syz_z[j][i][h1];
				pml_wfd->psi_szz_z[j][i][h1] = pml_coeff->b_z_half[h1] * pml_wfd->psi_szz_z[j][i][h1] + pml_coeff->a_z_half[h1] * szz_z;
                           	szz_z = szz_z / pml_coeff->K_z_half[h1] + pml_wfd->psi_szz_z[j][i][h1];}

 
				vel->vx[j][i][k]+= (sxx_x + sxy_y + sxz_z)/mod_av->rip[j][i][k];
				vel->vy[j][i][k]+= (syy_y + sxy_x + syz_z)/mod_av->rjp[j][i][k];
				vel->vz[j][i][k]+= (szz_z + sxz_x + syz_y)/mod_av->rkp[j][i][k];

				}
			}
		}
	}

	if(POS[1]==NPROCX-1){
		for (j=1;j<=NY;j++){
			for (i=nx2+1;i<=nx2+FW;i++){
				for (k=1;k<=NZ;k++){
								
				sxx_x = dx*(b1*(stress->sxx[j][i+1][k]-stress->sxx[j][i][k])+b2*(stress->sxx[j][i+2][k]-stress->sxx[j][i-1][k]));
				sxy_x = dx*(b1*(stress->sxy[j][i][k]-stress->sxy[j][i-1][k])+b2*(stress->sxy[j][i+1][k]-stress->sxy[j][i-2][k]));
				sxz_x = dx*(b1*(stress->sxz[j][i][k]-stress->sxz[j][i-1][k])+b2*(stress->sxz[j][i+1][k]-stress->sxz[j][i-2][k]));
				sxy_y = dy*(b1*(stress->sxy[j][i][k]-stress->sxy[j-1][i][k])+b2*(stress->sxy[j+1][i][k]-stress->sxy[j-2][i][k]));
				syy_y = dy*(b1*(stress->syy[j+1][i][k]-stress->syy[j][i][k])+b2*(stress->syy[j+2][i][k]-stress->syy[j-1][i][k]));
				syz_y = dy*(b1*(stress->syz[j][i][k]-stress->syz[j-1][i][k])+b2*(stress->syz[j+1][i][k]-stress->syz[j-2][i][k]));
				sxz_z = dz*(b1*(stress->sxz[j][i][k]-stress->sxz[j][i][k-1])+b2*(stress->sxz[j][i][k+1]-stress->sxz[j][i][k-2]));
				syz_z = dz*(b1*(stress->syz[j][i][k]-stress->syz[j][i][k-1])+b2*(stress->syz[j][i][k+1]-stress->syz[j][i][k-2]));
				szz_z = dz*(b1*(stress->szz[j][i][k+1]-stress->szz[j][i][k])+b2*(stress->szz[j][i][k+2]-stress->szz[j][i][k-1]));

				h1 = (i-nx2+FW);

                           	pml_wfd->psi_sxx_x[j][h1][k] = pml_coeff->b_x_half[h1] * pml_wfd->psi_sxx_x[j][h1][k] + pml_coeff->a_x_half[h1] * sxx_x; 
			  	sxx_x = sxx_x / pml_coeff->K_x_half[h1] + pml_wfd->psi_sxx_x[j][h1][k];
                           	pml_wfd->psi_sxy_x[j][h1][k] = pml_coeff->b_x[h1] * pml_wfd->psi_sxy_x[j][h1][k] + pml_coeff->a_x[h1] * sxy_x;
                           	sxy_x = sxy_x / pml_coeff->K_x[h1] + pml_wfd->psi_sxy_x[j][h1][k];
				pml_wfd->psi_sxz_x[j][h1][k] = pml_coeff->b_x[h1] * pml_wfd->psi_sxz_x[j][h1][k] + pml_coeff->a_x[h1] * sxz_x;
                           	sxz_x = sxz_x / pml_coeff->K_x[h1] + pml_wfd->psi_sxz_x[j][h1][k];



			if((POS[2]==0 && FREE_SURF==0) && (j<=FW)){
				pml_wfd->psi_sxy_y[j][i][k] = pml_coeff->b_y[j] * pml_wfd->psi_sxy_y[j][i][k] + pml_coeff->a_y[j] * sxy_y;
                           	sxy_y = sxy_y / pml_coeff->K_y[j] + pml_wfd->psi_sxy_y[j][i][k]; 
		           	pml_wfd->psi_syy_y[j][i][k] = pml_coeff->b_y_half[j] * pml_wfd->psi_syy_y[j][i][k] + pml_coeff->a_y_half[j] * syy_y;
                           	syy_y = syy_y / pml_coeff->K_y_half[j] + pml_wfd->psi_syy_y[j][i][k]; 
                           	pml_wfd->psi_syz_y[j][i][k] = pml_coeff->b_y[j] * pml_wfd->psi_syz_y[j][i][k] + pml_coeff->a_y[j] * syz_y;
                           	syz_y = syz_y / pml_coeff->K_y[j] + pml_wfd->psi_syz_y[j][i][k]; 	 }


			if((POS[2]==NPROCY-1) && (j>=ny2+1)){
			h1 = (j-ny2+FW); 
  				pml_wfd->psi_sxy_y[h1][i][k] = pml_coeff->b_y[h1] * pml_wfd->psi_sxy_y[h1][i][k] + pml_coeff->a_y[h1] * sxy_y; 
                           	sxy_y = sxy_y / pml_coeff->K_y[h1] + pml_wfd->psi_sxy_y[h1][i][k]; 
                          	pml_wfd->psi_syy_y[h1][i][k] = pml_coeff->b_y_half[h1] * pml_wfd->psi_syy_y[h1][i][k] + pml_coeff->a_y_half[h1] * syy_y; 
			   	syy_y = syy_y / pml_coeff->K_y_half[h1] + pml_wfd->psi_syy_y[h1][i][k];
				pml_wfd->psi_syz_y[h1][i][k] = pml_coeff->b_y[h1] * pml_wfd->psi_syz_y[h1][i][k] + pml_coeff->a_y[h1] * syz_y;
                           	syz_y = syz_y / pml_coeff->K_y[h1] + pml_wfd->psi_syz_y[h1][i][k];
                	 }


			if((POS[3]==0) && (k<=FW)){
				pml_wfd->psi_sxz_z[j][i][k] = pml_coeff->b_z[k] * pml_wfd->psi_sxz_z[j][i][k] + pml_coeff->a_z[k] * sxz_z;
                           	sxz_z = sxz_z / pml_coeff->K_z[k] + pml_wfd->psi_sxz_z[j][i][k];
				pml_wfd->psi_syz_z[j][i][k] = pml_coeff->b_z[k] * pml_wfd->psi_syz_z[j][i][k] + pml_coeff->a_z[k] * syz_z;
                           	syz_z = syz_z / pml_coeff->K_z[k] + pml_wfd->psi_syz_z[j][i][k];
				pml_wfd->psi_szz_z[j][i][k] = pml_coeff->b_z_half[k] * pml_wfd->psi_szz_z[j][i][k] + pml_coeff->a_z_half[k] * szz_z;
                           	szz_z = szz_z / pml_coeff->K_z_half[k] + pml_wfd->psi_szz_z[j][i][k];}


			if((POS[3]==NPROCZ-1) && (k>=nz2+1)){
			h1 = (k-nz2+FW); 
				pml_wfd->psi_sxz_z[j][i][h1] = pml_coeff->b_z[h1] * pml_wfd->psi_sxz_z[j][i][h1] + pml_coeff->a_z[h1] * sxz_z; 
			  	sxz_z = sxz_z / pml_coeff->K_z[h1] + pml_wfd->psi_sxz_z[j][i][h1];
                           	pml_wfd->psi_syz_z[j][i][h1] = pml_coeff->b_z[h1] * pml_wfd->psi_syz_z[j][i][h1] + pml_coeff->a_z[h1] * syz_z;
                           	syz_z = syz_z / pml_coeff->K_z[h1] + pml_wfd->psi_syz_z[j][i][h1];
				pml_wfd->psi_szz_z[j][i][h1] = pml_coeff->b_z_half[h1] * pml_wfd->psi_szz_z[j][i][h1] + pml_coeff->a_z_half[h1] * szz_z;
                           	szz_z = szz_z / pml_coeff->K_z_half[h1] + pml_wfd->psi_szz_z[j][i][h1];}

				vel->vx[j][i][k]+= (sxx_x + sxy_y + sxz_z)/mod_av->rip[j][i][k];
				vel->vy[j][i][k]+= (syy_y + sxy_x + syz_z)/mod_av->rjp[j][i][k];
				vel->vz[j][i][k]+= (szz_z + sxz_x + syz_y)/mod_av->rkp[j][i][k];

				}
			}
		}
	}

	if((POS[2]==0 && FREE_SURF==0)){
		for (j=1;j<=FW;j++){
			for (i=nx1;i<=nx2;i++){
				for (k=1;k<=NZ;k++){
												
				sxx_x = dx*(b1*(stress->sxx[j][i+1][k]-stress->sxx[j][i][k])+b2*(stress->sxx[j][i+2][k]-stress->sxx[j][i-1][k]));
				sxy_x = dx*(b1*(stress->sxy[j][i][k]-stress->sxy[j][i-1][k])+b2*(stress->sxy[j][i+1][k]-stress->sxy[j][i-2][k]));
				sxz_x = dx*(b1*(stress->sxz[j][i][k]-stress->sxz[j][i-1][k])+b2*(stress->sxz[j][i+1][k]-stress->sxz[j][i-2][k]));
				sxy_y = dy*(b1*(stress->sxy[j][i][k]-stress->sxy[j-1][i][k])+b2*(stress->sxy[j+1][i][k]-stress->sxy[j-2][i][k]));
				syy_y = dy*(b1*(stress->syy[j+1][i][k]-stress->syy[j][i][k])+b2*(stress->syy[j+2][i][k]-stress->syy[j-1][i][k]));
				syz_y = dy*(b1*(stress->syz[j][i][k]-stress->syz[j-1][i][k])+b2*(stress->syz[j+1][i][k]-stress->syz[j-2][i][k]));
				sxz_z = dz*(b1*(stress->sxz[j][i][k]-stress->sxz[j][i][k-1])+b2*(stress->sxz[j][i][k+1]-stress->sxz[j][i][k-2]));
				syz_z = dz*(b1*(stress->syz[j][i][k]-stress->syz[j][i][k-1])+b2*(stress->syz[j][i][k+1]-stress->syz[j][i][k-2]));
				szz_z = dz*(b1*(stress->szz[j][i][k+1]-stress->szz[j][i][k])+b2*(stress->szz[j][i][k+2]-stress->szz[j][i][k-1]));
								
				pml_wfd->psi_sxy_y[j][i][k] = pml_coeff->b_y[j] * pml_wfd->psi_sxy_y[j][i][k] + pml_coeff->a_y[j] * sxy_y;
                           	sxy_y = sxy_y / pml_coeff->K_y[j] + pml_wfd->psi_sxy_y[j][i][k]; 
	
		           	pml_wfd->psi_syy_y[j][i][k] = pml_coeff->b_y_half[j] * pml_wfd->psi_syy_y[j][i][k] + pml_coeff->a_y_half[j] * syy_y;
                           	syy_y = syy_y / pml_coeff->K_y_half[j] + pml_wfd->psi_syy_y[j][i][k]; 

                           	pml_wfd->psi_syz_y[j][i][k] = pml_coeff->b_y[j] * pml_wfd->psi_syz_y[j][i][k] + pml_coeff->a_y[j] * syz_y;
                           	syz_y = syz_y / pml_coeff->K_y[j] + pml_wfd->psi_syz_y[j][i][k]; 

			if((POS[3]==0) && (k<=FW)){
				pml_wfd->psi_sxz_z[j][i][k] = pml_coeff->b_z[k] * pml_wfd->psi_sxz_z[j][i][k] + pml_coeff->a_z[k] * sxz_z;
                           	sxz_z = sxz_z / pml_coeff->K_z[k] + pml_wfd->psi_sxz_z[j][i][k];
				pml_wfd->psi_syz_z[j][i][k] = pml_coeff->b_z[k] * pml_wfd->psi_syz_z[j][i][k] + pml_coeff->a_z[k] * syz_z;
                           	syz_z = syz_z / pml_coeff->K_z[k] + pml_wfd->psi_syz_z[j][i][k];
				pml_wfd->psi_szz_z[j][i][k] = pml_coeff->b_z_half[k] * pml_wfd->psi_szz_z[j][i][k] + pml_coeff->a_z_half[k] * szz_z;
                           	szz_z = szz_z / pml_coeff->K_z_half[k] + pml_wfd->psi_szz_z[j][i][k];}


			if((POS[3]==NPROCZ-1) && (k>=nz2+1)){
			h1 = (k-nz2+FW); 
				pml_wfd->psi_sxz_z[j][i][h1] = pml_coeff->b_z[h1] * pml_wfd->psi_sxz_z[j][i][h1] + pml_coeff->a_z[h1] * sxz_z; 
			  	sxz_z = sxz_z / pml_coeff->K_z[h1] + pml_wfd->psi_sxz_z[j][i][h1];
                           	pml_wfd->psi_syz_z[j][i][h1] = pml_coeff->b_z[h1] * pml_wfd->psi_syz_z[j][i][h1] + pml_coeff->a_z[h1] * syz_z;
                           	syz_z = syz_z / pml_coeff->K_z[h1] + pml_wfd->psi_syz_z[j][i][h1];
				pml_wfd->psi_szz_z[j][i][h1] = pml_coeff->b_z_half[h1] * pml_wfd->psi_szz_z[j][i][h1] + pml_coeff->a_z_half[h1] * szz_z;
                           	szz_z = szz_z / pml_coeff->K_z_half[h1] + pml_wfd->psi_szz_z[j][i][h1];}


				vel->vx[j][i][k]+= (sxx_x + sxy_y +sxz_z)/mod_av->rip[j][i][k];
				vel->vy[j][i][k]+= (syy_y + sxy_x + syz_z)/mod_av->rjp[j][i][k];
				vel->vz[j][i][k]+= (szz_z + sxz_x + syz_y)/mod_av->rkp[j][i][k];

				}
			}
		}
	}

	if(POS[2]==NPROCY-1){
	for (j=ny2+1;j<=ny2+FW;j++){
		for (i=nx1;i<=nx2;i++){
			for (k=1;k<=NZ;k++){
				
				sxx_x = dx*(b1*(stress->sxx[j][i+1][k]-stress->sxx[j][i][k])+b2*(stress->sxx[j][i+2][k]-stress->sxx[j][i-1][k]));
				sxy_x = dx*(b1*(stress->sxy[j][i][k]-stress->sxy[j][i-1][k])+b2*(stress->sxy[j][i+1][k]-stress->sxy[j][i-2][k]));
				sxz_x = dx*(b1*(stress->sxz[j][i][k]-stress->sxz[j][i-1][k])+b2*(stress->sxz[j][i+1][k]-stress->sxz[j][i-2][k]));
				sxy_y = dy*(b1*(stress->sxy[j][i][k]-stress->sxy[j-1][i][k])+b2*(stress->sxy[j+1][i][k]-stress->sxy[j-2][i][k]));
				syy_y = dy*(b1*(stress->syy[j+1][i][k]-stress->syy[j][i][k])+b2*(stress->syy[j+2][i][k]-stress->syy[j-1][i][k]));
				syz_y = dy*(b1*(stress->syz[j][i][k]-stress->syz[j-1][i][k])+b2*(stress->syz[j+1][i][k]-stress->syz[j-2][i][k]));
				sxz_z = dz*(b1*(stress->sxz[j][i][k]-stress->sxz[j][i][k-1])+b2*(stress->sxz[j][i][k+1]-stress->sxz[j][i][k-2]));
				syz_z = dz*(b1*(stress->syz[j][i][k]-stress->syz[j][i][k-1])+b2*(stress->syz[j][i][k+1]-stress->syz[j][i][k-2]));
				szz_z = dz*(b1*(stress->szz[j][i][k+1]-stress->szz[j][i][k])+b2*(stress->szz[j][i][k+2]-stress->szz[j][i][k-1]));
				
				h1 = (j-ny2+FW);
		           			
                          	pml_wfd->psi_sxy_y[h1][i][k] = pml_coeff->b_y[h1] * pml_wfd->psi_sxy_y[h1][i][k] + pml_coeff->a_y[h1] * sxy_y; 
                           	sxy_y = sxy_y / pml_coeff->K_y[h1] + pml_wfd->psi_sxy_y[h1][i][k]; 
                          	pml_wfd->psi_syy_y[h1][i][k] = pml_coeff->b_y_half[h1] * pml_wfd->psi_syy_y[h1][i][k] + pml_coeff->a_y_half[h1] * syy_y; 
			   	syy_y = syy_y / pml_coeff->K_y_half[h1] + pml_wfd->psi_syy_y[h1][i][k];
				pml_wfd->psi_syz_y[h1][i][k] = pml_coeff->b_y[h1] * pml_wfd->psi_syz_y[h1][i][k] + pml_coeff->a_y[h1] * syz_y;
                           	syz_y = syz_y / pml_coeff->K_y[h1] + pml_wfd->psi_syz_y[h1][i][k];

			if((POS[3]==0) && (k<=FW)){
				pml_wfd->psi_sxz_z[j][i][k] = pml_coeff->b_z[k] * pml_wfd->psi_sxz_z[j][i][k] + pml_coeff->a_z[k] * sxz_z;
                           	sxz_z = sxz_z / pml_coeff->K_z[k] + pml_wfd->psi_sxz_z[j][i][k];
				pml_wfd->psi_syz_z[j][i][k] = pml_coeff->b_z[k] * pml_wfd->psi_syz_z[j][i][k] + pml_coeff->a_z[k] * syz_z;
                           	syz_z = syz_z / pml_coeff->K_z[k] + pml_wfd->psi_syz_z[j][i][k];
				pml_wfd->psi_szz_z[j][i][k] = pml_coeff->b_z_half[k] * pml_wfd->psi_szz_z[j][i][k] + pml_coeff->a_z_half[k] * szz_z;
                           	szz_z = szz_z / pml_coeff->K_z_half[k] + pml_wfd->psi_szz_z[j][i][k];}


			if((POS[3]==NPROCZ-1) && (k>=nz2+1)){
			h1 = (k-nz2+FW); 
				pml_wfd->psi_sxz_z[j][i][h1] = pml_coeff->b_z[h1] * pml_wfd->psi_sxz_z[j][i][h1] + pml_coeff->a_z[h1] * sxz_z; 
			  	sxz_z = sxz_z / pml_coeff->K_z[h1] + pml_wfd->psi_sxz_z[j][i][h1];
                           	pml_wfd->psi_syz_z[j][i][h1] = pml_coeff->b_z[h1] * pml_wfd->psi_syz_z[j][i][h1] + pml_coeff->a_z[h1] * syz_z;
                           	syz_z = syz_z / pml_coeff->K_z[h1] + pml_wfd->psi_syz_z[j][i][h1];
				pml_wfd->psi_szz_z[j][i][h1] = pml_coeff->b_z_half[h1] * pml_wfd->psi_szz_z[j][i][h1] + pml_coeff->a_z_half[h1] * szz_z;
                           	szz_z = szz_z / pml_coeff->K_z_half[h1] + pml_wfd->psi_szz_z[j][i][h1];}

				vel->vx[j][i][k]+= (sxx_x + sxy_y +sxz_z)/mod_av->rip[j][i][k];
				vel->vy[j][i][k]+= (syy_y + sxy_x + syz_z)/mod_av->rjp[j][i][k];
				vel->vz[j][i][k]+= (szz_z + sxz_x + syz_y)/mod_av->rkp[j][i][k];
				
				} 
			}
		}
	}

		/* boundaries in z-direction */

	if(POS[3]==0){
		for (j=ny1;j<=ny2;j++){
			for (i=nx1;i<=nx2;i++){
				for (k=1;k<=FW;k++){

				sxx_x = dx*(b1*(stress->sxx[j][i+1][k]-stress->sxx[j][i][k])+b2*(stress->sxx[j][i+2][k]-stress->sxx[j][i-1][k]));
				sxy_x = dx*(b1*(stress->sxy[j][i][k]-stress->sxy[j][i-1][k])+b2*(stress->sxy[j][i+1][k]-stress->sxy[j][i-2][k]));
				sxz_x = dx*(b1*(stress->sxz[j][i][k]-stress->sxz[j][i-1][k])+b2*(stress->sxz[j][i+1][k]-stress->sxz[j][i-2][k]));
				sxy_y = dy*(b1*(stress->sxy[j][i][k]-stress->sxy[j-1][i][k])+b2*(stress->sxy[j+1][i][k]-stress->sxy[j-2][i][k]));
				syy_y = dy*(b1*(stress->syy[j+1][i][k]-stress->syy[j][i][k])+b2*(stress->syy[j+2][i][k]-stress->syy[j-1][i][k]));
				syz_y = dy*(b1*(stress->syz[j][i][k]-stress->syz[j-1][i][k])+b2*(stress->syz[j+1][i][k]-stress->syz[j-2][i][k]));
				sxz_z = dz*(b1*(stress->sxz[j][i][k]-stress->sxz[j][i][k-1])+b2*(stress->sxz[j][i][k+1]-stress->sxz[j][i][k-2]));
				syz_z = dz*(b1*(stress->syz[j][i][k]-stress->syz[j][i][k-1])+b2*(stress->syz[j][i][k+1]-stress->syz[j][i][k-2]));
				szz_z = dz*(b1*(stress->szz[j][i][k+1]-stress->szz[j][i][k])+b2*(stress->szz[j][i][k+2]-stress->szz[j][i][k-1]));
				

				pml_wfd->psi_sxz_z[j][i][k] = pml_coeff->b_z[k] * pml_wfd->psi_sxz_z[j][i][k] + pml_coeff->a_z[k] * sxz_z;
                           	sxz_z = sxz_z / pml_coeff->K_z[k] + pml_wfd->psi_sxz_z[j][i][k];
				pml_wfd->psi_syz_z[j][i][k] = pml_coeff->b_z[k] * pml_wfd->psi_syz_z[j][i][k] + pml_coeff->a_z[k] * syz_z;
                           	syz_z = syz_z / pml_coeff->K_z[k] + pml_wfd->psi_syz_z[j][i][k];
				pml_wfd->psi_szz_z[j][i][k] = pml_coeff->b_z_half[k] * pml_wfd->psi_szz_z[j][i][k] + pml_coeff->a_z_half[k] * szz_z;
                           	szz_z = szz_z / pml_coeff->K_z_half[k] + pml_wfd->psi_szz_z[j][i][k];

				vel->vx[j][i][k]+= (sxx_x + sxy_y +sxz_z)/mod_av->rip[j][i][k];
				vel->vy[j][i][k]+= (syy_y + sxy_x + syz_z)/mod_av->rjp[j][i][k];
				vel->vz[j][i][k]+= (szz_z + sxz_x + syz_y)/mod_av->rkp[j][i][k];
				
				}
			}
		}
	}

	if(POS[3]==NPROCZ-1){
		for (j=ny1;j<=ny2;j++){
			for (i=nx1;i<=nx2;i++){
				for (k=nz2+1;k<=nz2+FW;k++){
				sxx_x = dx*(b1*(stress->sxx[j][i+1][k]-stress->sxx[j][i][k])+b2*(stress->sxx[j][i+2][k]-stress->sxx[j][i-1][k]));
				sxy_x = dx*(b1*(stress->sxy[j][i][k]-stress->sxy[j][i-1][k])+b2*(stress->sxy[j][i+1][k]-stress->sxy[j][i-2][k]));
				sxz_x = dx*(b1*(stress->sxz[j][i][k]-stress->sxz[j][i-1][k])+b2*(stress->sxz[j][i+1][k]-stress->sxz[j][i-2][k]));
				sxy_y = dy*(b1*(stress->sxy[j][i][k]-stress->sxy[j-1][i][k])+b2*(stress->sxy[j+1][i][k]-stress->sxy[j-2][i][k]));
				syy_y = dy*(b1*(stress->syy[j+1][i][k]-stress->syy[j][i][k])+b2*(stress->syy[j+2][i][k]-stress->syy[j-1][i][k]));
				syz_y = dy*(b1*(stress->syz[j][i][k]-stress->syz[j-1][i][k])+b2*(stress->syz[j+1][i][k]-stress->syz[j-2][i][k]));
				sxz_z = dz*(b1*(stress->sxz[j][i][k]-stress->sxz[j][i][k-1])+b2*(stress->sxz[j][i][k+1]-stress->sxz[j][i][k-2]));
				syz_z = dz*(b1*(stress->syz[j][i][k]-stress->syz[j][i][k-1])+b2*(stress->syz[j][i][k+1]-stress->syz[j][i][k-2]));
				szz_z = dz*(b1*(stress->szz[j][i][k+1]-stress->szz[j][i][k])+b2*(stress->szz[j][i][k+2]-stress->szz[j][i][k-1]));
				
        			h1 = (k-nz2+FW);

                           	pml_wfd->psi_sxz_z[j][i][h1] = pml_coeff->b_z[h1] * pml_wfd->psi_sxz_z[j][i][h1] + pml_coeff->a_z[h1] * sxz_z; 
			  	sxz_z = sxz_z / pml_coeff->K_z[h1] + pml_wfd->psi_sxz_z[j][i][h1];
                           	pml_wfd->psi_syz_z[j][i][h1] = pml_coeff->b_z[h1] * pml_wfd->psi_syz_z[j][i][h1] + pml_coeff->a_z[h1] * syz_z;
                           	syz_z = syz_z / pml_coeff->K_z[h1] + pml_wfd->psi_syz_z[j][i][h1];
				pml_wfd->psi_szz_z[j][i][h1] = pml_coeff->b_z_half[h1] * pml_wfd->psi_szz_z[j][i][h1] + pml_coeff->a_z_half[h1] * szz_z;
                           	szz_z = szz_z / pml_coeff->K_z_half[h1] + pml_wfd->psi_szz_z[j][i][h1];

				vel->vx[j][i][k]+= (sxx_x + sxy_y +sxz_z)/mod_av->rip[j][i][k];
				vel->vy[j][i][k]+= (syy_y + sxy_x + syz_z)/mod_av->rjp[j][i][k];
				vel->vz[j][i][k]+= (szz_z + sxz_x + syz_y)/mod_av->rkp[j][i][k];
	
                           	}
         		}
         	}	
	}

	if (back==1){
	  for (l=1;l<=nsrc;l++) {
		i=(int)srcpos_loc[1][l];
		j=(int)srcpos_loc[2][l];
		k=(int)srcpos_loc[3][l];
		vel->vx[j][i][k]+=signals->sectionvxdiff[l][nt];
		vel->vz[j][i][k]+=signals->sectionvzdiff[l][nt];
		vel->vy[j][i][k]+=signals->sectionvydiff[l][nt];
	  }
	}

        /*if (LOG)
	if (MYID==0){
		time2=MPI_Wtime();
		time=time2-time1;
		fprintf(FP," Real time for CPML particle velocity update: \t %4.2f s.\n",time);
	}*/
	return time;

}
