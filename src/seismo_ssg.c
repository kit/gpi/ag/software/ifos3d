/*------------------------------------------------------------------------
 * Copyright (C) 2015 For the list of authors, see file AUTHORS.
 *
 * This file is part of IFOS3D.
 * 
 * IFOS3D is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 2.0 of the License only.
 * 
 * IFOS3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with IFOS3D. See file COPYING and/or 
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
--------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
 *   store amplitudes (particle velocities or pressure) at receiver positions
     in arrays
 *  ----------------------------------------------------------------------*/

#include "fd.h"

void seismo(int lsamp, int ntr, int **recpos, st_seismogram *section,
st_velocity *vel, st_stress *stress, st_model *mod){ 
		
	
	extern int SEISMO; 
	int i, j, k, itr, ins, nxrec, nyrec, nzrec;
	float amp, dh24x, dh24y, dh24z, vyx, vxy, vxx, vyy, vzx, vyz, vxz, vzy, vzz;
	extern float DX, DY, DZ;


	ins=lsamp; /* changed from "ins=lsamp/NDT;" (neccessary after correction of the buggy ns in ifos.c) */
	dh24x=1.0/DX;
	dh24y=1.0/DY;
	dh24z=1.0/DZ;
	
	for (itr=1;itr<=ntr;itr++){
		nxrec=recpos[1][itr];
		nyrec=recpos[2][itr];
		nzrec=recpos[3][itr];
		section->vx[itr][ins]=0.0;
		section->vy[itr][ins]=0.0;
		section->vz[itr][ins]=0.0;
		switch (SEISMO){                          
		case 1 : section->vx[itr][ins]=vel->vx[nyrec][nxrec][nzrec];
			 section->vy[itr][ins]=vel->vy[nyrec][nxrec][nzrec];
			 section->vz[itr][ins]=vel->vz[nyrec][nxrec][nzrec]; 
			 break;
		case 2 : section->p[itr][ins]=-stress->sxx[nyrec][nxrec][nzrec]
					-stress->syy[nyrec][nxrec][nzrec]
					-stress->szz[nyrec][nxrec][nzrec];
			 break;
		case 3 :				
			i=nxrec; j=nyrec; k=nzrec;
			/*vxy=(-vel->vx[j+2][i][k]+27.0*(vel->vx[j+1][i][k]-vel->vx[j][i][k])+vel->vx[j-1][i][k])*(24.0*DY);
			vxz=(-vel->vx[j][i][k+2]+27.0*(vel->vx[j][i][k+1]-vel->vx[j][i][k])+vel->vx[j][i][k-1])*(24.0*DZ);
			vyx=(-vel->vy[j][i+2][k]+27.0*(vel->vy[j][i+1][k]-vel->vy[j][i][k])+vel->vy[j][i-1][k])*(24.0*DX);
			vyz=(-vel->vy[j][i][k+2]+27.0*(vel->vy[j][i][k+1]-vel->vy[j][i][k])+vel->vy[j][i][k-1])*(24.0*DZ);
			vzx=(-vel->vz[j][i+2][k]+27.0*(vel->vz[j][i+1][k]-vel->vz[j][i][k])+vel->vz[j][i-1][k])*(24.0*DX);
			vzy=(-vel->vz[j+2][i][k]+27.0*(vel->vz[j+1][i][k]-vel->vz[j][i][k])+vel->vz[j-1][i][k])*(24.0*DY);*/
			
			vxy=(vel->vx[j+1][i][k]-vel->vx[j][i][k])*(dh24y);
		        vxz=(vel->vx[j][i][k+1]-vel->vx[j][i][k])*(dh24z);
			vyx=(vel->vy[j][i+1][k]-vel->vy[j][i][k])*(dh24x);
			vyz=(vel->vy[j][i][k+1]-vel->vy[j][i][k])*(dh24z);
			vzx=(vel->vz[j][i+1][k]-vel->vz[j][i][k])*(dh24x);
			vzy=(vel->vz[j+1][i][k]-vel->vz[j][i][k])*(dh24y);
			
			amp=mod->u[j][i][k]*((vyz-vzy)*fabs(vyz-vzy)+
					    (vzx-vxz)*fabs(vzx-vxz)+(vxy-vyx)*fabs(vxy-vyx));
			section->curl[itr][ins]=fsign(amp)*sqrt(fabs(amp));

			/*vxx=(-vel->vx[j][i+1][k]+27.0*(vel->vx[j][i][k]-vel->vx[j][i-1][k])+vel->vx[j][i-2][k])*(24.0*DX);
			vyy=(-vel->vy[j+1][i][k]+27.0*(vel->vy[j][i][k]-vel->vy[j-1][i][k])+vel->vy[j-2][i][k])*(24.0*DY);
			vzz=(-vel->vz[j][i][k+1]+27.0*(vel->vz[j][i][k]-vel->vz[j][i][k-1])+vel->vz[j][i][k-2])*(24.0*DZ);*/
			
			vxx=(vel->vx[j][i][k]-vel->vx[j][i-1][k])*(dh24x);
			vyy=(vel->vy[j][i][k]-vel->vy[j-1][i][k])*(dh24y);
			vzz=(vel->vz[j][i][k]-vel->vz[j][i][k-1])*(dh24z);
			
			section->div[itr][ins]=(vxx+vyy+vzz)*sqrt(mod->pi[j][i][k]);

			break;
		case 4 :				

			section->vx[itr][ins]=vel->vx[nyrec][nxrec][nzrec];
			section->vy[itr][ins]=vel->vy[nyrec][nxrec][nzrec];
			section->vz[itr][ins]=vel->vz[nyrec][nxrec][nzrec]; 
			section->p[itr][ins]=-stress->sxx[nyrec][nxrec][nzrec]-stress->syy[nyrec][nxrec][nzrec]-stress->szz[nyrec][nxrec][nzrec];
			i=nxrec; j=nyrec; k=nzrec;
			
			/*vxy=(-vel->vx[j+2][i][k]+27.0*(vel->vx[j+1][i][k]-vel->vx[j][i][k])+vel->vx[j-1][i][k])*(dh24y);
			vxz=(-vel->vx[j][i][k+2]+27.0*(vel->vx[j][i][k+1]-vel->vx[j][i][k])+vel->vx[j][i][k-1])*(dh24z);
			vyx=(-vel->vy[j][i+2][k]+27.0*(vel->vy[j][i+1][k]-vel->vy[j][i][k])+vel->vy[j][i-1][k])*(dh24x);
			vyz=(-vel->vy[j][i][k+2]+27.0*(vel->vy[j][i][k+1]-vel->vy[j][i][k])+vel->vy[j][i][k-1])*(dh24z);
			vzx=(-vel->vz[j][i+2][k]+27.0*(vel->vz[j][i+1][k]-vel->vz[j][i][k])+vel->vz[j][i-1][k])*(dh24x);
			vzy=(-vel->vz[j+2][i][k]+27.0*(vel->vz[j+1][i][k]-vel->vz[j][i][k])+vel->vz[j-1][i][k])*(dh24y);*/
			
			vxy=(vel->vx[j+1][i][k]-vel->vx[j][i][k])*(dh24y);
		        vxz=(vel->vx[j][i][k+1]-vel->vx[j][i][k])*(dh24z);
			vyx=(vel->vy[j][i+1][k]-vel->vy[j][i][k])*(dh24x);
			vyz=(vel->vy[j][i][k+1]-vel->vy[j][i][k])*(dh24z);
			vzx=(vel->vz[j][i+1][k]-vel->vz[j][i][k])*(dh24x);
			vzy=(vel->vz[j+1][i][k]-vel->vz[j][i][k])*(dh24y);
			
			amp=mod->u[j][i][k]*((vyz-vzy)*fabs(vyz-vzy)+
					    (vzx-vxz)*fabs(vzx-vxz)+(vxy-vyx)*fabs(vxy-vyx));
			section->curl[itr][ins]=fsign(amp)*sqrt(fabs(amp));

			/*vxx=(-vel->vx[j][i+1][k]+27.0*(vel->vx[j][i][k]-vel->vx[j][i-1][k])+vel->vx[j][i-2][k])*(dh24x);
			vyy=(-vel->vy[j+1][i][k]+27.0*(vel->vy[j][i][k]-vel->vy[j-1][i][k])+vel->vy[j-2][i][k])*(dh24y);
			vzz=(-vel->vz[j][i][k+1]+27.0*(vel->vz[j][i][k]-vel->vz[j][i][k-1])+vel->vz[j][i][k-2])*(dh24z);*/
			
			vxx=(vel->vx[j][i][k]-vel->vx[j][i-1][k])*(dh24x);
			vyy=(vel->vy[j][i][k]-vel->vy[j-1][i][k])*(dh24y);
			vzz=(vel->vz[j][i][k]-vel->vz[j][i][k-1])*(dh24z);
			
			section->div[itr][ins]=(vxx+vyy+vzz)*sqrt(mod->pi[j][i][k]);
			break;
			 		 
		}
	}
}
