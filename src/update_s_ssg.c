/*------------------------------------------------------------------------
 * Copyright (C) 2015 For the list of authors, see file AUTHORS.
 *
 * This file is part of IFOS3D.
 * 
 * IFOS3D is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 2.0 of the License only.
 * 
 * IFOS3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with IFOS3D. See file COPYING and/or 
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
--------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
 *   updating stress values by a staggered grid finite difference scheme of 
 *   nth order accuracy in space and second order accuracy in time
 *   viscoelastic version
 *  ----------------------------------------------------------------------*/

#include "fd.h"

double update_s(int nx1, int nx2, int ny1, int ny2, int nz1, int nz2, st_velocity *vel,
st_stress *stress, st_visc_mem *mem, st_model *mod, st_model_av *mod_av){


	extern float DT, DX, DY, DZ;
	extern int L, FDORDER, FDCOEFF;      /*MYID, LOG, */


	int i, j, k, l=1;
	double time=0.0;  /*, time1=0.0;*/
	float vxx,vxy,vxz,vyx,vyy,vyz,vzx,vzy,vzz;
	float vxyyx,vyzzy,vxzzx,vxxyyzz,vyyzz,vxxzz,vxxyy;
	float b,c,e,g,d,f,fipjp,fjpkp,fipkp,dipjp,djpkp,dipkp;
	float sumrxy,sumryz,sumrxz,sumrxx,sumryy,sumrzz;
        float b1, b2, b3, b4, b5, b6, dthalbe;

        dthalbe = DT/2.0; 
	
	/*if (LOG)
	if (MYID==0) time1=MPI_Wtime();*/

        switch (FDORDER){

 case 2 :
	
		 

		for (j=ny1;j<=ny2;j++){
			for (i=nx1;i<=nx2;i++){
				for (k=nz1;k<=nz2;k++){

				/* spatial derivatives of the components of the velocities
						    are computed */
						    
				vxx = (vel->vx[j][i][k]-vel->vx[j][i-1][k])/DX;
				vxy = (vel->vx[j+1][i][k]-vel->vx[j][i][k])/DY;		    
         			vxz = (vel->vx[j][i][k+1]-vel->vx[j][i][k])/DZ;		    
				vyx = (vel->vy[j][i+1][k]-vel->vy[j][i][k])/DX;
                                vyy = (vel->vy[j][i][k]-vel->vy[j-1][i][k])/DY;
				vyz = (vel->vy[j][i][k+1]-vel->vy[j][i][k])/DZ;
			        vzx = (vel->vz[j][i+1][k]-vel->vz[j][i][k])/DX;
				vzy = (vel->vz[j+1][i][k]-vel->vz[j][i][k])/DY;
				vzz = (vel->vz[j][i][k]-vel->vz[j][i][k-1])/DZ;
	

				/* computing sums of the old memory variables in this version only one mechanism is possi
				    ble */

				sumrxy=mem->rxy[j][i][k];
				sumryz=mem->ryz[j][i][k];
				sumrxz=mem->rxz[j][i][k];
				sumrxx=mem->rxx[j][i][k];
				sumryy=mem->ryy[j][i][k];
				sumrzz=mem->rzz[j][i][k];


				/* updating components of the stress tensor, partially */
				fipjp=mod_av->uipjp[j][i][k]*DT*(1.0+L*mod_av->tausipjp[j][i][k]);
				fjpkp=mod_av->ujpkp[j][i][k]*DT*(1.0+L*mod_av->tausjpkp[j][i][k]);
				fipkp=mod_av->uipkp[j][i][k]*DT*(1.0+L*mod_av->tausipkp[j][i][k]);
				g=mod->pi[j][i][k]*(1.0+L*mod->taup[j][i][k]);
				f=2.0*mod->u[j][i][k]*(1.0+L*mod->taus[j][i][k]);

				vxyyx=vxy+vyx;
				vyzzy=vyz+vzy;
				vxzzx=vxz+vzx;
				vxxyyzz=vxx+vyy+vzz;
				vyyzz=vyy+vzz;
				vxxzz=vxx+vzz;
				vxxyy=vxx+vyy;

				stress->sxy[j][i][k]+=(fipjp*vxyyx)+(dthalbe*sumrxy);
				stress->syz[j][i][k]+=(fjpkp*vyzzy)+(dthalbe*sumryz);
				stress->sxz[j][i][k]+=(fipkp*vxzzx)+(dthalbe*sumrxz);
				stress->sxx[j][i][k]+=DT*((g*vxxyyzz)-(f*vyyzz))+(dthalbe*sumrxx);
				stress->syy[j][i][k]+=DT*((g*vxxyyzz)-(f*vxxzz))+(dthalbe*sumryy);
				stress->szz[j][i][k]+=DT*((g*vxxyyzz)-(f*vxxyy))+(dthalbe*sumrzz);

				/* in the case of L=1 update
						    the memory-variables without suming*/
				l=1;
				b=1.0/(1.0+(mod->eta[l]*0.5));
				c=1.0-(mod->eta[l]*0.5);
				dipjp=mod_av->uipjp[j][i][k]*mod->eta[l]*mod_av->tausipjp[j][i][k];
				djpkp=mod_av->ujpkp[j][i][k]*mod->eta[l]*mod_av->tausjpkp[j][i][k];
				dipkp=mod_av->uipkp[j][i][k]*mod->eta[l]*mod_av->tausipkp[j][i][k];
				d=2.0*mod->u[j][i][k]*mod->eta[l]*mod->taus[j][i][k];
				e=mod->pi[j][i][k]*mod->eta[l]*mod->taup[j][i][k];
				mem->rxy[j][i][k]=b*(mem->rxy[j][i][k]*c-(dipjp*vxyyx));
				mem->ryz[j][i][k]=b*(mem->ryz[j][i][k]*c-(djpkp*vyzzy));
				mem->rxz[j][i][k]=b*(mem->rxz[j][i][k]*c-(dipkp*vxzzx));
				mem->rxx[j][i][k]=b*(mem->rxx[j][i][k]*c-(e*vxxyyzz)+(d*vyyzz));
				mem->ryy[j][i][k]=b*(mem->ryy[j][i][k]*c-(e*vxxyyzz)+(d*vxxzz));
				mem->rzz[j][i][k]=b*(mem->rzz[j][i][k]*c-(e*vxxyyzz)+(d*vxxyy));
				sumrxy=mem->rxy[j][i][k];
				sumryz=mem->ryz[j][i][k];
				sumrxz=mem->rxz[j][i][k];
				sumrxx=mem->rxx[j][i][k];
				sumryy=mem->ryy[j][i][k];
				sumrzz=mem->rzz[j][i][k];



				/* and now the components of the stress tensor are
						    completely updated */
				stress->sxy[j][i][k]+=(dthalbe*sumrxy);
				stress->syz[j][i][k]+=(dthalbe*sumryz);
				stress->sxz[j][i][k]+=(dthalbe*sumrxz);
				stress->sxx[j][i][k]+=(dthalbe*sumrxx);
				stress->syy[j][i][k]+=(dthalbe*sumryy);
				stress->szz[j][i][k]+=(dthalbe*sumrzz);

                           




			}
		}
	}
break;

        case 4 :
	
		b1=9.0/8.0; b2=-1.0/24.0; /* Taylor coefficients */
		if(FDCOEFF==2){
		b1=1.1382; b2=-0.046414;} /* Holberg coefficients E=0.1 %*/ 

		for (j=ny1;j<=ny2;j++){
			for (i=nx1;i<=nx2;i++){
				for (k=nz1;k<=nz2;k++){

				/* spatial derivatives of the components of the velocities
						    are computed */
						    
				vxx = (b1*(vel->vx[j][i][k]-vel->vx[j][i-1][k])+b2*(vel->vx[j][i+1][k]-vel->vx[j][i-2][k]))/DX;
				vxy = (b1*(vel->vx[j+1][i][k]-vel->vx[j][i][k])+b2*(vel->vx[j+2][i][k]-vel->vx[j-1][i][k]))/DY;		    
         			vxz = (b1*(vel->vx[j][i][k+1]-vel->vx[j][i][k])+b2*(vel->vx[j][i][k+2]-vel->vx[j][i][k-1]))/DZ;		    
				vyx = (b1*(vel->vy[j][i+1][k]-vel->vy[j][i][k])+b2*(vel->vy[j][i+2][k]-vel->vy[j][i-1][k]))/DX;
                                vyy = (b1*(vel->vy[j][i][k]-vel->vy[j-1][i][k])+b2*(vel->vy[j+1][i][k]-vel->vy[j-2][i][k]))/DY;
				vyz = (b1*(vel->vy[j][i][k+1]-vel->vy[j][i][k])+b2*(vel->vy[j][i][k+2]-vel->vy[j][i][k-1]))/DZ;
			        vzx = (b1*(vel->vz[j][i+1][k]-vel->vz[j][i][k])+b2*(vel->vz[j][i+2][k]-vel->vz[j][i-1][k]))/DX;
				vzy = (b1*(vel->vz[j+1][i][k]-vel->vz[j][i][k])+b2*(vel->vz[j+2][i][k]-vel->vz[j-1][i][k]))/DY;
				vzz = (b1*(vel->vz[j][i][k]-vel->vz[j][i][k-1])+b2*(vel->vz[j][i][k+1]-vel->vz[j][i][k-2]))/DZ;
	

				/* computing sums of the old memory variables in this version only one mechanism is possi
				    ble */

				sumrxy=mem->rxy[j][i][k];
				sumryz=mem->ryz[j][i][k];
				sumrxz=mem->rxz[j][i][k];
				sumrxx=mem->rxx[j][i][k];
				sumryy=mem->ryy[j][i][k];
				sumrzz=mem->rzz[j][i][k];


				/* updating components of the stress tensor, partially */
				fipjp=mod_av->uipjp[j][i][k]*DT*(1.0+L*mod_av->tausipjp[j][i][k]);
				fjpkp=mod_av->ujpkp[j][i][k]*DT*(1.0+L*mod_av->tausjpkp[j][i][k]);
				fipkp=mod_av->uipkp[j][i][k]*DT*(1.0+L*mod_av->tausipkp[j][i][k]);
				g=mod->pi[j][i][k]*(1.0+L*mod->taup[j][i][k]);
				f=2.0*mod->u[j][i][k]*(1.0+L*mod->taus[j][i][k]);

				vxyyx=vxy+vyx;
				vyzzy=vyz+vzy;
				vxzzx=vxz+vzx;
				vxxyyzz=vxx+vyy+vzz;
				vyyzz=vyy+vzz;
				vxxzz=vxx+vzz;
				vxxyy=vxx+vyy;

				stress->sxy[j][i][k]+=(fipjp*vxyyx)+(dthalbe*sumrxy);
				stress->syz[j][i][k]+=(fjpkp*vyzzy)+(dthalbe*sumryz);
				stress->sxz[j][i][k]+=(fipkp*vxzzx)+(dthalbe*sumrxz);
				stress->sxx[j][i][k]+=DT*((g*vxxyyzz)-(f*vyyzz))+(dthalbe*sumrxx);
				stress->syy[j][i][k]+=DT*((g*vxxyyzz)-(f*vxxzz))+(dthalbe*sumryy);
				stress->szz[j][i][k]+=DT*((g*vxxyyzz)-(f*vxxyy))+(dthalbe*sumrzz);

				/* in the case of L=1 update
						    the memory-variables without suming*/
				l=1;
				b=1.0/(1.0+(mod->eta[l]*0.5));
				c=1.0-(mod->eta[l]*0.5);
				dipjp=mod_av->uipjp[j][i][k]*mod->eta[l]*mod_av->tausipjp[j][i][k];
				djpkp=mod_av->ujpkp[j][i][k]*mod->eta[l]*mod_av->tausjpkp[j][i][k];
				dipkp=mod_av->uipkp[j][i][k]*mod->eta[l]*mod_av->tausipkp[j][i][k];
				d=2.0*mod->u[j][i][k]*mod->eta[l]*mod->taus[j][i][k];
				e=mod->pi[j][i][k]*mod->eta[l]*mod->taup[j][i][k];
				mem->rxy[j][i][k]=b*(mem->rxy[j][i][k]*c-(dipjp*vxyyx));
				mem->ryz[j][i][k]=b*(mem->ryz[j][i][k]*c-(djpkp*vyzzy));
				mem->rxz[j][i][k]=b*(mem->rxz[j][i][k]*c-(dipkp*vxzzx));
				mem->rxx[j][i][k]=b*(mem->rxx[j][i][k]*c-(e*vxxyyzz)+(d*vyyzz));
				mem->ryy[j][i][k]=b*(mem->ryy[j][i][k]*c-(e*vxxyyzz)+(d*vxxzz));
				mem->rzz[j][i][k]=b*(mem->rzz[j][i][k]*c-(e*vxxyyzz)+(d*vxxyy));
				sumrxy=mem->rxy[j][i][k];
				sumryz=mem->ryz[j][i][k];
				sumrxz=mem->rxz[j][i][k];
				sumrxx=mem->rxx[j][i][k];
				sumryy=mem->ryy[j][i][k];
				sumrzz=mem->rzz[j][i][k];



				/* and now the components of the stress tensor are
						    completely updated */
				stress->sxy[j][i][k]+=(dthalbe*sumrxy);
				stress->syz[j][i][k]+=(dthalbe*sumryz);
				stress->sxz[j][i][k]+=(dthalbe*sumrxz);
				stress->sxx[j][i][k]+=(dthalbe*sumrxx);
				stress->syy[j][i][k]+=(dthalbe*sumryy);
				stress->szz[j][i][k]+=(dthalbe*sumrzz);






			}
		}
	}
break;
	
        case 6 :
	      
	      b1=75.0/64.0; b2=-25.0/384.0; b3=3.0/640.0; /* Taylor coefficients */
	      if(FDCOEFF==2){
	      b1=1.1965; b2=-0.078804; b3=0.0081781;}   /* Holberg coefficients E=0.1 %*/
		
		for (j=ny1;j<=ny2;j++){
			for (i=nx1;i<=nx2;i++){
				for (k=nz1;k<=nz2;k++){

				/* spatial derivatives of the components of the velocities
						    are computed */
						    
				vxx = (b1*(vel->vx[j][i][k]-vel->vx[j][i-1][k])+
				       b2*(vel->vx[j][i+1][k]-vel->vx[j][i-2][k])+
		                       b3*(vel->vx[j][i+2][k]-vel->vx[j][i-3][k]))/DX;
				       
				vxy = (b1*(vel->vx[j+1][i][k]-vel->vx[j][i][k])+
				       b2*(vel->vx[j+2][i][k]-vel->vx[j-1][i][k])+
				       b3*(vel->vx[j+3][i][k]-vel->vx[j-2][i][k]))/DY;	
				       	    
         			vxz = (b1*(vel->vx[j][i][k+1]-vel->vx[j][i][k])+
				       b2*(vel->vx[j][i][k+2]-vel->vx[j][i][k-1])+
				       b3*(vel->vx[j][i][k+3]-vel->vx[j][i][k-2]))/DZ;
				       		    
				vyx = (b1*(vel->vy[j][i+1][k]-vel->vy[j][i][k])+
				       b2*(vel->vy[j][i+2][k]-vel->vy[j][i-1][k])+
				       b3*(vel->vy[j][i+3][k]-vel->vy[j][i-2][k]))/DX;
				       
                                vyy = (b1*(vel->vy[j][i][k]-vel->vy[j-1][i][k])+
				       b2*(vel->vy[j+1][i][k]-vel->vy[j-2][i][k])+
				       b3*(vel->vy[j+2][i][k]-vel->vy[j-3][i][k]))/DY;
				       
				vyz = (b1*(vel->vy[j][i][k+1]-vel->vy[j][i][k])+
				       b2*(vel->vy[j][i][k+2]-vel->vy[j][i][k-1])+
				       b3*(vel->vy[j][i][k+3]-vel->vy[j][i][k-2]))/DZ;								       
								       
			        vzx = (b1*(vel->vz[j][i+1][k]-vel->vz[j][i][k])+
				       b2*(vel->vz[j][i+2][k]-vel->vz[j][i-1][k])+
				       b3*(vel->vz[j][i+3][k]-vel->vz[j][i-2][k]))/DX;
				       
				vzy = (b1*(vel->vz[j+1][i][k]-vel->vz[j][i][k])+
				       b2*(vel->vz[j+2][i][k]-vel->vz[j-1][i][k])+
				       b3*(vel->vz[j+3][i][k]-vel->vz[j-2][i][k]))/DY;
				       
				vzz = (b1*(vel->vz[j][i][k]-vel->vz[j][i][k-1])+
				       b2*(vel->vz[j][i][k+1]-vel->vz[j][i][k-2])+
				       b3*(vel->vz[j][i][k+2]-vel->vz[j][i][k-3]))/DZ;
	

				/* computing sums of the old memory variables in this version only one mechanism is possi
				    ble */

				sumrxy=mem->rxy[j][i][k];
				sumryz=mem->ryz[j][i][k];
				sumrxz=mem->rxz[j][i][k];
				sumrxx=mem->rxx[j][i][k];
				sumryy=mem->ryy[j][i][k];
				sumrzz=mem->rzz[j][i][k];


				/* updating components of the stress tensor, partially */
				fipjp=mod_av->uipjp[j][i][k]*DT*(1.0+L*mod_av->tausipjp[j][i][k]);
				fjpkp=mod_av->ujpkp[j][i][k]*DT*(1.0+L*mod_av->tausjpkp[j][i][k]);
				fipkp=mod_av->uipkp[j][i][k]*DT*(1.0+L*mod_av->tausipkp[j][i][k]);
				g=mod->pi[j][i][k]*(1.0+L*mod->taup[j][i][k]);
				f=2.0*mod->u[j][i][k]*(1.0+L*mod->taus[j][i][k]);

				vxyyx=vxy+vyx;
				vyzzy=vyz+vzy;
				vxzzx=vxz+vzx;
				vxxyyzz=vxx+vyy+vzz;
				vyyzz=vyy+vzz;
				vxxzz=vxx+vzz;
				vxxyy=vxx+vyy;

				stress->sxy[j][i][k]+=(fipjp*vxyyx)+(dthalbe*sumrxy);
				stress->syz[j][i][k]+=(fjpkp*vyzzy)+(dthalbe*sumryz);
				stress->sxz[j][i][k]+=(fipkp*vxzzx)+(dthalbe*sumrxz);
				stress->sxx[j][i][k]+=DT*((g*vxxyyzz)-(f*vyyzz))+(dthalbe*sumrxx);
				stress->syy[j][i][k]+=DT*((g*vxxyyzz)-(f*vxxzz))+(dthalbe*sumryy);
				stress->szz[j][i][k]+=DT*((g*vxxyyzz)-(f*vxxyy))+(dthalbe*sumrzz);

				/* in the case of L=1 update
						    the memory-variables without suming*/
				l=1;
				b=1.0/(1.0+(mod->eta[l]*0.5));
				c=1.0-(mod->eta[l]*0.5);
				dipjp=mod_av->uipjp[j][i][k]*mod->eta[l]*mod_av->tausipjp[j][i][k];
				djpkp=mod_av->ujpkp[j][i][k]*mod->eta[l]*mod_av->tausjpkp[j][i][k];
				dipkp=mod_av->uipkp[j][i][k]*mod->eta[l]*mod_av->tausipkp[j][i][k];
				d=2.0*mod->u[j][i][k]*mod->eta[l]*mod->taus[j][i][k];
				e=mod->pi[j][i][k]*mod->eta[l]*mod->taup[j][i][k];
				mem->rxy[j][i][k]=b*(mem->rxy[j][i][k]*c-(dipjp*vxyyx));
				mem->ryz[j][i][k]=b*(mem->ryz[j][i][k]*c-(djpkp*vyzzy));
				mem->rxz[j][i][k]=b*(mem->rxz[j][i][k]*c-(dipkp*vxzzx));
				mem->rxx[j][i][k]=b*(mem->rxx[j][i][k]*c-(e*vxxyyzz)+(d*vyyzz));
				mem->ryy[j][i][k]=b*(mem->ryy[j][i][k]*c-(e*vxxyyzz)+(d*vxxzz));
				mem->rzz[j][i][k]=b*(mem->rzz[j][i][k]*c-(e*vxxyyzz)+(d*vxxyy));
				sumrxy=mem->rxy[j][i][k];
				sumryz=mem->ryz[j][i][k];
				sumrxz=mem->rxz[j][i][k];
				sumrxx=mem->rxx[j][i][k];
				sumryy=mem->ryy[j][i][k];
				sumrzz=mem->rzz[j][i][k];



				/* and now the components of the stress tensor are
						    completely updated */
				stress->sxy[j][i][k]+=(dthalbe*sumrxy);
				stress->syz[j][i][k]+=(dthalbe*sumryz);
				stress->sxz[j][i][k]+=(dthalbe*sumrxz);
				stress->sxx[j][i][k]+=(dthalbe*sumrxx);
				stress->syy[j][i][k]+=(dthalbe*sumryy);
				stress->szz[j][i][k]+=(dthalbe*sumrzz);






			}
		}
	}
break;	

 case 8 :
	      
	     b1=1225.0/1024.0; b2=-245.0/3072.0; b3=49.0/5120.0; b4=-5.0/7168.0; /* Taylor coefficients */
	     if(FDCOEFF==2){
	     b1=1.2257; b2=-0.099537; b3=0.018063; b4=-0.0026274;} /* Holberg coefficients E=0.1 %*/
		
		for (j=ny1;j<=ny2;j++){
			for (i=nx1;i<=nx2;i++){
				for (k=nz1;k<=nz2;k++){

				/* spatial derivatives of the components of the velocities
						    are computed */
						    
				vxx = (b1*(vel->vx[j][i][k]-vel->vx[j][i-1][k])+
				       b2*(vel->vx[j][i+1][k]-vel->vx[j][i-2][k])+
		                       b3*(vel->vx[j][i+2][k]-vel->vx[j][i-3][k])+
				       b4*(vel->vx[j][i+3][k]-vel->vx[j][i-4][k]))/DX;
				       
				vxy = (b1*(vel->vx[j+1][i][k]-vel->vx[j][i][k])+
				       b2*(vel->vx[j+2][i][k]-vel->vx[j-1][i][k])+
				       b3*(vel->vx[j+3][i][k]-vel->vx[j-2][i][k])+
				       b4*(vel->vx[j+4][i][k]-vel->vx[j-3][i][k]))/DY;	
				       	    
         			vxz = (b1*(vel->vx[j][i][k+1]-vel->vx[j][i][k])+
				       b2*(vel->vx[j][i][k+2]-vel->vx[j][i][k-1])+
				       b3*(vel->vx[j][i][k+3]-vel->vx[j][i][k-2])+
				       b4*(vel->vx[j][i][k+4]-vel->vx[j][i][k-3]))/DZ;
				       		    
				vyx = (b1*(vel->vy[j][i+1][k]-vel->vy[j][i][k])+
				       b2*(vel->vy[j][i+2][k]-vel->vy[j][i-1][k])+
				       b3*(vel->vy[j][i+3][k]-vel->vy[j][i-2][k])+
				       b4*(vel->vy[j][i+4][k]-vel->vy[j][i-3][k]))/DX;
				       
                                vyy = (b1*(vel->vy[j][i][k]-vel->vy[j-1][i][k])+
				       b2*(vel->vy[j+1][i][k]-vel->vy[j-2][i][k])+
				       b3*(vel->vy[j+2][i][k]-vel->vy[j-3][i][k])+
				       b4*(vel->vy[j+3][i][k]-vel->vy[j-4][i][k]))/DY;
				       
				vyz = (b1*(vel->vy[j][i][k+1]-vel->vy[j][i][k])+
				       b2*(vel->vy[j][i][k+2]-vel->vy[j][i][k-1])+
				       b3*(vel->vy[j][i][k+3]-vel->vy[j][i][k-2])+
				       b4*(vel->vy[j][i][k+4]-vel->vy[j][i][k-3]))/DZ;								       
								       
			        vzx = (b1*(vel->vz[j][i+1][k]-vel->vz[j][i][k])+
				       b2*(vel->vz[j][i+2][k]-vel->vz[j][i-1][k])+
				       b3*(vel->vz[j][i+3][k]-vel->vz[j][i-2][k])+
				       b4*(vel->vz[j][i+4][k]-vel->vz[j][i-3][k]))/DX;
				       
				vzy = (b1*(vel->vz[j+1][i][k]-vel->vz[j][i][k])+
				       b2*(vel->vz[j+2][i][k]-vel->vz[j-1][i][k])+
				       b3*(vel->vz[j+3][i][k]-vel->vz[j-2][i][k])+
				       b4*(vel->vz[j+4][i][k]-vel->vz[j-3][i][k]))/DY;
				       
				vzz = (b1*(vel->vz[j][i][k]-vel->vz[j][i][k-1])+
				       b2*(vel->vz[j][i][k+1]-vel->vz[j][i][k-2])+
				       b3*(vel->vz[j][i][k+2]-vel->vz[j][i][k-3])+
				       b4*(vel->vz[j][i][k+3]-vel->vz[j][i][k-4]))/DZ;
	

				/* computing sums of the old memory variables in this version only one mechanism is possi
				    ble */

				sumrxy=mem->rxy[j][i][k];
				sumryz=mem->ryz[j][i][k];
				sumrxz=mem->rxz[j][i][k];
				sumrxx=mem->rxx[j][i][k];
				sumryy=mem->ryy[j][i][k];
				sumrzz=mem->rzz[j][i][k];


				/* updating components of the stress tensor, partially */
				fipjp=mod_av->uipjp[j][i][k]*DT*(1.0+L*mod_av->tausipjp[j][i][k]);
				fjpkp=mod_av->ujpkp[j][i][k]*DT*(1.0+L*mod_av->tausjpkp[j][i][k]);
				fipkp=mod_av->uipkp[j][i][k]*DT*(1.0+L*mod_av->tausipkp[j][i][k]);
				g=mod->pi[j][i][k]*(1.0+L*mod->taup[j][i][k]);
				f=2.0*mod->u[j][i][k]*(1.0+L*mod->taus[j][i][k]);

				vxyyx=vxy+vyx;
				vyzzy=vyz+vzy;
				vxzzx=vxz+vzx;
				vxxyyzz=vxx+vyy+vzz;
				vyyzz=vyy+vzz;
				vxxzz=vxx+vzz;
				vxxyy=vxx+vyy;

				stress->sxy[j][i][k]+=(fipjp*vxyyx)+(dthalbe*sumrxy);
				stress->syz[j][i][k]+=(fjpkp*vyzzy)+(dthalbe*sumryz);
				stress->sxz[j][i][k]+=(fipkp*vxzzx)+(dthalbe*sumrxz);
				stress->sxx[j][i][k]+=DT*((g*vxxyyzz)-(f*vyyzz))+(dthalbe*sumrxx);
				stress->syy[j][i][k]+=DT*((g*vxxyyzz)-(f*vxxzz))+(dthalbe*sumryy);
				stress->szz[j][i][k]+=DT*((g*vxxyyzz)-(f*vxxyy))+(dthalbe*sumrzz);

				/* in the case of L=1 update
						    the memory-variables without suming*/
				l=1;
				b=1.0/(1.0+(mod->eta[l]*0.5));
				c=1.0-(mod->eta[l]*0.5);
				dipjp=mod_av->uipjp[j][i][k]*mod->eta[l]*mod_av->tausipjp[j][i][k];
				djpkp=mod_av->ujpkp[j][i][k]*mod->eta[l]*mod_av->tausjpkp[j][i][k];
				dipkp=mod_av->uipkp[j][i][k]*mod->eta[l]*mod_av->tausipkp[j][i][k];
				d=2.0*mod->u[j][i][k]*mod->eta[l]*mod->taus[j][i][k];
				e=mod->pi[j][i][k]*mod->eta[l]*mod->taup[j][i][k];
				mem->rxy[j][i][k]=b*(mem->rxy[j][i][k]*c-(dipjp*vxyyx));
				mem->ryz[j][i][k]=b*(mem->ryz[j][i][k]*c-(djpkp*vyzzy));
				mem->rxz[j][i][k]=b*(mem->rxz[j][i][k]*c-(dipkp*vxzzx));
				mem->rxx[j][i][k]=b*(mem->rxx[j][i][k]*c-(e*vxxyyzz)+(d*vyyzz));
				mem->ryy[j][i][k]=b*(mem->ryy[j][i][k]*c-(e*vxxyyzz)+(d*vxxzz));
				mem->rzz[j][i][k]=b*(mem->rzz[j][i][k]*c-(e*vxxyyzz)+(d*vxxyy));
				sumrxy=mem->rxy[j][i][k];
				sumryz=mem->ryz[j][i][k];
				sumrxz=mem->rxz[j][i][k];
				sumrxx=mem->rxx[j][i][k];
				sumryy=mem->ryy[j][i][k];
				sumrzz=mem->rzz[j][i][k];



				/* and now the components of the stress tensor are
						    completely updated */
				stress->sxy[j][i][k]+=(dthalbe*sumrxy);
				stress->syz[j][i][k]+=(dthalbe*sumryz);
				stress->sxz[j][i][k]+=(dthalbe*sumrxz);
				stress->sxx[j][i][k]+=(dthalbe*sumrxx);
				stress->syy[j][i][k]+=(dthalbe*sumryy);
				stress->szz[j][i][k]+=(dthalbe*sumrzz);






			}
		}
	}
break;	

case 10 :
	      
	     b1=19845.0/16384.0; b2=-735.0/8192.0; b3=567.0/40960.0; b4=-405.0/229376.0; b5=35.0/294912.0; /* Taylor coefficients */
	     if(FDCOEFF==2){
	     b1=1.2415; b2=-0.11231; b3=0.026191; b4=-0.0064682; b5=0.001191;} /* Holberg coefficients E=0.1 %*/
		
		for (j=ny1;j<=ny2;j++){
			for (i=nx1;i<=nx2;i++){
				for (k=nz1;k<=nz2;k++){

				/* spatial derivatives of the components of the velocities
						    are computed */
						    
				vxx = (b1*(vel->vx[j][i][k]-vel->vx[j][i-1][k])+
				       b2*(vel->vx[j][i+1][k]-vel->vx[j][i-2][k])+
		                       b3*(vel->vx[j][i+2][k]-vel->vx[j][i-3][k])+
				       b4*(vel->vx[j][i+3][k]-vel->vx[j][i-4][k])+
				       b5*(vel->vx[j][i+4][k]-vel->vx[j][i-5][k]))/DX;
				       
				vxy = (b1*(vel->vx[j+1][i][k]-vel->vx[j][i][k])+
				       b2*(vel->vx[j+2][i][k]-vel->vx[j-1][i][k])+
				       b3*(vel->vx[j+3][i][k]-vel->vx[j-2][i][k])+
				       b4*(vel->vx[j+4][i][k]-vel->vx[j-3][i][k])+
				       b5*(vel->vx[j+5][i][k]-vel->vx[j-4][i][k]))/DY;	
				       	    
         			vxz = (b1*(vel->vx[j][i][k+1]-vel->vx[j][i][k])+
				       b2*(vel->vx[j][i][k+2]-vel->vx[j][i][k-1])+
				       b3*(vel->vx[j][i][k+3]-vel->vx[j][i][k-2])+
				       b4*(vel->vx[j][i][k+4]-vel->vx[j][i][k-3])+
				       b5*(vel->vx[j][i][k+5]-vel->vx[j][i][k-4]))/DZ;
				       		    
				vyx = (b1*(vel->vy[j][i+1][k]-vel->vy[j][i][k])+
				       b2*(vel->vy[j][i+2][k]-vel->vy[j][i-1][k])+
				       b3*(vel->vy[j][i+3][k]-vel->vy[j][i-2][k])+
				       b4*(vel->vy[j][i+4][k]-vel->vy[j][i-3][k])+
				       b5*(vel->vy[j][i+5][k]-vel->vy[j][i-4][k]))/DX;
				       
                                vyy = (b1*(vel->vy[j][i][k]-vel->vy[j-1][i][k])+
				       b2*(vel->vy[j+1][i][k]-vel->vy[j-2][i][k])+
				       b3*(vel->vy[j+2][i][k]-vel->vy[j-3][i][k])+
				       b4*(vel->vy[j+3][i][k]-vel->vy[j-4][i][k])+
				       b5*(vel->vy[j+4][i][k]-vel->vy[j-5][i][k]))/DY;
				       
				vyz = (b1*(vel->vy[j][i][k+1]-vel->vy[j][i][k])+
				       b2*(vel->vy[j][i][k+2]-vel->vy[j][i][k-1])+
				       b3*(vel->vy[j][i][k+3]-vel->vy[j][i][k-2])+
				       b4*(vel->vy[j][i][k+4]-vel->vy[j][i][k-3])+
				       b5*(vel->vy[j][i][k+5]-vel->vy[j][i][k-4]))/DZ;								       
								       
			        vzx = (b1*(vel->vz[j][i+1][k]-vel->vz[j][i][k])+
				       b2*(vel->vz[j][i+2][k]-vel->vz[j][i-1][k])+
				       b3*(vel->vz[j][i+3][k]-vel->vz[j][i-2][k])+
				       b4*(vel->vz[j][i+4][k]-vel->vz[j][i-3][k])+
				       b5*(vel->vz[j][i+5][k]-vel->vz[j][i-4][k]))/DX;
				       
				vzy = (b1*(vel->vz[j+1][i][k]-vel->vz[j][i][k])+
				       b2*(vel->vz[j+2][i][k]-vel->vz[j-1][i][k])+
				       b3*(vel->vz[j+3][i][k]-vel->vz[j-2][i][k])+
				       b4*(vel->vz[j+4][i][k]-vel->vz[j-3][i][k])+
				       b5*(vel->vz[j+5][i][k]-vel->vz[j-4][i][k]))/DY;
				       
				vzz = (b1*(vel->vz[j][i][k]-vel->vz[j][i][k-1])+
				       b2*(vel->vz[j][i][k+1]-vel->vz[j][i][k-2])+
				       b3*(vel->vz[j][i][k+2]-vel->vz[j][i][k-3])+
				       b4*(vel->vz[j][i][k+3]-vel->vz[j][i][k-4])+
				       b5*(vel->vz[j][i][k+4]-vel->vz[j][i][k-5]))/DZ;
	

				/* computing sums of the old memory variables in this version only one mechanism is possi
				    ble */

				sumrxy=mem->rxy[j][i][k];
				sumryz=mem->ryz[j][i][k];
				sumrxz=mem->rxz[j][i][k];
				sumrxx=mem->rxx[j][i][k];
				sumryy=mem->ryy[j][i][k];
				sumrzz=mem->rzz[j][i][k];


				/* updating components of the stress tensor, partially */
				fipjp=mod_av->uipjp[j][i][k]*DT*(1.0+L*mod_av->tausipjp[j][i][k]);
				fjpkp=mod_av->ujpkp[j][i][k]*DT*(1.0+L*mod_av->tausjpkp[j][i][k]);
				fipkp=mod_av->uipkp[j][i][k]*DT*(1.0+L*mod_av->tausipkp[j][i][k]);
				g=mod->pi[j][i][k]*(1.0+L*mod->taup[j][i][k]);
				f=2.0*mod->u[j][i][k]*(1.0+L*mod->taus[j][i][k]);

				vxyyx=vxy+vyx;
				vyzzy=vyz+vzy;
				vxzzx=vxz+vzx;
				vxxyyzz=vxx+vyy+vzz;
				vyyzz=vyy+vzz;
				vxxzz=vxx+vzz;
				vxxyy=vxx+vyy;

				stress->sxy[j][i][k]+=(fipjp*vxyyx)+(dthalbe*sumrxy);
				stress->syz[j][i][k]+=(fjpkp*vyzzy)+(dthalbe*sumryz);
				stress->sxz[j][i][k]+=(fipkp*vxzzx)+(dthalbe*sumrxz);
				stress->sxx[j][i][k]+=DT*((g*vxxyyzz)-(f*vyyzz))+(dthalbe*sumrxx);
				stress->syy[j][i][k]+=DT*((g*vxxyyzz)-(f*vxxzz))+(dthalbe*sumryy);
				stress->szz[j][i][k]+=DT*((g*vxxyyzz)-(f*vxxyy))+(dthalbe*sumrzz);

				/* in the case of L=1 update
						    the memory-variables without suming*/
				l=1;
				b=1.0/(1.0+(mod->eta[l]*0.5));
				c=1.0-(mod->eta[l]*0.5);
				dipjp=mod_av->uipjp[j][i][k]*mod->eta[l]*mod_av->tausipjp[j][i][k];
				djpkp=mod_av->ujpkp[j][i][k]*mod->eta[l]*mod_av->tausjpkp[j][i][k];
				dipkp=mod_av->uipkp[j][i][k]*mod->eta[l]*mod_av->tausipkp[j][i][k];
				d=2.0*mod->u[j][i][k]*mod->eta[l]*mod->taus[j][i][k];
				e=mod->pi[j][i][k]*mod->eta[l]*mod->taup[j][i][k];
				mem->rxy[j][i][k]=b*(mem->rxy[j][i][k]*c-(dipjp*vxyyx));
				mem->ryz[j][i][k]=b*(mem->ryz[j][i][k]*c-(djpkp*vyzzy));
				mem->rxz[j][i][k]=b*(mem->rxz[j][i][k]*c-(dipkp*vxzzx));
				mem->rxx[j][i][k]=b*(mem->rxx[j][i][k]*c-(e*vxxyyzz)+(d*vyyzz));
				mem->ryy[j][i][k]=b*(mem->ryy[j][i][k]*c-(e*vxxyyzz)+(d*vxxzz));
				mem->rzz[j][i][k]=b*(mem->rzz[j][i][k]*c-(e*vxxyyzz)+(d*vxxyy));
				sumrxy=mem->rxy[j][i][k];
				sumryz=mem->ryz[j][i][k];
				sumrxz=mem->rxz[j][i][k];
				sumrxx=mem->rxx[j][i][k];
				sumryy=mem->ryy[j][i][k];
				sumrzz=mem->rzz[j][i][k];



				/* and now the components of the stress tensor are
						    completely updated */
				stress->sxy[j][i][k]+=(dthalbe*sumrxy);
				stress->syz[j][i][k]+=(dthalbe*sumryz);
				stress->sxz[j][i][k]+=(dthalbe*sumrxz);
				stress->sxx[j][i][k]+=(dthalbe*sumrxx);
				stress->syy[j][i][k]+=(dthalbe*sumryy);
				stress->szz[j][i][k]+=(dthalbe*sumrzz);






			}
		}
	}
break;	

case 12 :
	      
	        /* Taylor coefficients */
	        b1=160083.0/131072.0; b2=-12705.0/131072.0; b3=22869.0/1310720.0; 
		b4=-5445.0/1835008.0; b5=847.0/2359296.0; b6=-63.0/2883584;
		
		/* Holberg coefficients E=0.1 %*/
		if(FDCOEFF==2){
		b1=1.2508; b2=-0.12034; b3=0.032131; b4=-0.010142; b5=0.0029857; b6=-0.00066667;}
		
		for (j=ny1;j<=ny2;j++){
			for (i=nx1;i<=nx2;i++){
				for (k=nz1;k<=nz2;k++){

				/* spatial derivatives of the components of the velocities
						    are computed */
						    
				vxx = (b1*(vel->vx[j][i][k]-vel->vx[j][i-1][k])+
				       b2*(vel->vx[j][i+1][k]-vel->vx[j][i-2][k])+
		                       b3*(vel->vx[j][i+2][k]-vel->vx[j][i-3][k])+
				       b4*(vel->vx[j][i+3][k]-vel->vx[j][i-4][k])+
				       b5*(vel->vx[j][i+4][k]-vel->vx[j][i-5][k])+
				       b6*(vel->vx[j][i+5][k]-vel->vx[j][i-6][k]))/DX;
				       
				vxy = (b1*(vel->vx[j+1][i][k]-vel->vx[j][i][k])+
				       b2*(vel->vx[j+2][i][k]-vel->vx[j-1][i][k])+
				       b3*(vel->vx[j+3][i][k]-vel->vx[j-2][i][k])+
				       b4*(vel->vx[j+4][i][k]-vel->vx[j-3][i][k])+
				       b5*(vel->vx[j+5][i][k]-vel->vx[j-4][i][k])+
				       b6*(vel->vx[j+6][i][k]-vel->vx[j-5][i][k]))/DY;	
				       	    
         			vxz = (b1*(vel->vx[j][i][k+1]-vel->vx[j][i][k])+
				       b2*(vel->vx[j][i][k+2]-vel->vx[j][i][k-1])+
				       b3*(vel->vx[j][i][k+3]-vel->vx[j][i][k-2])+
				       b4*(vel->vx[j][i][k+4]-vel->vx[j][i][k-3])+
				       b5*(vel->vx[j][i][k+5]-vel->vx[j][i][k-4])+
				       b6*(vel->vx[j][i][k+6]-vel->vx[j][i][k-5]))/DZ;
				       		    
				vyx = (b1*(vel->vy[j][i+1][k]-vel->vy[j][i][k])+
				       b2*(vel->vy[j][i+2][k]-vel->vy[j][i-1][k])+
				       b3*(vel->vy[j][i+3][k]-vel->vy[j][i-2][k])+
				       b4*(vel->vy[j][i+4][k]-vel->vy[j][i-3][k])+
				       b5*(vel->vy[j][i+5][k]-vel->vy[j][i-4][k])+
				       b6*(vel->vy[j][i+6][k]-vel->vy[j][i-5][k]))/DX;
				       
                                vyy = (b1*(vel->vy[j][i][k]-vel->vy[j-1][i][k])+
				       b2*(vel->vy[j+1][i][k]-vel->vy[j-2][i][k])+
				       b3*(vel->vy[j+2][i][k]-vel->vy[j-3][i][k])+
				       b4*(vel->vy[j+3][i][k]-vel->vy[j-4][i][k])+
				       b5*(vel->vy[j+4][i][k]-vel->vy[j-5][i][k])+
				       b6*(vel->vy[j+5][i][k]-vel->vy[j-6][i][k]))/DY;
				       
				vyz = (b1*(vel->vy[j][i][k+1]-vel->vy[j][i][k])+
				       b2*(vel->vy[j][i][k+2]-vel->vy[j][i][k-1])+
				       b3*(vel->vy[j][i][k+3]-vel->vy[j][i][k-2])+
				       b4*(vel->vy[j][i][k+4]-vel->vy[j][i][k-3])+
				       b5*(vel->vy[j][i][k+5]-vel->vy[j][i][k-4])+
				       b6*(vel->vy[j][i][k+6]-vel->vy[j][i][k-5]))/DZ;								       
								       
			        vzx = (b1*(vel->vz[j][i+1][k]-vel->vz[j][i][k])+
				       b2*(vel->vz[j][i+2][k]-vel->vz[j][i-1][k])+
				       b3*(vel->vz[j][i+3][k]-vel->vz[j][i-2][k])+
				       b4*(vel->vz[j][i+4][k]-vel->vz[j][i-3][k])+
				       b5*(vel->vz[j][i+5][k]-vel->vz[j][i-4][k])+
				       b6*(vel->vz[j][i+6][k]-vel->vz[j][i-5][k]))/DX;
				       
				vzy = (b1*(vel->vz[j+1][i][k]-vel->vz[j][i][k])+
				       b2*(vel->vz[j+2][i][k]-vel->vz[j-1][i][k])+
				       b3*(vel->vz[j+3][i][k]-vel->vz[j-2][i][k])+
				       b4*(vel->vz[j+4][i][k]-vel->vz[j-3][i][k])+
				       b5*(vel->vz[j+5][i][k]-vel->vz[j-4][i][k])+
				       b6*(vel->vz[j+6][i][k]-vel->vz[j-5][i][k]))/DY;
				       
				vzz = (b1*(vel->vz[j][i][k]-vel->vz[j][i][k-1])+
				       b2*(vel->vz[j][i][k+1]-vel->vz[j][i][k-2])+
				       b3*(vel->vz[j][i][k+2]-vel->vz[j][i][k-3])+
				       b4*(vel->vz[j][i][k+3]-vel->vz[j][i][k-4])+
				       b5*(vel->vz[j][i][k+4]-vel->vz[j][i][k-5])+
				       b6*(vel->vz[j][i][k+5]-vel->vz[j][i][k-6]))/DZ;
	

				/* computing sums of the old memory variables in this version only one mechanism is possi
				    ble */

				sumrxy=mem->rxy[j][i][k];
				sumryz=mem->ryz[j][i][k];
				sumrxz=mem->rxz[j][i][k];
				sumrxx=mem->rxx[j][i][k];
				sumryy=mem->ryy[j][i][k];
				sumrzz=mem->rzz[j][i][k];


				/* updating components of the stress tensor, partially */
				fipjp=mod_av->uipjp[j][i][k]*DT*(1.0+L*mod_av->tausipjp[j][i][k]);
				fjpkp=mod_av->ujpkp[j][i][k]*DT*(1.0+L*mod_av->tausjpkp[j][i][k]);
				fipkp=mod_av->uipkp[j][i][k]*DT*(1.0+L*mod_av->tausipkp[j][i][k]);
				g=mod->pi[j][i][k]*(1.0+L*mod->taup[j][i][k]);
				f=2.0*mod->u[j][i][k]*(1.0+L*mod->taus[j][i][k]);

				vxyyx=vxy+vyx;
				vyzzy=vyz+vzy;
				vxzzx=vxz+vzx;
				vxxyyzz=vxx+vyy+vzz;
				vyyzz=vyy+vzz;
				vxxzz=vxx+vzz;
				vxxyy=vxx+vyy;

				stress->sxy[j][i][k]+=(fipjp*vxyyx)+(dthalbe*sumrxy);
				stress->syz[j][i][k]+=(fjpkp*vyzzy)+(dthalbe*sumryz);
				stress->sxz[j][i][k]+=(fipkp*vxzzx)+(dthalbe*sumrxz);
				stress->sxx[j][i][k]+=DT*((g*vxxyyzz)-(f*vyyzz))+(dthalbe*sumrxx);
				stress->syy[j][i][k]+=DT*((g*vxxyyzz)-(f*vxxzz))+(dthalbe*sumryy);
				stress->szz[j][i][k]+=DT*((g*vxxyyzz)-(f*vxxyy))+(dthalbe*sumrzz);

				/* in the case of L=1 update
						    the memory-variables without suming*/
				l=1;
				b=1.0/(1.0+(mod->eta[l]*0.5));
				c=1.0-(mod->eta[l]*0.5);
				dipjp=mod_av->uipjp[j][i][k]*mod->eta[l]*mod_av->tausipjp[j][i][k];
				djpkp=mod_av->ujpkp[j][i][k]*mod->eta[l]*mod_av->tausjpkp[j][i][k];
				dipkp=mod_av->uipkp[j][i][k]*mod->eta[l]*mod_av->tausipkp[j][i][k];
				d=2.0*mod->u[j][i][k]*mod->eta[l]*mod->taus[j][i][k];
				e=mod->pi[j][i][k]*mod->eta[l]*mod->taup[j][i][k];
				mem->rxy[j][i][k]=b*(mem->rxy[j][i][k]*c-(dipjp*vxyyx));
				mem->ryz[j][i][k]=b*(mem->ryz[j][i][k]*c-(djpkp*vyzzy));
				mem->rxz[j][i][k]=b*(mem->rxz[j][i][k]*c-(dipkp*vxzzx));
				mem->rxx[j][i][k]=b*(mem->rxx[j][i][k]*c-(e*vxxyyzz)+(d*vyyzz));
				mem->ryy[j][i][k]=b*(mem->ryy[j][i][k]*c-(e*vxxyyzz)+(d*vxxzz));
				mem->rzz[j][i][k]=b*(mem->rzz[j][i][k]*c-(e*vxxyyzz)+(d*vxxyy));
				sumrxy=mem->rxy[j][i][k];
				sumryz=mem->ryz[j][i][k];
				sumrxz=mem->rxz[j][i][k];
				sumrxx=mem->rxx[j][i][k];
				sumryy=mem->ryy[j][i][k];
				sumrzz=mem->rzz[j][i][k];



				/* and now the components of the stress tensor are
						    completely updated */
				stress->sxy[j][i][k]+=(dthalbe*sumrxy);
				stress->syz[j][i][k]+=(dthalbe*sumryz);
				stress->sxz[j][i][k]+=(dthalbe*sumrxz);
				stress->sxx[j][i][k]+=(dthalbe*sumrxx);
				stress->syy[j][i][k]+=(dthalbe*sumryy);
				stress->szz[j][i][k]+=(dthalbe*sumrzz);






			}
		}
	}
break;	

}

        /*if (LOG)
	if (MYID==0){
		time2=MPI_Wtime();
		time=time2-time1;
		fprintf(FP," Real time for stress tensor update: \t\t %4.2f s.\n",time);
	}*/
	return time;

}
