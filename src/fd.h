/*------------------------------------------------------------------------
 * Copyright (C) 2015 For the list of authors, see file AUTHORS.
 *
 * This file is part of IFOS3D.
 *
 * IFOS3D is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 2.0 of the License only.
 *
 * IFOS3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with IFOS3D. See file COPYING and/or
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
--------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
 *  fd.h - include file for IFOS3D
 *  ---------------------------------------------------------------------*/


/* files to include */
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <time.h>
#include <mpi.h>


#define iround(x) ((int)(floor)(x+0.5))
#define min(x,y) ((x<y)?x:y)
#define max(x,y) ((x<y)?y:x)
#define fsign(x) ((x<0.0)?(-1):1)

#define PI (3.141592653589793)
#define NPAR 45
#define STRING_SIZE 256
#define REQUEST_COUNT 6
#define NPROCX_MAX 100
#define NPROCY_MAX 100
#define NPROCZ_MAX 100

 typedef struct Model{
    //model
	float  ***  rho, ***  pi, ***  u;
    /* Variables for viscoelastic modeling */
	float  ***  taus, ***  taup, *eta;

  }st_model;
  
  typedef struct Model_AV{
    //Averaged model for staggered grid
	float  *** uipjp, *** ujpkp, *** uipkp, *** rjp, *** rkp, *** rip;
    /* Variables for viscoelastic modeling */
	float *** tausipjp, *** tausjpkp, *** tausipkp;
  }st_model_av;
  

typedef struct Stress {
	//stress tensor
	// Pointer for dynamic wavefields:
	float ** *sxx,  * **syy,  * **szz;
	float ** *sxy,  * **syz,  * **sxz;
} st_stress;

typedef struct Particle_velocity {
	//particle velocity
	// Pointer for dynamic wavefields:
	float   ** *vx, * **vy, * **vz;
} st_velocity;

typedef struct Freq_Particle_velocity {
	//particle velocity in the frequency domain
	// Pointer for dynamic wavefields:
	float **** Fvx_re, **** Fvy_re, **** Fvz_re;
	float **** Fvx_im, **** Fvy_im, **** Fvz_im;
	
} st_freq_velocity;

typedef struct PML_Coefficients {
	//Pml coefficients for pml damping boundaries
	float *K_x, * K_x_half, * K_y, * K_y_half , * K_z , * K_z_half;
	float *a_x, * a_x_half, * a_y, * a_y_half, * a_z, * a_z_half;
	float *b_x, * b_x_half, * b_y, * b_y_half, * b_z, * b_z_half;
	float *alpha_prime_x, * alpha_prime_x_half, * alpha_prime_y, * alpha_prime_y_half, * alpha_prime_z, * alpha_prime_z_half;

} st_pml_coeff;

typedef struct PML_Wavefield {
	//Pml Damping Wavefields for pml damping boundaries
	float ***psi_sxx_x, ***psi_syy_y, ***psi_szz_z;
	float ***psi_sxy_y, ***psi_sxy_x, ***psi_sxz_x, ***psi_sxz_z, ***psi_syz_y, ***psi_syz_z;
	float ***psi_vxx, ***psi_vyy, ***psi_vzz;
	float ***psi_vxy, ***psi_vxz, ***psi_vyx, ***psi_vyz, ***psi_vzx, ***psi_vzy;
} st_pml_wfd;

  typedef struct Visco_memory{ 
    /* memory variables for viscoelastic modeling */     
	float  ***  rxy, ***  ryz, ***  rxz;
	float  ***  rxx, ***  ryy, ***  rzz;
  }st_visc_mem;

 typedef struct Buffer{
	// buffer arrays in which the wavefield is exchanged between neighbouring PEs 
	float *** lef_to_rig, *** rig_to_lef;
	float *** top_to_bot, *** bot_to_top;
	float *** fro_to_bac, *** bac_to_fro;
  }st_buffer;
  
   typedef struct Hessian{
	// diagonal Hessian approximation
	float *** vp, *** vs, *** rho;
  }st_hessian;
  
     typedef struct Gradient{
	// Gradients for vp, vs and rho
	float *** vp, *** vs, *** rho;
  }st_gradient;
  
       typedef struct Seismogram{
		float  ** vx, ** vy, ** vz, ** p,
		** curl, ** div, ** read, ** readf;
	
  }st_seismogram;
  
         typedef struct signals{
		float ** fw;
		float  ** sectionvxdiff, ** sectionvydiff,** sectionvzdiff;
	
	}st_signals;
	
typedef struct Acquisition{
    //Acquisition
	float  ** srcpos, **srcpos_loc, **srcpos_loc_back;
	int    ** recpos, ** recpos_loc, *snum_loc,*rnum_loc;
  }st_acquisition;
/* declaration of functions */


void absorb(float *** absorb_coeff);

void absorb_PML(float *** absorb_coeffx, float *** absorb_coeffy, float *** absorb_coeffz);

void CPML_coeff(st_pml_coeff *pml_coeff);

void av_mat(st_model *mod, st_model_av *mod_av);

void av_mat_acoustic(float *** rho, float  *** rjp, float  *** rkp, float  *** rip);

void checkfd(FILE *fp, st_model *mod, float **srcpos, int nsrc, int **recpos, int ntr);

void checkfd_acoustic(FILE *fp, st_model *mod);

void checkfd_rsg(FILE *fp, st_model *mod);

void comm_ini(st_buffer *buffer, MPI_Request *req_send, MPI_Request *req_rec);

void comm_ini_s(st_buffer *buffer, MPI_Request *req_send, MPI_Request *req_rec);

void comm_ini_acoustic(st_buffer *buffer, MPI_Request *req_send, MPI_Request *req_rec);

void info(FILE *fp);

void initproc(void);
/*void initproc_block(void);*/

/*int initsour(int nxs,int nys, int  nzs, int *nxsl,int *nysl, int  *nzsl );*/

void matcopy(st_model *mod);

void matcopy_acoustic(st_model *mod);

void merge(int nsnap, int type);

void mergemod(char modfile[STRING_SIZE], int format);

void readmod(st_model *mod);

void model(st_model *mod);

void model_acoustic(st_model *mod);

void note(FILE *fp);

void  outseis(FILE *fp, FILE *fpdata, int comp, float **section,
              int **recpos, int **recpos_loc, int ntr, float **srcpos_loc,
              int nsrc, int ns, int seis_form);

float *rd_sour(int *nts,FILE *fp_source);

int plane_wave(float *** force_points);

void CPML_ini_elastic(int *xb, int *yb, int *zb);

void psource(int nt, st_stress *stress, float **srcpos_loc, float **signals, int nsrc);

void psource_acoustic(int nt, st_stress *stress, float   **srcpos_loc, float **signals, int nsrc, int *stype);

void psource_rsg(st_stress *stress, float  **srcpos_loc, float **signals, int nsrc);

void readbufs(st_stress *stress, st_buffer *buffer);

void exchange_par(void);

void exchange_s_rsg(st_stress *stress,st_buffer *buffer);

double exchange_s(st_stress *stress, st_buffer *buffer, MPI_Request *req_send, MPI_Request *req_rec);

double exchange_s_acoustic(st_stress *stress, st_buffer *buffer, MPI_Request *req_send, MPI_Request *req_rec);

void readbufv(st_velocity *vel, st_buffer *buffer);

void exchange_v_rsg(st_velocity *vel,st_buffer *buffer);

double exchange_v(st_velocity *vel, st_buffer *buffer, MPI_Request *req_send, MPI_Request *req_rec);

double exchange_Fv(float ** **vx, float ** **vy, float ** **vz, int nf, st_buffer *buffer, MPI_Request *req_send, MPI_Request *req_rec, int ntr_hess);

float readdsk(FILE *fp_in, int format);

void read_par_json(FILE *fp, char *fileinp);

void readmod_acoustic(st_model *mod, int ishot);

int **receiver(FILE *fp, int *ntr);

void saveseis(FILE *fp, st_seismogram *section, st_acquisition *acq, int ntr, int ishot, int ns, int obs, int iteration);

void seismo_acoustic(int lsamp, int ntr, int **recpos, float **sectionvx, float **sectionvy,
                     float **sectionvz, float **sectiondiv, float **sectioncurl, float **sectionp,
                     st_velocity *vel, st_stress *stress, st_model *mod);

void seismo(int lsamp, int ntr, int **recpos, st_seismogram *section, st_velocity *vel, st_stress *stress, st_model *mod);

void seismo_rsg(int lsamp, int ntr, int **recpos, float **sectionvx, float **sectionvy,
                float **sectionvz, float **sectiondiv, float **sectioncurl, float **sectionp,
                st_velocity *vel,st_stress *stress, st_model *mod);

void snap_acoustic(FILE *fp, int nt, int nsnap, int format, int type,
                   st_velocity *vel, st_stress *stress, st_model *mod,int idx, int idy, int idz, int nx1, int ny1, int nz1, int nx2,int ny2, int nz2);

void snap(FILE *fp, int nt, int nsnap, int format, int type, st_velocity *vel, st_stress *stress, st_model *mod, int idx, int idy, int idz, int nx1, int ny1, int nz1, int nx2, int ny2, int nz2);


void snap_rsg(FILE *fp, int nt, int nsnap, int format, int type,
              st_velocity *vel, st_stress *stress,st_model *mod,
              int idx, int idy, int idz, int nx1, int ny1, int nz1, int nx2,int ny2, int nz2);

void snapmerge(int nsnap);

void sources(FILE *fpsrc, int *nsrc, float **srcpos);

void pwsources(int *nsrc, float **srcpos);

void  output_source_signal(FILE *fp, float **signals, int ns, int seis_form);

int **splitrec(int **recpos,int *ntr_loc, int ntr, int *rnum_loc);

float **splitsrc(float **srcpos,int *nsrc_loc, int nsrc, int *snum_loc);

void surface(int ndepth, st_model *mod, st_pml_coeff *pml_coeff, st_pml_wfd *pml_wfd, st_visc_mem *mem, st_stress *stress, st_velocity *vel);

void surface_acoustic(int ndepth,  st_model *mod, st_stress *stress, st_velocity *vel);

/*void timing(double * time_v_update,  double * time_s_update, double * time_s_exchange, double * time_v_exchange,
            double * time_timestep, int ishot);*/

double update_s(int nx1, int nx2, int ny1, int ny2, int nz1, int nz2, st_velocity *vel, st_stress *stress, st_visc_mem *mem, st_model *mod, st_model_av *mod_av);

double update_s_CPML(int nx1, int nx2, int ny1, int ny2, int nz1, int nz2, st_velocity *vel, st_stress *stress, st_visc_mem *mem, st_model *mod, st_model_av *mod_av, st_pml_coeff *pml_coeff, st_pml_wfd *pml_wfd);

double update_s_acoustic(int nx1, int nx2, int ny1, int ny2, int nz1, int nz2,
                         st_velocity *vel, st_stress *stress, st_model *mod);

double update_s_acoustic_PML(int nx1, int nx2, int ny1, int ny2, int nz1, int nz2,
                             st_velocity *vel,st_stress *stress,st_model *mod, float ***absorb_coeffx, float ***absorb_coeffy, float ***absorb_coeffz);

void update_s_rsg(int nx1, int nx2, int ny1, int ny2, int nz1, int nz2, st_velocity *vel,st_stress *stress, st_visc_mem *mem,
                  st_model *mod,float  ***  taus, float  ***  taup, float   *eta);

double update_v(int nx1, int nx2, int ny1, int ny2, int nz1, int nz2, int nt, st_velocity *vel, st_stress *stress, st_model_av *mod_av, float **srcpos_loc, st_signals *signals, int nsrc, float *** absorb_coeff, int back);

double update_v_CPML(int nx1, int nx2, int ny1, int ny2, int nz1, int nz2, int nt, st_velocity *vel, st_stress *stress, st_model_av *mod_av, float **srcpos_loc, st_signals *signals, int nsrc, float *** absorb_coeff, int back, st_pml_coeff *pml_coeff, st_pml_wfd *pml_wfd);

double update_v_acoustic(int nx1, int nx2, int ny1, int ny2, int nz1, int nz2,st_velocity *vel,
                        st_stress *stress, float  ***  rho, float   **srcpos_loc, float **signals, int nsrc, float ***absorb_coeff, int *stype);

double update_v_acoustic_PML(int nx1, int nx2, int ny1, int ny2, int nz1, int nz2,
                             int nt, st_velocity *vel, st_stress *stress, float  ***  rho, float   **srcpos_loc,
                             float **signals, int nsrc, float ***absorb_coeffx, float ***absorb_coeffy, float ***absorb_coeffz, int *stype);

void update_v_rsg(int nx1, int nx2, int ny1, int ny2, int nz1, int nz2, int nt, 
		  st_velocity *vel,st_stress *stress, float  ***  rho,float   **srcpos_loc, float **signals, int nsrc, float ***absorb_coeff);

void wavelet(float **srcpos_loc, int nsrc, int sourceshape, float **signals);

void writebufs(st_stress *stress, st_buffer *buffer);

void writebufv(st_velocity *vel, st_buffer *buffer);

void writedsk(FILE *fp_out, float amp, int format);

void writemod(char modfile[74], st_model *mod, int format);

void writepar(FILE *fp, int ns);

/* declaration of functions for json parser in json_parser.c*/
int read_objects_from_intputfile(FILE *fp, char input_file[STRING_SIZE],char **varname_list,char **value_list);

void print_objectlist_screen(FILE *fp, int number_readobject,char **varname_list,char **value_list);

int count_occure_charinstring(char stringline[STRING_SIZE], char teststring[]);

void copy_str2str_uptochar(char string_in[STRING_SIZE], char string_out[STRING_SIZE], char teststring[]);

int get_int_from_objectlist(char string_in[STRING_SIZE], int number_readobject, int *int_buffer,
                            char **varname_list,char **value_list);

int get_float_from_objectlist(char string_in[STRING_SIZE], int number_readobject, float *double_buffer,
                              char **varname_list,char **value_list);

int get_string_from_objectlist(char string_in[STRING_SIZE], int number_readobject, char string_buffer[STRING_SIZE],
                               char **varname_list,char **value_list);

int is_string_blankspace(char string_in[STRING_SIZE]);

void remove_blankspaces_around_string(char string_in[STRING_SIZE]);

void add_object_tolist(char string_name[STRING_SIZE],char string_value[STRING_SIZE], int *number_read_object,
                       char **varname_list,char **value_list);


/* utility functions (defined in file util.c)*/
void err(char error_text[]);
void warning(char warn_text[]);
double maximum(float **a, int nx, int ny);

float *vector(int nl, int nh);
float **fmatrix(int nrl, int nrh, int ncl, int nch);

int *ivector(int nl, int nh);
int **imatrix(int nrl, int nrh, int ncl, int nch);

float ** *f3tensor(int nrl, int nrh, int ncl, int nch,int ndl, int ndh);
float ** **f4tensor(int nrl, int nrh, int ncl, int nch,int ndl, int ndh, int nvl, int nvh);

void free_vector(float *v, int nl, int nh);
void free_ivector(int *v, int nl, int nh);
void free_matrix(float **m, int nrl, int nrh, int ncl, int nch);
void free_imatrix(int **m, int nrl, int nrh, int ncl, int nch);
void free_f3tensor(float ***t, int nrl, int nrh, int ncl, int nch, int ndl,
                   int ndh);
void free_f4tensor(float ** **t, int nrl, int nrh, int ncl, int nch, int ndl, int
                   ndh, int nvl,int nvh);


double *dvector(int nl, int nh);
void free_dvector(double *v, int nl, int nh);

void readseis(int ishot, float **section, float **sectionf, int ntr, int ns, int comp);
void readseis_split(int ishot, float **section, int ntr, int *rnum_loc, int ns, int comp);
void residual(float **sectiondata, float **sectiondataf, float **section, float **sectiondiff, int ntr, int ns, float *L2, float *L2f);
void zero_wavefield(int NX, int NY, int NZ, st_velocity *vel, st_stress *stress, st_visc_mem *mem, st_pml_wfd *pml_wfd);

void gradient_F(int nx, int ny, int nz, st_freq_velocity *fw, st_freq_velocity *back, st_gradient *grad, int nt, st_model *mod, float *finv, int nf, int iteration);
void modelupdate(int nx, int ny, int nz, st_gradient *grad, st_model *mod, float **bfgsmod1, float step, float *beta, int it_group);

void zero_invers(int NX, int NY, int NZ, st_freq_velocity *fourier_fw, st_freq_velocity *fourier_back, int nfmax, int ntr_hess);
void precon_grad(int nx, int ny, int nz, st_gradient *grad, int nsrc, float **srcpos, int ntr_glob, int **recpos, float finv, int iteration, int cdf);
void outgrad(int nx,int ny,int nz,float ***grad1, float ***grad2,float ***grad3, float finv, int iteration, char outfile[STRING_SIZE]);
void outmod(int nx, int ny, int nz, st_model *mod, int iteration);
void steplength(float *L2, float *step, int iteration, int it_group);
void zero_grad(int NX, int NY, int NZ, st_gradient *grad);
void cpmodel(int nx, int ny, int nz, st_model *mod, st_model *testmod);
void conjugate(int nx, int ny, int nz, st_gradient *grad, st_gradient *grad_prior1, st_gradient *grad_prior2, float *beta, int iteration, int cdf);

double update_s_elastic(int nx1, int nx2, int ny1, int ny2, int nz1, int nz2, st_velocity *vel, st_stress *stress, st_model *mod, st_model_av *mod_av);

double update_s_CPML_elastic(int nx1, int nx2, int ny1, int ny2, int nz1, int nz2, st_velocity *vel, st_stress *stress, st_model *mod, st_model_av *mod_av, st_pml_coeff *pml_coeff, st_pml_wfd *pml_wfd);
void surface_elastic(int ndepth, st_model *mod, st_pml_coeff *pml_coeff, st_pml_wfd *pml_wfd, st_stress *stress, st_velocity *vel);

void readinv(float *finv, int *nf, int *groupnum,int *itpergroup,int nfmax);
void discfourier(int nx1, int nx2, int ny1, int ny2, int nz1, int nz2, int nt, st_velocity *vel, st_freq_velocity *fourier_vel, float *finv, int nf, int ntast, int pshot1, int back);

/*void model_gauss(st_model *mod);*/

void filt_seis(float **data,int ntr, int ns, float finv);

void model2_5(st_model *mod);

void smooth(int nx,int ny,int nz,float ***mod, float ***mod1);

void hess_F(int nx, int ny, int nz, st_freq_velocity *fw, st_freq_velocity *back, st_hessian *hessian, int nt, st_model *mod, float *finv, int nf, int ntr_hess);

void hess_apply(int nx1, int nx2, int ny1, int ny2, int nz1, int nz2, st_gradient *grad, st_hessian *hessian, float finv, int iteration);

void readhess(int nx, int ny, int nz, st_hessian *hessian, float finv, int iteration);

void lbfgs(st_gradient *grad, float *bfgsscale, float **bfgsmod, float **bfgsgrad, int iteration);
void lbfgs_savegrad(st_gradient *grad, float **bfgsgrad1);

void constant_boundary(int nx1, int nx2, int ny1, int ny2, int nz1, int nz2, st_model *mod);
