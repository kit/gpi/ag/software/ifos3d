/*------------------------------------------------------------------------
 * Copyright (C) 2015 For the list of authors, see file AUTHORS.
 *
 * This file is part of IFOS3D.
 * 
 * IFOS3D is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 2.0 of the License only.
 * 
 * IFOS3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with IFOS3D. See file COPYING and/or 
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------
 * 
 * S. Butzer 2015
 * ------------------------------------------------------------------------*/

#include "fd.h"

void constant_boundary(int nx1, int nx2, int ny1, int ny2, int nz1, int nz2, st_model *mod){


	/*extern int FREE_SURF;*/
	extern int NPROCX, NPROCY, NPROCZ, POS[4];
	extern int FW,NX, NY;

	int i, j, k,dum1,dum2;
	int dist=4;
	dum1=1;
	dum2=NY;
	

	if (POS[1]==0){	
		if (POS[2]==0){
			dum1=2;
			for (i=1;i<=FW+dist;i++){
				for (k=nz1;k<=nz2;k++){
					mod->u[1][i][k]=mod->u[1][FW+1+dist][k];
					mod->pi[1][i][k]= mod->pi[1][FW+1+dist][k];
					mod->rho[1][i][k]= mod->rho[1][FW+1+dist][k];
				}
			}
		}
		if (POS[2]==NPROCY-1){
			dum2=NY-1; 
			for (i=1;i<=FW+dist;i++){
				for (k=nz1;k<=nz2;k++){
					mod->u[NY][i][k]=mod->u[NY][FW+1+dist][k];
					mod->pi[NY][i][k]= mod->pi[NY][FW+1+dist][k];
					mod->rho[NY][i][k]= mod->rho[NY][FW+1+dist][k];
				}
			}
		}
		for (j=dum1;j<=dum2;j++){
			for (i=1;i<=FW+dist;i++){
				for (k=nz1;k<=nz2;k++){
					mod->u[j][i][k]=(3.0*mod->u[j][FW+1+dist][k]+mod->u[j-1][FW+1+dist][k]+mod->u[j+1][FW+1+dist][k]+mod->u[j][FW+1+dist][k-1]+mod->u[j][FW+1+dist][k+1]+mod->u[j][FW+2+dist][k])/8.0;
					mod->pi[j][i][k]= (3.0*mod->pi[j][FW+1+dist][k]+mod->pi[j-1][FW+1+dist][k]+mod->pi[j+1][FW+1+dist][k]+mod->pi[j][FW+1+dist][k-1]+mod->pi[j][FW+1+dist][k+1]+mod->pi[j][FW+2+dist][k])/8.0;
					mod->rho[j][i][k]= (3.0*mod->rho[j][FW+1+dist][k]+mod->rho[j-1][FW+1+dist][k]+mod->rho[j+1][FW+1+dist][k]+mod->rho[j][FW+1+dist][k-1]+mod->rho[j][FW+1+dist][k+1]+mod->rho[j][FW+2+dist][k])/8.0;
				}
			}
		}

	}
	

	if(POS[1]==NPROCX-1){
	  	if (POS[2]==0){
			dum1=2;
			for (i=nx2+1-dist;i<=nx2+FW;i++){
				for (k=nz1;k<=nz2;k++){
					mod->u[1][i][k]=mod->u[1][nx2-dist][k];
					mod->pi[1][i][k]= mod->pi[1][nx2-dist][k];
					mod->rho[1][i][k]= mod->rho[1][nx2-dist][k];
				}
			}
		}
		if (POS[2]==NPROCY-1){
			dum2=NY-1; 
			for (i=nx2+1-dist;i<=nx2+FW;i++){
				for (k=nz1;k<=nz2;k++){
					mod->u[NY][i][k]=mod->u[NY][nx2-dist][k];
					mod->pi[NY][i][k]= mod->pi[NY][nx2-dist][k];
					mod->rho[NY][i][k]= mod->rho[NY][nx2-dist][k];
				}
			}
		}
		for (j=dum1;j<=dum2;j++){
			for (i=nx2+1-dist;i<=nx2+FW;i++){
				for (k=nz1;k<=nz2;k++){
					mod->u[j][i][k]=(3.0*mod->u[j][nx2-dist][k]+mod->u[j-1][nx2-dist][k]+mod->u[j+1][nx2-dist][k]+mod->u[j][nx2-dist][k-1]+mod->u[j][nx2-dist][k+1]+mod->u[j][nx2-1-dist][k])/8.0;
					mod->pi[j][i][k]=(3.0*mod->pi[j][nx2-dist][k]+mod->pi[j-1][nx2-dist][k]+mod->pi[j+1][nx2-dist][k]+mod->pi[j][nx2-dist][k-1]+mod->pi[j][nx2-dist][k+1]+mod->pi[j][nx2-1-dist][k])/8.0;
					mod->rho[j][i][k]=(3.0*mod->rho[j][nx2-dist][k]+mod->rho[j-1][nx2-dist][k]+mod->rho[j+1][nx2-dist][k]+mod->rho[j][nx2-dist][k-1]+mod->rho[j][nx2-dist][k+1]+mod->rho[j][nx2-1-dist][k])/8.0;
				}
			}
		}
	}

	if(POS[3]==0){
		if (POS[2]==0){
			dum1=2;
			for (i=1;i<=NX;i++){
				for (k=1;k<=FW+dist;k++){
					mod->u[1][i][k]=mod->u[1][i][FW+1+dist];
					mod->pi[1][i][k]= mod->pi[1][i][FW+1+dist];
					mod->rho[1][i][k]= mod->rho[1][i][FW+1+dist];
				}
			}
		}
		if (POS[2]==NPROCY-1){
			dum2=NY-1; 
			for (i=1;i<=NX;i++){
				for (k=1;k<=FW+dist;k++){
					mod->u[NY][i][k]=mod->u[NY][i][FW+1+dist];
					mod->pi[NY][i][k]= mod->pi[NY][i][FW+1+dist];	
					mod->rho[NY][i][k]= mod->rho[NY][i][FW+1+dist];
				}
			}
		}
		if(POS[1]==0){
			for (j=dum1;j<=dum2;j++){
				for (i=1;i<=FW+dist;i++){
					for (k=1;k<=FW+dist;k++){
						mod->u[j][i][k]=mod->u[j][i][FW+1+dist];
						mod->pi[j][i][k]=mod->pi[j][i][FW+1+dist];
						mod->rho[j][i][k]=mod->rho[j][i][FW+1+dist];
					}
				}
			}
		}
		if(POS[1]==NPROCX-1){
			for (j=dum1;j<=dum2;j++){
				for (i=nx2+1-dist;i<=nx2+FW;i++){
					for (k=1;k<=FW+dist;k++){
						mod->u[j][i][k]=mod->u[j][i][FW+1+dist];
						mod->pi[j][i][k]=mod->pi[j][i][FW+1+dist];
						mod->rho[j][i][k]=mod->rho[j][i][FW+1+dist];
					}
				}
			}
		}
		
		
		
		for (j=dum1;j<=dum2;j++){
			for (i=nx1;i<=nx2;i++){
				for (k=1;k<=FW+dist;k++){
					mod->u[j][i][k]=(3.0*mod->u[j][i][FW+1+dist]+mod->u[j-1][i][FW+1+dist]+mod->u[j+1][i][FW+1+dist]+mod->u[j][i-1][FW+1+dist]+mod->u[j][i+1][FW+1+dist]+mod->u[j][i][FW+2+dist])/8.0;
					mod->pi[j][i][k]=(3.0*mod->pi[j][i][FW+1+dist]+mod->pi[j-1][i][FW+1+dist]+mod->pi[j+1][i][FW+1+dist]+mod->pi[j][i-1][FW+1+dist]+mod->pi[j][i+1][FW+1+dist]+mod->pi[j][i][FW+2+dist])/8.0;
					mod->rho[j][i][k]=(3.0*mod->rho[j][i][FW+1+dist]+mod->rho[j-1][i][FW+1+dist]+mod->rho[j+1][i][FW+1+dist]+mod->rho[j][i-1][FW+1+dist]+mod->rho[j][i+1][FW+1+dist]+mod->rho[j][i][FW+2+dist])/8.0;
				}
			}
		}
	
	
	}


	if(POS[3]==NPROCZ-1){	
		if (POS[2]==0){
			dum1=2;
			for (i=1;i<=NX;i++){
				for (k=nz2+1-dist;k<=nz2+FW;k++){
					mod->u[1][i][k]=mod->u[1][i][nz2-dist];
					mod->pi[1][i][k]= mod->pi[1][i][nz2-dist];
					mod->rho[1][i][k]= mod->rho[1][i][nz2-dist];
				}
			}
		}
		if (POS[2]==NPROCY-1){
			dum2=NY-1; 
			for (i=1;i<=NX;i++){
				for (k=nz2+1-dist;k<=nz2+FW;k++){
					mod->u[NY][i][k]=mod->u[NY][i][nz2-dist];
					mod->pi[NY][i][k]= mod->pi[NY][i][nz2-dist];
					mod->rho[NY][i][k]= mod->rho[NY][i][nz2-dist];
				}
			}
		}
		if(POS[1]==0){
			for (j=dum1;j<=dum2;j++){
				for (i=1;i<=FW+dist;i++){
					for (k=nz2+1-dist;k<=nz2+FW;k++){
						mod->u[j][i][k]=mod->u[j][i][nz2-dist];
						mod->pi[j][i][k]=mod->pi[j][i][nz2-dist];
						mod->rho[j][i][k]=mod->rho[j][i][nz2-dist];
					}
				}
			}
		}
		if(POS[1]==NPROCX-1){
			for (j=dum1;j<=dum2;j++){
				for (i=nx2+1-dist;i<=nx2+FW;i++){
					for (k=nz2+1-dist;k<=nz2+FW;k++){
						mod->u[j][i][k]=mod->u[j][i][nz2-dist];
						mod->pi[j][i][k]=mod->pi[j][i][nz2-dist];
						mod->rho[j][i][k]=mod->rho[j][i][nz2-dist];
					}
				}
			}
		}
		for (j=dum1;j<=dum2;j++){
			for (i=nx1;i<=nx2;i++){
				for (k=nz2+1-dist;k<=nz2+FW;k++){
					mod->u[j][i][k]=(3.0*mod->u[j][i][nz2-dist]+mod->u[j-1][i][nz2-dist]+mod->u[j+1][i][nz2-dist]+mod->u[j][i-1][nz2-dist]+mod->u[j][i+1][nz2-dist]+mod->u[j][i][nz2-1-dist])/8.0;
					mod->pi[j][i][k]=(3.0*mod->pi[j][i][nz2-dist]+mod->pi[j-1][i][nz2-dist]+mod->pi[j+1][i][nz2-dist]+mod->pi[j][i-1][nz2-dist]+mod->pi[j][i+1][nz2-dist]+mod->pi[j][i][nz2-dist-1])/8.0;
					mod->rho[j][i][k]=(3.0*mod->rho[j][i][nz2-dist]+mod->rho[j-1][i][nz2-dist]+mod->rho[j+1][i][nz2-dist]+mod->rho[j][i-1][nz2-dist]+mod->rho[j][i+1][nz2-dist]+mod->rho[j][i][nz2-dist-1])/8.0;
				}
			}
		}

	}


}
