/*------------------------------------------------------------------------
 * Copyright (C) 2015 For the list of authors, see file AUTHORS.
 *
 * This file is part of IFOS3D.
 * 
 * IFOS3D is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 2.0 of the License only.
 * 
 * IFOS3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with IFOS3D. See file COPYING and/or 
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
--------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
 *   stress free surface condition
 *  ----------------------------------------------------------------------*/

#include "fd.h"

void surface(int ndepth, st_model *mod, st_pml_coeff *pml_coeff,st_pml_wfd *pml_wfd, st_visc_mem *mem, st_stress *stress, 
 st_velocity *vel){

	int i, k ,j, fdoh,m,h1;
	float  vxx, vyy, vzz;
	float b, d, e, f, g, h, dthalbe; /*dh24x*/
	

	extern int NX, NZ, L, FDORDER, FW, ABS_TYPE, FDCOEFF, POS[4],NPROCX,NPROCZ;
	register float b1, b2, b3, b4, b5, b6;
	extern float DT, DX, DY, DZ;


	j=ndepth;     /* The free surface is located exactly in y=(ndepth)*dh meter!! */

	dthalbe=DT/2.0;
	fdoh=FDORDER/2;
        
	
	switch (FDORDER){
	case 2 :
	
	/*dh24x=DX*24.0;*/
	b1=9.0/8.0; b2=-1.0/24.0; /* Taylor coefficients */
		
	if(FDCOEFF==2){b1=1.1382; b2=-0.046414;} /* Holberg coefficients E=0.1 %*/ 
		
	for (k=1;k<=NZ;k++){
		for (i=1;i<=NX;i++){
		
		
			/*Mirroring the components of the stress tensor to make
					  a stress free surface (method of imaging, Levander, 1988)*/
			stress->syy[j][i][k]=mem->ryy[j][i][k]=0.0;
			
                        stress->syy[j-1][i][k]=-stress->syy[j+1][i][k];
                        stress->sxy[j-1][i][k]=-stress->sxy[j][i][k];
                        stress->sxy[j-2][i][k]=-stress->sxy[j+1][i][k];
                        stress->syz[j-1][i][k]=-stress->syz[j][i][k];
                        stress->syz[j-2][i][k]=-stress->syz[j+1][i][k];


			/* in the case of using several relaxation mechanisms all
						 memory-variables corresponding to syy must be set to zero
					  for (l=1;l<=L;l++) ryy(i,j,k,l)=0.0;*/


			/* now updating the stress components sxx, szz and the memory-
						 variables rxx,rzz at the free surface */

			/* first calculate spatial derivatives of components of particle velocities */
			vxx = (b1*(vel->vx[j][i][k]-vel->vx[j][i-1][k])+b2*(vel->vx[j][i+1][k]-vel->vx[j][i-2][k]))/DX;
                        vyy = (b1*(vel->vy[j][i][k]-vel->vy[j-1][i][k])+b2*(vel->vy[j+1][i][k]-vel->vy[j-2][i][k]))/DY;
			vzz = (b1*(vel->vz[j][i][k]-vel->vz[j][i][k-1])+b2*(vel->vz[j][i][k+1]-vel->vz[j][i][k-2]))/DZ;
			
			/*vxx=(-vel->vx[j][i+1][k]+27.0*(vel->vx[j][i][k]-vel->vx[j][i-1][k])+vel->vx[j][i-2][k])/(dh24x);
                        vyy=(-vel->vy[j+1][i][k]+27.0*(vel->vy[j][i][k]-vel->vy[j-1][i][k])+vel->vy[j-2][i][k])/(dh24x);
                        vzz=(-vel->vz[j][i][k+1]+27.0*(vel->vz[j][i][k]-vel->vz[j][i][k-1])+vel->vz[j][i][k-2])/(dh24x);*/
			
			if (ABS_TYPE==1){
					if((POS[1]==0) && (i<=FW)){
						pml_wfd->psi_vxx[j][i][k] = pml_coeff->b_x[i] * pml_wfd->psi_vxx[j][i][k] + pml_coeff->a_x[i] * vxx;
						vxx = vxx / pml_coeff->K_x[i] + pml_wfd->psi_vxx[j][i][k];
					}
					if((POS[1]==NPROCX-1) && (i>=NX-FW+1)){
						h1 = i-NX+2*FW;

						pml_wfd->psi_vxx[j][h1][k] = pml_coeff->b_x[h1] * pml_wfd->psi_vxx[j][h1][k] + pml_coeff->a_x[h1] * vxx;
						vxx = vxx /pml_coeff->K_x[h1] + pml_wfd->psi_vxx[j][h1][k];
					}
					if((POS[3]==0) && (k<=FW)){
						pml_wfd->psi_vzz[j][i][k] = pml_coeff->b_z[k] * pml_wfd->psi_vzz[j][i][k] + pml_coeff->a_z[k] * vzz;
						vzz = vzz / pml_coeff->K_z[k] + pml_wfd->psi_vzz[j][i][k];					  
					}
					if((POS[3]==NPROCZ-1) && (k>=NZ-FW+1)){

						h1 = (k-NZ+2*FW);
						pml_wfd->psi_vzz[j][i][h1] = pml_coeff->b_z[h1] * pml_wfd->psi_vzz[j][i][h1] + pml_coeff->a_z[h1] * vzz;
						vzz = vzz / pml_coeff->K_z[h1] + pml_wfd->psi_vzz[j][i][h1];
					}
					
				}
			
                          /* partially updating sxx and szz in the same way*/
                        f=mod->u[j][i][k]*2.0*(1.0+L*mod->taus[j][i][k]);
                        g=mod->pi[j][i][k]*(1.0+L*mod->taup[j][i][k]);
                        h=-(DT*(g-f)*(g-f)*(vxx+vzz)/g)-(DT*(g-f)*vyy);
                        stress->sxx[j][i][k]+=h-(dthalbe*mem->rxx[j][i][k]);
                        stress->szz[j][i][k]+=h-(dthalbe*mem->rzz[j][i][k]);

                        /* updating the memory-variable rxx, rzz at the free surface */
                        b=mod->eta[1]/(1.0+(mod->eta[1]*0.5));
                        d=2.0*mod->u[j][i][k]*mod->taus[j][i][k];
                        e=mod->pi[j][i][k]*mod->taup[j][i][k];
                        h=b*(((d-e)*((f/g)-1.0)*(vxx+vzz))-((d-e)*vyy));
                        mem->rxx[j][i][k]+=h;
                        mem->rzz[j][i][k]+=h;

                        /*completely updating the stresses sxx and szz */
                        stress->sxx[j][i][k]+=(dthalbe*mem->rxx[j][i][k]);
                        stress->szz[j][i][k]+=(dthalbe*mem->rzz[j][i][k]);

			
		}
	}
   break;
   
   case 4 :
		
		b1=9.0/8.0; b2=-1.0/24.0; /* Taylor coefficients */
		
		if(FDCOEFF==2){
		b1=1.1382; b2=-0.046414;} /* Holberg coefficients E=0.1 %*/ 
	
	for (k=1;k<=NZ;k++){
		for (i=1;i<=NX;i++){


			/*Mirroring the components of the stress tensor to make
					  a stress free surface (method of imaging, Levander, 1988)*/
			stress->syy[j][i][k]=mem->ryy[j][i][k]=0.0;
			
			for (m=1; m<=fdoh; m++) {
			  stress->syy[j-m][i][k]=-stress->syy[j+m][i][k];
			  stress->sxy[j-m][i][k]=-stress->sxy[j+m-1][i][k];
			  stress->syz[j-m][i][k]=-stress->syz[j+m-1][i][k];
			}


			/* in the case of using several relaxation mechanisms all
						 memory-variables corresponding to syy must be set to zero
					  for (l=1;l<=L;l++) ryy(i,j,k,l)=0.0;*/


			/* now updating the stress components sxx, szz and the memory-
						 variables rxx,rzz at the free surface */

			/* first calculate spatial derivatives of components
						 of particle velocities */
	
                        vxx = (b1*(vel->vx[j][i][k]-vel->vx[j][i-1][k])+b2*(vel->vx[j][i+1][k]-vel->vx[j][i-2][k]))/DX;
			vyy = (b1*(vel->vy[j][i][k]-vel->vy[j-1][i][k])+b2*(vel->vy[j+1][i][k]-vel->vy[j-2][i][k]))/DY;
			vzz = (b1*(vel->vz[j][i][k]-vel->vz[j][i][k-1])+b2*(vel->vz[j][i][k+1]-vel->vz[j][i][k-2]))/DZ;
			
			if (ABS_TYPE==1){
					if((POS[1]==0) && (i<=FW)){
						pml_wfd->psi_vxx[j][i][k] = pml_coeff->b_x[i] * pml_wfd->psi_vxx[j][i][k] + pml_coeff->a_x[i] * vxx;
						vxx = vxx / pml_coeff->K_x[i] + pml_wfd->psi_vxx[j][i][k];
					}
					if((POS[1]==NPROCX-1) && (i>=NX-FW+1)){
						h1 = i-NX+2*FW;

						pml_wfd->psi_vxx[j][h1][k] = pml_coeff->b_x[h1] * pml_wfd->psi_vxx[j][h1][k] + pml_coeff->a_x[h1] * vxx;
						vxx = vxx /pml_coeff->K_x[h1] + pml_wfd->psi_vxx[j][h1][k];
					}
					if((POS[3]==0) && (k<=FW)){
						pml_wfd->psi_vzz[j][i][k] = pml_coeff->b_z[k] * pml_wfd->psi_vzz[j][i][k] + pml_coeff->a_z[k] * vzz;
						vzz = vzz / pml_coeff->K_z[k] + pml_wfd->psi_vzz[j][i][k];					  
					}
					if((POS[3]==NPROCZ-1) && (k>=NZ-FW+1)){

						h1 = (k-NZ+2*FW);
						pml_wfd->psi_vzz[j][i][h1] = pml_coeff->b_z[h1] * pml_wfd->psi_vzz[j][i][h1] + pml_coeff->a_z[h1] * vzz;
						vzz = vzz / pml_coeff->K_z[h1] + pml_wfd->psi_vzz[j][i][h1];
					}
					
				}
			
			/* partially updating sxx and szz in the same way*/
			f=mod->u[j][i][k]*2.0*(1.0+L*mod->taus[j][i][k]);
			g=mod->pi[j][i][k]*(1.0+L*mod->taup[j][i][k]);
			h=-(DT*(g-f)*(g-f)*(vxx+vzz)/g)-(DT*(g-f)*vyy);
			stress->sxx[j][i][k]+=h-(dthalbe*mem->rxx[j][i][k]);
			stress->szz[j][i][k]+=h-(dthalbe*mem->rzz[j][i][k]);

			/* updating the memory-variable rxx, rzz at the free surface */
			b=mod->eta[1]/(1.0+(mod->eta[1]*0.5));
			d=2.0*mod->u[j][i][k]*mod->taus[j][i][k];
			e=mod->pi[j][i][k]*mod->taup[j][i][k];
			h=b*(((d-e)*((f/g)-1.0)*(vxx+vzz))-((d-e)*vyy));
			mem->rxx[j][i][k]+=h;
			mem->rzz[j][i][k]+=h;

			/*completely updating the stresses sxx and szz */
			stress->sxx[j][i][k]+=(dthalbe*mem->rxx[j][i][k]);
			stress->szz[j][i][k]+=(dthalbe*mem->rzz[j][i][k]);
		}
	}
   break;
   
   case 6 :
		
	      b1=75.0/64.0; b2=-25.0/384.0; b3=3.0/640.0; /* Taylor coefficients */
	      if(FDCOEFF==2){
	      b1=1.1965; b2=-0.078804; b3=0.0081781;}   /* Holberg coefficients E=0.1 %*/
	
	for (k=1;k<=NZ;k++){
		for (i=1;i<=NX;i++){


			/*Mirroring the components of the stress tensor to make
					  a stress free surface (method of imaging, Levander, 1988)*/
			stress->syy[j][i][k]=mem->ryy[j][i][k]=0.0;
			
			for (m=1; m<=fdoh; m++) {
			  stress->syy[j-m][i][k]=-stress->syy[j+m][i][k];
			  stress->sxy[j-m][i][k]=-stress->sxy[j+m-1][i][k];
			  stress->syz[j-m][i][k]=-stress->syz[j+m-1][i][k];
			}


			/* in the case of using several relaxation mechanisms all
						 memory-variables corresponding to syy must be set to zero
					  for (l=1;l<=L;l++) ryy(i,j,k,l)=0.0;*/


			/* now updating the stress components sxx, szz and the memory-
						 variables rxx,rzz at the free surface */

			/* first calculate spatial derivatives of components
						 of particle velocities */
	
                        vxx = (b1*(vel->vx[j][i][k]-vel->vx[j][i-1][k])+
				       b2*(vel->vx[j][i+1][k]-vel->vx[j][i-2][k])+
		                       b3*(vel->vx[j][i+2][k]-vel->vx[j][i-3][k]))/DX;
			
		        vyy = (b1*(vel->vy[j][i][k]-vel->vy[j-1][i][k])+
				       b2*(vel->vy[j+1][i][k]-vel->vy[j-2][i][k])+
				       b3*(vel->vy[j+2][i][k]-vel->vy[j-3][i][k]))/DY;	       
			
			vzz = (b1*(vel->vz[j][i][k]-vel->vz[j][i][k-1])+
				       b2*(vel->vz[j][i][k+1]-vel->vz[j][i][k-2])+
				       b3*(vel->vz[j][i][k+2]-vel->vz[j][i][k-3]))/DZ;
			
			/* partially updating sxx and szz in the same way*/
			f=mod->u[j][i][k]*2.0*(1.0+L*mod->taus[j][i][k]);
			g=mod->pi[j][i][k]*(1.0+L*mod->taup[j][i][k]);
			h=-(DT*(g-f)*(g-f)*(vxx+vzz)/g)-(DT*(g-f)*vyy);
			stress->sxx[j][i][k]+=h-(dthalbe*mem->rxx[j][i][k]);
			stress->szz[j][i][k]+=h-(dthalbe*mem->rzz[j][i][k]);

			/* updating the memory-variable rxx, rzz at the free surface */
			b=mod->eta[1]/(1.0+(mod->eta[1]*0.5));
			d=2.0*mod->u[j][i][k]*mod->taus[j][i][k];
			e=mod->pi[j][i][k]*mod->taup[j][i][k];
			h=b*(((d-e)*((f/g)-1.0)*(vxx+vzz))-((d-e)*vyy));
			mem->rxx[j][i][k]+=h;
			mem->rzz[j][i][k]+=h;

			/*completely updating the stresses sxx and szz */
			stress->sxx[j][i][k]+=(dthalbe*mem->rxx[j][i][k]);
			stress->szz[j][i][k]+=(dthalbe*mem->rzz[j][i][k]);
		}
	}
   break;
   
   case 8 :
		
	  b1=1225.0/1024.0; b2=-245.0/3072.0; b3=49.0/5120.0; b4=-5.0/7168.0; /* Taylor coefficients */
	  
	  if(FDCOEFF==2){
	  b1=1.2257; b2=-0.099537; b3=0.018063; b4=-0.0026274;} /* Holberg coefficients E=0.1 %*/
	
	for (k=1;k<=NZ;k++){
		for (i=1;i<=NX;i++){


			/*Mirroring the components of the stress tensor to make
					  a stress free surface (method of imaging, Levander, 1988)*/
			stress->syy[j][i][k]=mem->ryy[j][i][k]=0.0;
			
			for (m=1; m<=fdoh; m++) {
			  stress->syy[j-m][i][k]=-stress->syy[j+m][i][k];
			  stress->sxy[j-m][i][k]=-stress->sxy[j+m-1][i][k];
			  stress->syz[j-m][i][k]=-stress->syz[j+m-1][i][k];
			}


			/* in the case of using several relaxation mechanisms all
						 memory-variables corresponding to syy must be set to zero
					  for (l=1;l<=L;l++) ryy(i,j,k,l)=0.0;*/


			/* now updating the stress components sxx, szz and the memory-
						 variables rxx,rzz at the free surface */

			/* first calculate spatial derivatives of components
						 of particle velocities */
	
                        vxx = (b1*(vel->vx[j][i][k]-vel->vx[j][i-1][k])+
				       b2*(vel->vx[j][i+1][k]-vel->vx[j][i-2][k])+
		                       b3*(vel->vx[j][i+2][k]-vel->vx[j][i-3][k])+
				       b4*(vel->vx[j][i+3][k]-vel->vx[j][i-4][k]))/DX;
				       
			vyy = (b1*(vel->vy[j][i][k]-vel->vy[j-1][i][k])+
				       b2*(vel->vy[j+1][i][k]-vel->vy[j-2][i][k])+
				       b3*(vel->vy[j+2][i][k]-vel->vy[j-3][i][k])+
				       b4*(vel->vy[j+3][i][k]-vel->vy[j-4][i][k]))/DY;
				       
			vzz = (b1*(vel->vz[j][i][k]-vel->vz[j][i][k-1])+
				       b2*(vel->vz[j][i][k+1]-vel->vz[j][i][k-2])+
				       b3*(vel->vz[j][i][k+2]-vel->vz[j][i][k-3])+
				       b4*(vel->vz[j][i][k+3]-vel->vz[j][i][k-4]))/DZ;	       	       
			
			/* partially updating sxx and szz in the same way*/
			f=mod->u[j][i][k]*2.0*(1.0+L*mod->taus[j][i][k]);
			g=mod->pi[j][i][k]*(1.0+L*mod->taup[j][i][k]);
			h=-(DT*(g-f)*(g-f)*(vxx+vzz)/g)-(DT*(g-f)*vyy);
			stress->sxx[j][i][k]+=h-(dthalbe*mem->rxx[j][i][k]);
			stress->szz[j][i][k]+=h-(dthalbe*mem->rzz[j][i][k]);

			/* updating the memory-variable rxx, rzz at the free surface */
			b=mod->eta[1]/(1.0+(mod->eta[1]*0.5));
			d=2.0*mod->u[j][i][k]*mod->taus[j][i][k];
			e=mod->pi[j][i][k]*mod->taup[j][i][k];
			h=b*(((d-e)*((f/g)-1.0)*(vxx+vzz))-((d-e)*vyy));
			mem->rxx[j][i][k]+=h;
			mem->rzz[j][i][k]+=h;

			/*completely updating the stresses sxx and szz */
			stress->sxx[j][i][k]+=(dthalbe*mem->rxx[j][i][k]);
			stress->szz[j][i][k]+=(dthalbe*mem->rzz[j][i][k]);
		}
	}
   break;	

   case 10 :
		
	  b1=19845.0/16384.0; b2=-735.0/8192.0; b3=567.0/40960.0; b4=-405.0/229376.0; b5=35.0/294912.0; /* Taylor coefficients */
	  
	  if(FDCOEFF==2){
	  b1=1.2415; b2=-0.11231; b3=0.026191; b4=-0.0064682; b5=0.001191;} /* Holberg coefficients E=0.1 %*/
	
	for (k=1;k<=NZ;k++){
		for (i=1;i<=NX;i++){


			/*Mirroring the components of the stress tensor to make
					  a stress free surface (method of imaging, Levander, 1988)*/
			stress->syy[j][i][k]=mem->ryy[j][i][k]=0.0;
			
			for (m=1; m<=fdoh; m++) {
			  stress->syy[j-m][i][k]=-stress->syy[j+m][i][k];
			  stress->sxy[j-m][i][k]=-stress->sxy[j+m-1][i][k];
			  stress->syz[j-m][i][k]=-stress->syz[j+m-1][i][k];
			}


			/* in the case of using several relaxation mechanisms all
						 memory-variables corresponding to syy must be set to zero
					  for (l=1;l<=L;l++) ryy(i,j,k,l)=0.0;*/


			/* now updating the stress components sxx, szz and the memory-
						 variables rxx,rzz at the free surface */

			/* first calculate spatial derivatives of components
						 of particle velocities */
	
                                vxx = (b1*(vel->vx[j][i][k]-vel->vx[j][i-1][k])+
				       b2*(vel->vx[j][i+1][k]-vel->vx[j][i-2][k])+
		                       b3*(vel->vx[j][i+2][k]-vel->vx[j][i-3][k])+
				       b4*(vel->vx[j][i+3][k]-vel->vx[j][i-4][k])+
				       b5*(vel->vx[j][i+4][k]-vel->vx[j][i-5][k]))/DX;   
				       
				vyy = (b1*(vel->vy[j][i][k]-vel->vy[j-1][i][k])+
				       b2*(vel->vy[j+1][i][k]-vel->vy[j-2][i][k])+
				       b3*(vel->vy[j+2][i][k]-vel->vy[j-3][i][k])+
				       b4*(vel->vy[j+3][i][k]-vel->vy[j-4][i][k])+
				       b5*(vel->vy[j+4][i][k]-vel->vy[j-5][i][k]))/DY;
				       
			        vzz = (b1*(vel->vz[j][i][k]-vel->vz[j][i][k-1])+
				       b2*(vel->vz[j][i][k+1]-vel->vz[j][i][k-2])+
				       b3*(vel->vz[j][i][k+2]-vel->vz[j][i][k-3])+
				       b4*(vel->vz[j][i][k+3]-vel->vz[j][i][k-4])+
				       b5*(vel->vz[j][i][k+4]-vel->vz[j][i][k-5]))/DZ;	           	       
			
			/* partially updating sxx and szz in the same way*/
			f=mod->u[j][i][k]*2.0*(1.0+L*mod->taus[j][i][k]);
			g=mod->pi[j][i][k]*(1.0+L*mod->taup[j][i][k]);
			h=-(DT*(g-f)*(g-f)*(vxx+vzz)/g)-(DT*(g-f)*vyy);
			stress->sxx[j][i][k]+=h-(dthalbe*mem->rxx[j][i][k]);
			stress->szz[j][i][k]+=h-(dthalbe*mem->rzz[j][i][k]);

			/* updating the memory-variable rxx, rzz at the free surface */
			b=mod->eta[1]/(1.0+(mod->eta[1]*0.5));
			d=2.0*mod->u[j][i][k]*mod->taus[j][i][k];
			e=mod->pi[j][i][k]*mod->taup[j][i][k];
			h=b*(((d-e)*((f/g)-1.0)*(vxx+vzz))-((d-e)*vyy));
			mem->rxx[j][i][k]+=h;
			mem->rzz[j][i][k]+=h;

			/*completely updating the stresses sxx and szz */
			stress->sxx[j][i][k]+=(dthalbe*mem->rxx[j][i][k]);
			stress->szz[j][i][k]+=(dthalbe*mem->rzz[j][i][k]);
		}
	}
   break;
   
      case 12 :
		
	  /* Taylor coefficients */
	  b1=160083.0/131072.0; b2=-12705.0/131072.0; b3=22869.0/1310720.0; 
	  b4=-5445.0/1835008.0; b5=847.0/2359296.0; b6=-63.0/2883584;
		
	  /* Holberg coefficients E=0.1 %*/
	  if(FDCOEFF==2){
	  b1=1.2508; b2=-0.12034; b3=0.032131; b4=-0.010142; b5=0.0029857; b6=-0.00066667;}
	
	for (k=1;k<=NZ;k++){
		for (i=1;i<=NX;i++){


			/*Mirroring the components of the stress tensor to make
					  a stress free surface (method of imaging, Levander, 1988)*/
			stress->syy[j][i][k]=mem->ryy[j][i][k]=0.0;
			
			for (m=1; m<=fdoh; m++) {
			  stress->syy[j-m][i][k]=-stress->syy[j+m][i][k];
			  stress->sxy[j-m][i][k]=-stress->sxy[j+m-1][i][k];
			  stress->syz[j-m][i][k]=-stress->syz[j+m-1][i][k];
			}


			/* in the case of using several relaxation mechanisms all
						 memory-variables corresponding to syy must be set to zero
					  for (l=1;l<=L;l++) ryy(i,j,k,l)=0.0;*/


			/* now updating the stress components sxx, szz and the memory-
						 variables rxx,rzz at the free surface */

			/* first calculate spatial derivatives of components
						 of particle velocities */
	
                                vxx = (b1*(vel->vx[j][i][k]-vel->vx[j][i-1][k])+
				       b2*(vel->vx[j][i+1][k]-vel->vx[j][i-2][k])+
		                       b3*(vel->vx[j][i+2][k]-vel->vx[j][i-3][k])+
				       b4*(vel->vx[j][i+3][k]-vel->vx[j][i-4][k])+
				       b5*(vel->vx[j][i+4][k]-vel->vx[j][i-5][k])+
				       b6*(vel->vx[j][i+5][k]-vel->vx[j][i-6][k]))/DX;	
				       
				vyy = (b1*(vel->vy[j][i][k]-vel->vy[j-1][i][k])+
				       b2*(vel->vy[j+1][i][k]-vel->vy[j-2][i][k])+
				       b3*(vel->vy[j+2][i][k]-vel->vy[j-3][i][k])+
				       b4*(vel->vy[j+3][i][k]-vel->vy[j-4][i][k])+
				       b5*(vel->vy[j+4][i][k]-vel->vy[j-5][i][k])+
				       b6*(vel->vy[j+5][i][k]-vel->vy[j-6][i][k]))/DY; 
				       
				vzz = (b1*(vel->vz[j][i][k]-vel->vz[j][i][k-1])+
				       b2*(vel->vz[j][i][k+1]-vel->vz[j][i][k-2])+
				       b3*(vel->vz[j][i][k+2]-vel->vz[j][i][k-3])+
				       b4*(vel->vz[j][i][k+3]-vel->vz[j][i][k-4])+
				       b5*(vel->vz[j][i][k+4]-vel->vz[j][i][k-5])+
				       b6*(vel->vz[j][i][k+5]-vel->vz[j][i][k-6]))/DZ;      	       
			
			/* partially updating sxx and szz in the same way*/
			f=mod->u[j][i][k]*2.0*(1.0+L*mod->taus[j][i][k]);
			g=mod->pi[j][i][k]*(1.0+L*mod->taup[j][i][k]);
			h=-(DT*(g-f)*(g-f)*(vxx+vzz)/g)-(DT*(g-f)*vyy);
			stress->sxx[j][i][k]+=h-(dthalbe*mem->rxx[j][i][k]);
			stress->szz[j][i][k]+=h-(dthalbe*mem->rzz[j][i][k]);

			/* updating the memory-variable rxx, rzz at the free surface */
			b=mod->eta[1]/(1.0+(mod->eta[1]*0.5));
			d=2.0*mod->u[j][i][k]*mod->taus[j][i][k];
			e=mod->pi[j][i][k]*mod->taup[j][i][k];
			h=b*(((d-e)*((f/g)-1.0)*(vxx+vzz))-((d-e)*vyy));
			mem->rxx[j][i][k]+=h;
			mem->rzz[j][i][k]+=h;

			/*completely updating the stresses sxx and szz */
			stress->sxx[j][i][k]+=(dthalbe*mem->rxx[j][i][k]);
			stress->szz[j][i][k]+=(dthalbe*mem->rzz[j][i][k]);
		}
	}
   break;
}
}
