/*------------------------------------------------------------------------
 * Copyright (C) 2015 For the list of authors, see file AUTHORS.
 *
 * This file is part of IFOS3D.
 * 
 * IFOS3D is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 2.0 of the License only.
 * 
 * IFOS3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with IFOS3D. See file COPYING and/or 
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------
 * calculates the conjugate gradient direction (MORA, 1987)
 * S. Butzer 2015
 * ------------------------------------------------------------------------*/

#include "fd.h"

void conjugate(int nx,int ny,int nz, st_gradient *grad, st_gradient *grad_prior1,st_gradient *grad_prior2, float *beta, int iteration,int cdf){

	extern FILE *FP;

	int i,j,k;
	float betadum[6],betadum1[6];
	
	betadum[0]=0.0; betadum[1]=0.0;
	betadum[2]=0.0; betadum[3]=0.0;
	betadum[4]=0.0; betadum[5]=0.0;
	
	beta[0]=0.0; beta[1]=0.0; beta[2]=0.0;

	fprintf(FP,"\n Calculate conjugate gradient ");
	
	if(cdf==1){
		for (j=1;j<=ny;j++){
			for (i=1;i<=nx;i++){
				for (k=1;k<=nz;k++){
				grad_prior1->vp[j][i][k]=grad->vp[j][i][k]; 
				grad_prior1->vs[j][i][k]=grad->vs[j][i][k];
				grad_prior1->rho[j][i][k]=grad->rho[j][i][k];
				grad_prior2->vp[j][i][k]=grad->vp[j][i][k]; 
				grad_prior2->vs[j][i][k]=grad->vs[j][i][k];
				grad_prior2->rho[j][i][k]=grad->rho[j][i][k];
				}
			}
		}
	}

	else{
		/*Calculation of beta for conjugate gradient*/
		for (j=1;j<=ny;j++){
			for (i=1;i<=nx;i++){
				for (k=1;k<=nz;k++){		
					betadum[0]+=grad->vp[j][i][k]*(grad->vp[j][i][k]-grad_prior1->vp[j][i][k]);
					betadum[1]+=grad_prior1->vp[j][i][k]*grad_prior1->vp[j][i][k];
					betadum[2]+=grad->vs[j][i][k]*(grad->vs[j][i][k]-grad_prior1->vs[j][i][k]);
					betadum[3]+=grad_prior1->vs[j][i][k]*grad_prior1->vs[j][i][k];
					betadum[4]+=grad->rho[j][i][k]*(grad->rho[j][i][k]-grad_prior1->rho[j][i][k]);
					betadum[5]+=grad_prior1->rho[j][i][k]*grad_prior1->rho[j][i][k];
					
					/*save preconditioned gradient for next iteration*/
					grad_prior1->vp[j][i][k]=grad->vp[j][i][k]; 
					grad_prior1->vs[j][i][k]=grad->vs[j][i][k];
					grad_prior1->rho[j][i][k]=grad->rho[j][i][k];			
				}
			}
		}

		MPI_Allreduce(&betadum,&betadum1,6,MPI_FLOAT,MPI_SUM,MPI_COMM_WORLD);
		MPI_Barrier(MPI_COMM_WORLD);
		
		if(cdf==0){
		if(fabs(betadum1[1])>0.0) beta[0]=betadum1[0]/betadum1[1];
		if(fabs(betadum1[3])>0.0) beta[1]=betadum1[2]/betadum1[3];
		if(fabs(betadum1[5])>0.0) beta[2]=betadum1[4]/betadum1[5];
		fprintf(FP,"\n beta[1]=%e, beta[2]=%e, beta[3]=%e\n",beta[0],beta[1],beta[2] );}
		
		if(beta[0]<0)beta[0]=0.0;
		if(beta[0]<0)beta[1]=0.0;
		if(beta[0]<0)beta[2]=0.0;
		if(beta[0]>0.5)beta[0]=0.5;
		if(beta[0]>0.5)beta[1]=0.5;
		if(beta[0]>0.5)beta[2]=0.5;
		
		/*Calculation of conjugate gradient direction (not normalised)*/
		for (j=1;j<=ny;j++){
			for (i=1;i<=nx;i++){
				for (k=1;k<=nz;k++){
					grad->vp[j][i][k]=grad_prior2->vp[j][i][k]*beta[0]+grad->vp[j][i][k];
					grad->vs[j][i][k]=grad_prior2->vs[j][i][k]*beta[1]+grad->vs[j][i][k];
					grad->rho[j][i][k]=grad_prior2->rho[j][i][k]*beta[2]+grad->rho[j][i][k];
					
					/*save conjugate gradient for next iteration*/
					grad_prior2->vp[j][i][k]=grad->vp[j][i][k]; 
					grad_prior2->vs[j][i][k]=grad->vs[j][i][k];
					grad_prior2->rho[j][i][k]=grad->rho[j][i][k];
				}
			}
		}
	}
		
}
