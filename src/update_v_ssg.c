/*------------------------------------------------------------------------
 * Copyright (C) 2015 For the list of authors, see file AUTHORS.
 *
 * This file is part of IFOS3D.
 * 
 * IFOS3D is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 2.0 of the License only.
 * 
 * IFOS3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with IFOS3D. See file COPYING and/or 
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
--------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
 *   updating velocity values by a staggered grid finite difference scheme of 
 *   nth order accuracy in space and second order accuracy in time
 *  ----------------------------------------------------------------------*/

#include "fd.h"

double update_v(int nx1, int nx2, int ny1, int ny2, int nz1, int nz2,
int nt, st_velocity *vel, st_stress *stress,  st_model_av *mod_av,
float **  srcpos_loc, st_signals *signals, int nsrc, float *** absorb_coeff, int back){
	
	
	/*extern FILE *FP;*/
	extern float DT, DX, DY, DZ, ALPHA, BETA;
	double time=0.0; /*, time1=0.0;*/
	/*double time2=0.0;*/
	extern int  FDORDER,  ABS_TYPE, FDCOEFF; /*MYID,LOG,*/

	int i, j, k, l;
	float  amp, alpha_rad, beta_rad;
	float b1, b2, b3, b4, b5, b6, dx, dy, dz;
	float sxx_x, sxy_y, sxz_z, syy_y, sxy_x, syz_z;
	float szz_z, sxz_x, syz_y;
		
	/*unsigned int * test_float;*/
	
	

	/*if (LOG)
	if (MYID==0) time1=MPI_Wtime();*/


        switch (FDORDER){
	
	case 2 :
	 
	        dx=DT/DX;
		dy=DT/DY;
		dz=DT/DZ;
		
		for (j=ny1;j<=ny2;j++){
			for (i=nx1;i<=nx2;i++){
				for (k=nz1;k<=nz2;k++){
				
				sxx_x = dx*(stress->sxx[j][i+1][k]-stress->sxx[j][i][k]);
				sxy_y = dy*(stress->sxy[j][i][k]-stress->sxy[j-1][i][k]);
				sxz_z = dz*(stress->sxz[j][i][k]-stress->sxz[j][i][k-1]); /* Backward operator */
				
				/* updating components of particle velocities */
				vel->vx[j][i][k]+= ((sxx_x + sxy_y +sxz_z)/mod_av->rip[j][i][k]);
				
				syy_y = dy*(stress->syy[j+1][i][k]-stress->syy[j][i][k]);
				sxy_x = dx*(stress->sxy[j][i][k]-stress->sxy[j][i-1][k]);
				syz_z = dz*(stress->syz[j][i][k]-stress->syz[j][i][k-1]);
				

				vel->vy[j][i][k]+= ((syy_y + sxy_x + syz_z)/mod_av->rjp[j][i][k]);

				szz_z = dz*(stress->szz[j][i][k+1]-stress->szz[j][i][k]);
				sxz_x = dx*(stress->sxz[j][i][k]-stress->sxz[j][i-1][k]);
				syz_y = dy*(stress->syz[j][i][k]-stress->syz[j-1][i][k]);
				 
				
				vel->vz[j][i][k]+= ((szz_z + sxz_x + syz_y)/mod_av->rkp[j][i][k]);

			}
		}
	}
		
     break;
	
	case 4 : 
	
	        dx=DT/DX;
		dy=DT/DY;
		dz=DT/DZ;
		
		b1=9.0/8.0; b2=-1.0/24.0; /* Taylor coefficients*/
		if(FDCOEFF==2){
		b1=1.1382; b2=-0.046414;} /* Holberg coefficients E=0.1 %*/
		
		for (j=ny1;j<=ny2;j++){
			for (i=nx1;i<=nx2;i++){
				for (k=nz1;k<=nz2;k++){
				
				sxx_x = dx*(b1*(stress->sxx[j][i+1][k]-stress->sxx[j][i][k])+b2*(stress->sxx[j][i+2][k]-stress->sxx[j][i-1][k]));
				sxy_y = dy*(b1*(stress->sxy[j][i][k]-stress->sxy[j-1][i][k])+b2*(stress->sxy[j+1][i][k]-stress->sxy[j-2][i][k]));
				sxz_z = dz*(b1*(stress->sxz[j][i][k]-stress->sxz[j][i][k-1])+b2*(stress->sxz[j][i][k+1]-stress->sxz[j][i][k-2]));
				
				/* updating components of particle velocities */
				vel->vx[j][i][k]+= (sxx_x + sxy_y +sxz_z)/mod_av->rip[j][i][k];
				
				syy_y = dy*(b1*(stress->syy[j+1][i][k]-stress->syy[j][i][k])+b2*(stress->syy[j+2][i][k]-stress->syy[j-1][i][k]));
				sxy_x = dx*(b1*(stress->sxy[j][i][k]-stress->sxy[j][i-1][k])+b2*(stress->sxy[j][i+1][k]-stress->sxy[j][i-2][k]));
				syz_z = dz*(b1*(stress->syz[j][i][k]-stress->syz[j][i][k-1])+b2*(stress->syz[j][i][k+1]-stress->syz[j][i][k-2]));
				

				vel->vy[j][i][k]+= (syy_y + sxy_x + syz_z)/mod_av->rjp[j][i][k];

				szz_z = dz*(b1*(stress->szz[j][i][k+1]-stress->szz[j][i][k])+b2*(stress->szz[j][i][k+2]-stress->szz[j][i][k-1]));
				sxz_x = dx*(b1*(stress->sxz[j][i][k]-stress->sxz[j][i-1][k])+b2*(stress->sxz[j][i+1][k]-stress->sxz[j][i-2][k]));
				syz_y = dy*(b1*(stress->syz[j][i][k]-stress->syz[j-1][i][k])+b2*(stress->syz[j+1][i][k]-stress->syz[j-2][i][k]));
				 
				
				vel->vz[j][i][k]+= (szz_z + sxz_x + syz_y)/mod_av->rkp[j][i][k];
			}
		}
	}
		
     break;
     
     case 6 : 
     
                dx=DT/DX;
		dy=DT/DY;
		dz=DT/DZ;
	        	
		b1=75.0/64.0; b2=-25.0/384.0; b3=3.0/640.0; /* Taylor coefficients*/
		if(FDCOEFF==2){
		b1=1.1965; b2=-0.078804; b3=0.0081781;}   /* Holberg coefficients E=0.1 %*/
		
		for (j=ny1;j<=ny2;j++){
			for (i=nx1;i<=nx2;i++){
				for (k=nz1;k<=nz2;k++){
				
				sxx_x = dx*(b1*(stress->sxx[j][i+1][k]-stress->sxx[j][i][k])+
				        b2*(stress->sxx[j][i+2][k]-stress->sxx[j][i-1][k])+
					b3*(stress->sxx[j][i+3][k]-stress->sxx[j][i-2][k]));
					
				sxy_y = dy*(b1*(stress->sxy[j][i][k]-stress->sxy[j-1][i][k])+
				        b2*(stress->sxy[j+1][i][k]-stress->sxy[j-2][i][k])+
					b3*(stress->sxy[j+2][i][k]-stress->sxy[j-3][i][k]));
					
                                sxz_z = dz*(b1*(stress->sxz[j][i][k]-stress->sxz[j][i][k-1])+
				        b2*(stress->sxz[j][i][k+1]-stress->sxz[j][i][k-2])+
					b3*(stress->sxz[j][i][k+2]-stress->sxz[j][i][k-3]));
		
				
				/* updating components of particle velocities */
				vel->vx[j][i][k]+= (sxx_x + sxy_y +sxz_z)/mod_av->rip[j][i][k];
				
				syy_y = dy*(b1*(stress->syy[j+1][i][k]-stress->syy[j][i][k])+
				        b2*(stress->syy[j+2][i][k]-stress->syy[j-1][i][k])+
					b3*(stress->syy[j+3][i][k]-stress->syy[j-2][i][k]));
					
				sxy_x = dx*(b1*(stress->sxy[j][i][k]-stress->sxy[j][i-1][k])+
				        b2*(stress->sxy[j][i+1][k]-stress->sxy[j][i-2][k])+
					b3*(stress->sxy[j][i+2][k]-stress->sxy[j][i-3][k]));
					
				syz_z = dz*(b1*(stress->syz[j][i][k]-stress->syz[j][i][k-1])+
				        b2*(stress->syz[j][i][k+1]-stress->syz[j][i][k-2])+
					b3*(stress->syz[j][i][k+2]-stress->syz[j][i][k-3]));
				

				vel->vy[j][i][k]+= (syy_y + sxy_x + syz_z)/mod_av->rjp[j][i][k];

				szz_z = dz*(b1*(stress->szz[j][i][k+1]-stress->szz[j][i][k])+
				        b2*(stress->szz[j][i][k+2]-stress->szz[j][i][k-1])+
					b3*(stress->szz[j][i][k+3]-stress->szz[j][i][k-2]));
					
				sxz_x = dx*(b1*(stress->sxz[j][i][k]-stress->sxz[j][i-1][k])+
				        b2*(stress->sxz[j][i+1][k]-stress->sxz[j][i-2][k])+
					b3*(stress->sxz[j][i+2][k]-stress->sxz[j][i-3][k]));
					
					
				syz_y = dy*(b1*(stress->syz[j][i][k]-stress->syz[j-1][i][k])+
				        b2*(stress->syz[j+1][i][k]-stress->syz[j-2][i][k])+
					b3*(stress->syz[j+2][i][k]-stress->syz[j-3][i][k]));
				 
				
				vel->vz[j][i][k]+= (szz_z + sxz_x + syz_y)/mod_av->rkp[j][i][k];

			}
		}
	}
		
     break;

case 8 : 
	        
		dx=DT/DX;
		dy=DT/DY;
		dz=DT/DZ;
	
		 b1=1225.0/1024.0; b2=-245.0/3072.0; b3=49.0/5120.0; b4=-5.0/7168.0; /* Taylor coefficients*/
		 if(FDCOEFF==2){
		 b1=1.2257; b2=-0.099537; b3=0.018063; b4=-0.0026274;} /* Holberg coefficients E=0.1 %*/
		
		for (j=ny1;j<=ny2;j++){
			for (i=nx1;i<=nx2;i++){
				for (k=nz1;k<=nz2;k++){
				
				sxx_x = dx*(b1*(stress->sxx[j][i+1][k]-stress->sxx[j][i][k])+
				        b2*(stress->sxx[j][i+2][k]-stress->sxx[j][i-1][k])+
					b3*(stress->sxx[j][i+3][k]-stress->sxx[j][i-2][k])+
					b4*(stress->sxx[j][i+4][k]-stress->sxx[j][i-3][k]));
					
				sxy_y = dy*(b1*(stress->sxy[j][i][k]-stress->sxy[j-1][i][k])+
				        b2*(stress->sxy[j+1][i][k]-stress->sxy[j-2][i][k])+
					b3*(stress->sxy[j+2][i][k]-stress->sxy[j-3][i][k])+
					b4*(stress->sxy[j+3][i][k]-stress->sxy[j-4][i][k]));
					
				sxz_z = dz*(b1*(stress->sxz[j][i][k]-stress->sxz[j][i][k-1])+
				        b2*(stress->sxz[j][i][k+1]-stress->sxz[j][i][k-2])+
					b3*(stress->sxz[j][i][k+2]-stress->sxz[j][i][k-3])+
					b4*(stress->sxz[j][i][k+3]-stress->sxz[j][i][k-4]));
									
				/* updating components of particle velocities */
				vel->vx[j][i][k]+= (sxx_x + sxy_y +sxz_z)/mod_av->rip[j][i][k];
				
				syy_y = dy*(b1*(stress->syy[j+1][i][k]-stress->syy[j][i][k])+
				        b2*(stress->syy[j+2][i][k]-stress->syy[j-1][i][k])+
					b3*(stress->syy[j+3][i][k]-stress->syy[j-2][i][k])+
					b4*(stress->syy[j+4][i][k]-stress->syy[j-3][i][k]));
					
				sxy_x = dx*(b1*(stress->sxy[j][i][k]-stress->sxy[j][i-1][k])+
				        b2*(stress->sxy[j][i+1][k]-stress->sxy[j][i-2][k])+
					b3*(stress->sxy[j][i+2][k]-stress->sxy[j][i-3][k])+
					b4*(stress->sxy[j][i+3][k]-stress->sxy[j][i-4][k]));
					
				syz_z = dz*(b1*(stress->syz[j][i][k]-stress->syz[j][i][k-1])+
				        b2*(stress->syz[j][i][k+1]-stress->syz[j][i][k-2])+
					b3*(stress->syz[j][i][k+2]-stress->syz[j][i][k-3])+
					b4*(stress->syz[j][i][k+3]-stress->syz[j][i][k-4]));
				

				vel->vy[j][i][k]+= (syy_y + sxy_x + syz_z)/mod_av->rjp[j][i][k];
				/*test_float = (unsigned int*) (void*) &vel->vy[j][i][k];
			if(((*test_float & 0x7f800000) == 0x0) && ((*test_float & 0x007fffff) != 0x0))fprintf(FP,"Achtung:ILLEGALER FLOAT %4.2f",vel->vy[1000000][100000][1000000]);*/

				szz_z = dz*(b1*(stress->szz[j][i][k+1]-stress->szz[j][i][k])+
				        b2*(stress->szz[j][i][k+2]-stress->szz[j][i][k-1])+
					b3*(stress->szz[j][i][k+3]-stress->szz[j][i][k-2])+
					b4*(stress->szz[j][i][k+4]-stress->szz[j][i][k-3]));
					
				sxz_x = dx*(b1*(stress->sxz[j][i][k]-stress->sxz[j][i-1][k])+
				        b2*(stress->sxz[j][i+1][k]-stress->sxz[j][i-2][k])+
					b3*(stress->sxz[j][i+2][k]-stress->sxz[j][i-3][k])+
					b4*(stress->sxz[j][i+3][k]-stress->sxz[j][i-4][k]));
					
					
				syz_y = dy*(b1*(stress->syz[j][i][k]-stress->syz[j-1][i][k])+
				        b2*(stress->syz[j+1][i][k]-stress->syz[j-2][i][k])+
					b3*(stress->syz[j+2][i][k]-stress->syz[j-3][i][k])+
					b4*(stress->syz[j+3][i][k]-stress->syz[j-4][i][k]));
				 
				
				vel->vz[j][i][k]+= (szz_z + sxz_x + syz_y)/mod_av->rkp[j][i][k];

			}
		}
	}
		
     break;

case 10 : 
                
		dx=DT/DX;
		dy=DT/DY;
		dz=DT/DZ;
	        
		
		b1=19845.0/16384.0; b2=-735.0/8192.0; b3=567.0/40960.0; b4=-405.0/229376.0; b5=35.0/294912.0; /* Taylor Coefficients*/
		if(FDCOEFF==2){
		b1=1.2415; b2=-0.11231; b3=0.026191; b4=-0.0064682; b5=0.001191;} /* Holberg coefficients E=0.1 %*/
		
		for (j=ny1;j<=ny2;j++){
			for (i=nx1;i<=nx2;i++){
				for (k=nz1;k<=nz2;k++){
				
				sxx_x = dx*(b1*(stress->sxx[j][i+1][k]-stress->sxx[j][i][k])+
				        b2*(stress->sxx[j][i+2][k]-stress->sxx[j][i-1][k])+
					b3*(stress->sxx[j][i+3][k]-stress->sxx[j][i-2][k])+
					b4*(stress->sxx[j][i+4][k]-stress->sxx[j][i-3][k])+
					b5*(stress->sxx[j][i+5][k]-stress->sxx[j][i-4][k]));
					
				sxy_y = dy*(b1*(stress->sxy[j][i][k]-stress->sxy[j-1][i][k])+
				        b2*(stress->sxy[j+1][i][k]-stress->sxy[j-2][i][k])+
					b3*(stress->sxy[j+2][i][k]-stress->sxy[j-3][i][k])+
					b4*(stress->sxy[j+3][i][k]-stress->sxy[j-4][i][k])+
					b5*(stress->sxy[j+4][i][k]-stress->sxy[j-5][i][k]));
					
                                sxz_z = dz*(b1*(stress->sxz[j][i][k]-stress->sxz[j][i][k-1])+
				        b2*(stress->sxz[j][i][k+1]-stress->sxz[j][i][k-2])+
					b3*(stress->sxz[j][i][k+2]-stress->sxz[j][i][k-3])+
					b4*(stress->sxz[j][i][k+3]-stress->sxz[j][i][k-4])+
					b5*(stress->sxz[j][i][k+4]-stress->sxz[j][i][k-5]));
				
				
				/* updating components of particle velocities */
				vel->vx[j][i][k]+= (sxx_x + sxy_y +sxz_z)/mod_av->rip[j][i][k];
				
				syy_y = dy*(b1*(stress->syy[j+1][i][k]-stress->syy[j][i][k])+
				        b2*(stress->syy[j+2][i][k]-stress->syy[j-1][i][k])+
					b3*(stress->syy[j+3][i][k]-stress->syy[j-2][i][k])+
					b4*(stress->syy[j+4][i][k]-stress->syy[j-3][i][k])+
					b5*(stress->syy[j+5][i][k]-stress->syy[j-4][i][k]));
					
				sxy_x = dx*(b1*(stress->sxy[j][i][k]-stress->sxy[j][i-1][k])+
				        b2*(stress->sxy[j][i+1][k]-stress->sxy[j][i-2][k])+
					b3*(stress->sxy[j][i+2][k]-stress->sxy[j][i-3][k])+
					b4*(stress->sxy[j][i+3][k]-stress->sxy[j][i-4][k])+
					b5*(stress->sxy[j][i+4][k]-stress->sxy[j][i-5][k]));
					
				syz_z = dz*(b1*(stress->syz[j][i][k]-stress->syz[j][i][k-1])+
				        b2*(stress->syz[j][i][k+1]-stress->syz[j][i][k-2])+
					b3*(stress->syz[j][i][k+2]-stress->syz[j][i][k-3])+
					b4*(stress->syz[j][i][k+3]-stress->syz[j][i][k-4])+
					b5*(stress->syz[j][i][k+4]-stress->syz[j][i][k-5]));
				

				vel->vy[j][i][k]+= (syy_y + sxy_x + syz_z)/mod_av->rjp[j][i][k];

				szz_z = dz*(b1*(stress->szz[j][i][k+1]-stress->szz[j][i][k])+
				        b2*(stress->szz[j][i][k+2]-stress->szz[j][i][k-1])+
					b3*(stress->szz[j][i][k+3]-stress->szz[j][i][k-2])+
					b4*(stress->szz[j][i][k+4]-stress->szz[j][i][k-3])+
					b5*(stress->szz[j][i][k+5]-stress->szz[j][i][k-4]));
					
				sxz_x = dx*(b1*(stress->sxz[j][i][k]-stress->sxz[j][i-1][k])+
				        b2*(stress->sxz[j][i+1][k]-stress->sxz[j][i-2][k])+
					b3*(stress->sxz[j][i+2][k]-stress->sxz[j][i-3][k])+
					b4*(stress->sxz[j][i+3][k]-stress->sxz[j][i-4][k])+
					b5*(stress->sxz[j][i+4][k]-stress->sxz[j][i-5][k]));
					
					
				syz_y = dy*(b1*(stress->syz[j][i][k]-stress->syz[j-1][i][k])+
				        b2*(stress->syz[j+1][i][k]-stress->syz[j-2][i][k])+
					b3*(stress->syz[j+2][i][k]-stress->syz[j-3][i][k])+
					b4*(stress->syz[j+3][i][k]-stress->syz[j-4][i][k])+
					b5*(stress->syz[j+4][i][k]-stress->syz[j-5][i][k]));
				 
				
				vel->vz[j][i][k]+= (szz_z + sxz_x + syz_y)/mod_av->rkp[j][i][k];

			}
		}
	}
		
     break;

case 12 : 
	        dx=DT/DX;
		dy=DT/DY;
		dz=DT/DZ;
		
		/* Taylor coefficients */
		b1=160083.0/131072.0; b2=-12705.0/131072.0; b3=22869.0/1310720.0; 
		b4=-5445.0/1835008.0; b5=847.0/2359296.0; b6=-63.0/2883584;
		
		/* Holberg coefficients E=0.1 %*/
		if(FDCOEFF==2){
		b1=1.2508; b2=-0.12034; b3=0.032131; b4=-0.010142; b5=0.0029857; b6=-0.00066667;}
		
		
		for (j=ny1;j<=ny2;j++){
			for (i=nx1;i<=nx2;i++){
				for (k=nz1;k<=nz2;k++){
				
				sxx_x = dx*(b1*(stress->sxx[j][i+1][k]-stress->sxx[j][i][k])+
				        b2*(stress->sxx[j][i+2][k]-stress->sxx[j][i-1][k])+
					b3*(stress->sxx[j][i+3][k]-stress->sxx[j][i-2][k])+
					b4*(stress->sxx[j][i+4][k]-stress->sxx[j][i-3][k])+
					b5*(stress->sxx[j][i+5][k]-stress->sxx[j][i-4][k])+
					b6*(stress->sxx[j][i+6][k]-stress->sxx[j][i-5][k]));
					
				sxy_y = dy*(b1*(stress->sxy[j][i][k]-stress->sxy[j-1][i][k])+
				        b2*(stress->sxy[j+1][i][k]-stress->sxy[j-2][i][k])+
					b3*(stress->sxy[j+2][i][k]-stress->sxy[j-3][i][k])+
					b4*(stress->sxy[j+3][i][k]-stress->sxy[j-4][i][k])+
					b5*(stress->sxy[j+4][i][k]-stress->sxy[j-5][i][k])+
					b6*(stress->sxy[j+5][i][k]-stress->sxy[j-6][i][k]));

				sxz_z = dz*(b1*(stress->sxy[j][i][k]-stress->sxy[j][i][k-1])+
				        b2*(stress->sxy[j][i][k+1]-stress->sxy[j][i][k-2])+
					b3*(stress->sxy[j][i][k+2]-stress->sxy[j][i][k-3])+
					b4*(stress->sxy[j][i][k+3]-stress->sxy[j][i][k-4])+
					b5*(stress->sxy[j][i][k+4]-stress->sxy[j][i][k-5])+
					b6*(stress->sxy[j][i][k+5]-stress->sxy[j][i][k-6]));
					
									
				/* updating components of particle velocities */
				vel->vx[j][i][k]+= (sxx_x + sxy_y +sxz_z)/mod_av->rip[j][i][k];
				
				syy_y = dy*(b1*(stress->syy[j+1][i][k]-stress->syy[j][i][k])+
				        b2*(stress->syy[j+2][i][k]-stress->syy[j-1][i][k])+
					b3*(stress->syy[j+3][i][k]-stress->syy[j-2][i][k])+
					b4*(stress->syy[j+4][i][k]-stress->syy[j-3][i][k])+
					b5*(stress->syy[j+5][i][k]-stress->syy[j-4][i][k])+
					b6*(stress->syy[j+6][i][k]-stress->syy[j-5][i][k]));
					
				sxy_x = dx*(b1*(stress->sxy[j][i][k]-stress->sxy[j][i-1][k])+
				        b2*(stress->sxy[j][i+1][k]-stress->sxy[j][i-2][k])+
					b3*(stress->sxy[j][i+2][k]-stress->sxy[j][i-3][k])+
					b4*(stress->sxy[j][i+3][k]-stress->sxy[j][i-4][k])+
					b5*(stress->sxy[j][i+4][k]-stress->sxy[j][i-5][k])+
					b6*(stress->sxy[j][i+5][k]-stress->sxy[j][i-6][k]));
					
				syz_z = dz*(b1*(stress->syz[j][i][k]-stress->syz[j][i][k-1])+
				        b2*(stress->syz[j][i][k+1]-stress->syz[j][i][k-2])+
					b3*(stress->syz[j][i][k+2]-stress->syz[j][i][k-3])+
					b4*(stress->syz[j][i][k+3]-stress->syz[j][i][k-4])+
					b5*(stress->syz[j][i][k+4]-stress->syz[j][i][k-5])+
					b6*(stress->syz[j][i][k+5]-stress->syz[j][i][k-6]));
					
				

				vel->vy[j][i][k]+= (syy_y + sxy_x + syz_z)/mod_av->rjp[j][i][k];

				szz_z = dz*(b1*(stress->szz[j][i][k+1]-stress->szz[j][i][k])+
				        b2*(stress->szz[j][i][k+2]-stress->szz[j][i][k-1])+
					b3*(stress->szz[j][i][k+3]-stress->szz[j][i][k-2])+
					b4*(stress->szz[j][i][k+4]-stress->szz[j][i][k-3])+
					b5*(stress->szz[j][i][k+5]-stress->szz[j][i][k-4])+
					b6*(stress->szz[j][i][k+6]-stress->szz[j][i][k-5]));
					
				sxz_x = dx*(b1*(stress->sxz[j][i][k]-stress->sxz[j][i-1][k])+
				        b2*(stress->sxz[j][i+1][k]-stress->sxz[j][i-2][k])+
					b3*(stress->sxz[j][i+2][k]-stress->sxz[j][i-3][k])+
					b4*(stress->sxz[j][i+3][k]-stress->sxz[j][i-4][k])+
					b5*(stress->sxz[j][i+4][k]-stress->sxz[j][i-5][k])+
                                        b6*(stress->sxz[j][i+5][k]-stress->sxz[j][i-6][k]));
					
					
				syz_y = dy*(b1*(stress->syz[j][i][k]-stress->syz[j-1][i][k])+
				        b2*(stress->syz[j+1][i][k]-stress->syz[j-2][i][k])+
					b3*(stress->syz[j+2][i][k]-stress->syz[j-3][i][k])+
					b4*(stress->syz[j+3][i][k]-stress->syz[j-4][i][k])+
					b5*(stress->syz[j+4][i][k]-stress->syz[j-5][i][k])+
					b6*(stress->syz[j+5][i][k]-stress->syz[j-6][i][k]));
				 
				
				vel->vz[j][i][k]+= (szz_z + sxz_x + syz_y)/mod_av->rkp[j][i][k];

			}
		}
	}
		
     break;
     
     }
	/* Adding body force components to corresponding particle velocities */
	
	if(back==0){
	for (l=1;l<=nsrc;l++) {
		i=(int)srcpos_loc[1][l];
		j=(int)srcpos_loc[2][l];
		k=(int)srcpos_loc[3][l];
		amp=DT*signals->fw[l][nt]/(DX*DY*DZ);
					

		switch ((int)srcpos_loc[7][l]){
		case 2 : 
			vel->vx[j][i][k]+=amp*mod_av->rip[j][i][k]; 
			
			/*/(mod_av->rip[j][i][k]);  *//* single force in x , DX^3 because of force density*/
			
			/*test_float = (unsigned int*) (void*) &vel->vx[j][i][k];
			if(((*test_float & 0x7f800000) == 0x0) && ((*test_float & 0x007fffff) != 0x0))fprintf(FP,"Achtung:ILLEGALER FLOAT");*/
			
			break;
		case 3 : 
			vel->vz[j][i][k]+=amp*mod_av->rjp[j][i][k];  /* single force in z  */
			break;
		case 4 : 
			vel->vy[j][i][k]+=amp*mod_av->rkp[j][i][k];  /* single force in y, vertical direction*/
			break;
		case 5 : 
			alpha_rad=ALPHA*PI/180; /* custom force */
			beta_rad=BETA*PI/180;
			vel->vx[j][i][k]+=cos(beta_rad)*cos(alpha_rad)*amp;
			vel->vy[j][i][k]+=sin(alpha_rad)*amp;
			vel->vz[j][i][k]+=sin(beta_rad)*cos(alpha_rad)*amp;
			break;
		}
	}
	
	
	
	}
	
	if (back==1){
	  
		for (l=1;l<=nsrc;l++) {		
			i=(int)srcpos_loc[1][l];
			j=(int)srcpos_loc[2][l];
			k=(int)srcpos_loc[3][l];
			vel->vx[j][i][k]+=signals->sectionvxdiff[l][nt];
			vel->vz[j][i][k]+=signals->sectionvzdiff[l][nt];
			vel->vy[j][i][k]+=signals->sectionvydiff[l][nt];
		}
	}


				

	/* absorbing boundary condition (exponential damping) */

	if (ABS_TYPE==2){
		for (j=ny1;j<=ny2;j++){
			for (i=nx1;i<=nx2;i++){
				for (k=nz1;k<=nz2;k++){
				vel->vx[j][i][k]*=absorb_coeff[j][i][k];
				vel->vy[j][i][k]*=absorb_coeff[j][i][k];
				vel->vz[j][i][k]*=absorb_coeff[j][i][k];
				stress->sxy[j][i][k]*=absorb_coeff[j][i][k];
				stress->syz[j][i][k]*=absorb_coeff[j][i][k];
				stress->sxz[j][i][k]*=absorb_coeff[j][i][k];
				stress->sxx[j][i][k]*=absorb_coeff[j][i][k];
				stress->syy[j][i][k]*=absorb_coeff[j][i][k];
				stress->szz[j][i][k]*=absorb_coeff[j][i][k];

			        }
		        }
	        }
        }
	
        /*if (LOG)
	if (MYID==0){
		time2=MPI_Wtime();
		time=time2-time1;
		fprintf(FP," Real time for particle velocity update: \t %4.2f s.\n",time);
	}*/
	return time;

}
