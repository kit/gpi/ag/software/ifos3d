/*------------------------------------------------------------------------
 * Copyright (C) 2015 For the list of authors, see file AUTHORS.
 *
 * This file is part of IFOS3D.
 * 
 * IFOS3D is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 2.0 of the License only.
 * 
 * IFOS3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with IFOS3D. See file COPYING and/or 
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
--------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
 * updating stress values in the CPML-boundaries (4th order spatial FD sheme)
 * elastic version
 * Implementation acoording to Komatitsch, D. and Martin, R.(2007): "An unsplit convolutional perfectly matched layer improved at grazing incidence for the seismic wave equation", geophysics, Vol.72, No.5 
 * similar to fdveps (2D) 
 * S.Dunkl (November 2010)
 *  ----------------------------------------------------------------------*/

#include "fd.h"

double update_s_CPML_elastic(int nx1, int nx2, int ny1, int ny2, int nz1, int nz2, st_velocity *vel,
		st_stress *stress,  st_model *mod, st_model_av *mod_av,
		st_pml_coeff *pml_coeff,st_pml_wfd *pml_wfd){

	
	extern float DT, DX, DY, DZ;
	extern int  FDCOEFF;    /*MYID, LOG,*/
	extern int FREE_SURF;
	extern int NPROCX, NPROCY, NPROCZ, POS[4];
	extern int FW, NY, NZ;

	int i, j, k, h1;
	double time=0.0;  /*, time1=0.0;*/
	float vxx=0.0,vxy=0.0,vxz=0.0,vyx=0.0,vyy=0.0,vyz=0.0,vzx=0.0,vzy=0.0,vzz=0.0;
	float vxyyx,vyzzy,vxzzx,vxxyyzz,vyyzz,vxxzz,vxxyy;
	float g,f,fipjp,fjpkp,fipkp;
	float b1, b2;

	/*if (LOG)
		if (MYID==0) time1=MPI_Wtime();*/

	b1=9.0/8.0; b2=-1.0/24.0; /* Taylor coefficients */
	if(FDCOEFF==2){
		b1=1.1382; b2=-0.046414;} /* Holberg coefficients E=0.1 %*/ 

	if (POS[1]==0){
		for (j=1;j<=NY;j++){
			for (i=1;i<=FW;i++){
				for (k=1;k<=NZ;k++){

					vxx = (b1*(vel->vx[j][i][k]-vel->vx[j][i-1][k])+b2*(vel->vx[j][i+1][k]-vel->vx[j][i-2][k]))/DX;
					vxy = (b1*(vel->vx[j+1][i][k]-vel->vx[j][i][k])+b2*(vel->vx[j+2][i][k]-vel->vx[j-1][i][k]))/DY;
					vxz = (b1*(vel->vx[j][i][k+1]-vel->vx[j][i][k])+b2*(vel->vx[j][i][k+2]-vel->vx[j][i][k-1]))/DZ;
					vyx = (b1*(vel->vy[j][i+1][k]-vel->vy[j][i][k])+b2*(vel->vy[j][i+2][k]-vel->vy[j][i-1][k]))/DX;
					vyy = (b1*(vel->vy[j][i][k]-vel->vy[j-1][i][k])+b2*(vel->vy[j+1][i][k]-vel->vy[j-2][i][k]))/DY;
					vyz = (b1*(vel->vy[j][i][k+1]-vel->vy[j][i][k])+b2*(vel->vy[j][i][k+2]-vel->vy[j][i][k-1]))/DZ;
					vzx = (b1*(vel->vz[j][i+1][k]-vel->vz[j][i][k])+b2*(vel->vz[j][i+2][k]-vel->vz[j][i-1][k]))/DX;
					vzy = (b1*(vel->vz[j+1][i][k]-vel->vz[j][i][k])+b2*(vel->vz[j+2][i][k]-vel->vz[j-1][i][k]))/DY;
					vzz = (b1*(vel->vz[j][i][k]-vel->vz[j][i][k-1])+b2*(vel->vz[j][i][k+1]-vel->vz[j][i][k-2]))/DZ;

					pml_wfd->psi_vxx[j][i][k] = pml_coeff->b_x[i] * pml_wfd->psi_vxx[j][i][k] + pml_coeff->a_x[i] * vxx;
					vxx = vxx / pml_coeff->K_x[i] + pml_wfd->psi_vxx[j][i][k];
					pml_wfd->psi_vyx[j][i][k] = pml_coeff->b_x_half[i] * pml_wfd->psi_vyx[j][i][k] + pml_coeff->a_x_half[i] * vyx;
					vyx = vyx / pml_coeff->K_x_half[i] + pml_wfd->psi_vyx[j][i][k];
					pml_wfd->psi_vzx[j][i][k] = pml_coeff->b_x_half[i] * pml_wfd->psi_vzx[j][i][k] + pml_coeff->a_x_half[i] * vzx;
					vzx = vzx / pml_coeff->K_x_half[i] + pml_wfd->psi_vzx[j][i][k];


					if((POS[2]==0 && FREE_SURF==0) && (j<=FW)){

						pml_wfd->psi_vxy[j][i][k] = pml_coeff->b_y_half[j] * pml_wfd->psi_vxy[j][i][k] + pml_coeff->a_y_half[j] * vxy;
						vxy = vxy / pml_coeff->K_y_half[j] + pml_wfd->psi_vxy[j][i][k];
						pml_wfd->psi_vyy[j][i][k] = pml_coeff->b_y[j] * pml_wfd->psi_vyy[j][i][k] + pml_coeff->a_y[j] * vyy;
						vyy = vyy / pml_coeff->K_y[j] + pml_wfd->psi_vyy[j][i][k];
						pml_wfd->psi_vzy[j][i][k] = pml_coeff->b_y_half[j] * pml_wfd->psi_vzy[j][i][k] + pml_coeff->a_y_half[j] * vzy;
						vzy = vzy / pml_coeff->K_y_half[j] + pml_wfd->psi_vzy[j][i][k]; }

					if((POS[2]==NPROCY-1) && (j>=ny2+1)){
						h1 = (j-ny2+FW);

						pml_wfd->psi_vxy[h1][i][k] = pml_coeff->b_y_half[h1] * pml_wfd->psi_vxy[h1][i][k] + pml_coeff->a_y_half[h1] * vxy;
						vxy = vxy / pml_coeff->K_y_half[h1] + pml_wfd->psi_vxy[h1][i][k];
						pml_wfd->psi_vyy[h1][i][k] = pml_coeff->b_y[h1] * pml_wfd->psi_vyy[h1][i][k] + pml_coeff->a_y[h1] * vyy;
						vyy = vyy / pml_coeff->K_y[h1] + pml_wfd->psi_vyy[h1][i][k];
						pml_wfd->psi_vzy[h1][i][k] = pml_coeff->b_y_half[h1] * pml_wfd->psi_vzy[h1][i][k] + pml_coeff->a_y_half[h1] * vzy;
						vzy = vzy / pml_coeff->K_y_half[h1] + pml_wfd->psi_vzy[h1][i][k]; }

					if((POS[3]==0) && (k<=FW)){

						pml_wfd->psi_vxz[j][i][k] = pml_coeff->b_z_half[k] * pml_wfd->psi_vxz[j][i][k] + pml_coeff->a_z_half[k] * vxz;
						vxz = vxz / pml_coeff->K_y_half[k] + pml_wfd->psi_vxz[j][i][k];
						pml_wfd->psi_vyz[j][i][k] = pml_coeff->b_z_half[k] * pml_wfd->psi_vyz[j][i][k] + pml_coeff->a_z_half[k] * vyz;
						vyz = vyz / pml_coeff->K_y_half[k] + pml_wfd->psi_vyz[j][i][k];
						pml_wfd->psi_vzz[j][i][k] = pml_coeff->b_z[k] * pml_wfd->psi_vzz[j][i][k] + pml_coeff->a_z[k] * vzz;
						vzz = vzz / pml_coeff->K_y[k] + pml_wfd->psi_vzz[j][i][k];}

					if((POS[3]==NPROCZ-1) && (k>=nz2+1)){

						h1 = (k-nz2+FW);

						pml_wfd->psi_vxz[j][i][h1] = pml_coeff->b_z_half[h1] * pml_wfd->psi_vxz[j][i][h1] + pml_coeff->a_z_half[h1] * vxz;
						vxz = vxz / pml_coeff->K_z_half[h1] + pml_wfd->psi_vxz[j][i][h1];
						pml_wfd->psi_vyz[j][i][h1] = pml_coeff->b_z_half[h1] * pml_wfd->psi_vyz[j][i][h1] + pml_coeff->a_z_half[h1] * vyz;
						vyz = vyz / pml_coeff->K_z_half[h1] + pml_wfd->psi_vyz[j][i][h1];
						pml_wfd->psi_vzz[j][i][h1] = pml_coeff->b_z[h1] * pml_wfd->psi_vzz[j][i][h1] + pml_coeff->a_z[h1] * vzz;
						vzz = vzz / pml_coeff->K_z[h1] + pml_wfd->psi_vzz[j][i][h1];}




					fipjp=mod_av->uipjp[j][i][k]*DT;
					fjpkp=mod_av->ujpkp[j][i][k]*DT;
					fipkp=mod_av->uipkp[j][i][k]*DT;
					g=mod->pi[j][i][k];
					f=2.0*mod->u[j][i][k];

					vxyyx=vxy+vyx;
					vyzzy=vyz+vzy;
					vxzzx=vxz+vzx;
					vxxyyzz=vxx+vyy+vzz;
					vyyzz=vyy+vzz;
					vxxzz=vxx+vzz;
					vxxyy=vxx+vyy;

					stress->sxy[j][i][k]+=(fipjp*vxyyx);
					stress->syz[j][i][k]+=(fjpkp*vyzzy);
					stress->sxz[j][i][k]+=(fipkp*vxzzx);
					stress->sxx[j][i][k]+=DT*((g*vxxyyzz)-(f*vyyzz));
					stress->syy[j][i][k]+=DT*((g*vxxyyzz)-(f*vxxzz));
					stress->szz[j][i][k]+=DT*((g*vxxyyzz)-(f*vxxyy));

				}
			}
		}
	}

	if(POS[1]==NPROCX-1){
		for (j=1;j<=NY;j++){
			for (i=nx2+1;i<=nx2+FW;i++){
				for (k=1;k<=NZ;k++){

					vxx = (b1*(vel->vx[j][i][k]-vel->vx[j][i-1][k])+b2*(vel->vx[j][i+1][k]-vel->vx[j][i-2][k]))/DX;
					vxy = (b1*(vel->vx[j+1][i][k]-vel->vx[j][i][k])+b2*(vel->vx[j+2][i][k]-vel->vx[j-1][i][k]))/DY;
					vxz = (b1*(vel->vx[j][i][k+1]-vel->vx[j][i][k])+b2*(vel->vx[j][i][k+2]-vel->vx[j][i][k-1]))/DZ;
					vyx = (b1*(vel->vy[j][i+1][k]-vel->vy[j][i][k])+b2*(vel->vy[j][i+2][k]-vel->vy[j][i-1][k]))/DX;
					vyy = (b1*(vel->vy[j][i][k]-vel->vy[j-1][i][k])+b2*(vel->vy[j+1][i][k]-vel->vy[j-2][i][k]))/DY;
					vyz = (b1*(vel->vy[j][i][k+1]-vel->vy[j][i][k])+b2*(vel->vy[j][i][k+2]-vel->vy[j][i][k-1]))/DZ;
					vzx = (b1*(vel->vz[j][i+1][k]-vel->vz[j][i][k])+b2*(vel->vz[j][i+2][k]-vel->vz[j][i-1][k]))/DX;
					vzy = (b1*(vel->vz[j+1][i][k]-vel->vz[j][i][k])+b2*(vel->vz[j+2][i][k]-vel->vz[j-1][i][k]))/DY;
					vzz = (b1*(vel->vz[j][i][k]-vel->vz[j][i][k-1])+b2*(vel->vz[j][i][k+1]-vel->vz[j][i][k-2]))/DZ;

					h1 = i-nx2+FW;

					pml_wfd->psi_vxx[j][h1][k] = pml_coeff->b_x[h1] * pml_wfd->psi_vxx[j][h1][k] + pml_coeff->a_x[h1] * vxx;
					vxx = vxx /pml_coeff->K_x[h1] + pml_wfd->psi_vxx[j][h1][k];
					pml_wfd->psi_vyx[j][h1][k] = pml_coeff->b_x_half[h1] * pml_wfd->psi_vyx[j][h1][k] + pml_coeff->a_x_half[h1] * vyx;
					vyx = vyx  /pml_coeff->K_x_half[h1] + pml_wfd->psi_vyx[j][h1][k];
					pml_wfd->psi_vzx[j][h1][k] = pml_coeff->b_x_half[h1] * pml_wfd->psi_vzx[j][h1][k] + pml_coeff->a_x_half[h1] * vzx;
					vzx = vzx / pml_coeff->K_x_half[h1]  +pml_wfd->psi_vzx[j][h1][k];

					if((POS[2]==0 && FREE_SURF==0) && (j<=FW)){

						pml_wfd->psi_vxy[j][i][k] = pml_coeff->b_y_half[j] * pml_wfd->psi_vxy[j][i][k] + pml_coeff->a_y_half[j] * vxy;
						vxy = vxy / pml_coeff->K_y_half[j] + pml_wfd->psi_vxy[j][i][k];
						pml_wfd->psi_vyy[j][i][k] = pml_coeff->b_y[j] * pml_wfd->psi_vyy[j][i][k] + pml_coeff->a_y[j] * vyy;
						vyy = vyy / pml_coeff->K_y[j] + pml_wfd->psi_vyy[j][i][k];
						pml_wfd->psi_vzy[j][i][k] = pml_coeff->b_y_half[j] * pml_wfd->psi_vzy[j][i][k] + pml_coeff->a_y_half[j] * vzy;
						vzy = vzy / pml_coeff->K_y_half[j] + pml_wfd->psi_vzy[j][i][k]; }


					if((POS[2]==NPROCY-1) && (j>=ny2+1)){
						h1 = (j-ny2+FW);

						pml_wfd->psi_vxy[h1][i][k] = pml_coeff->b_y_half[h1] * pml_wfd->psi_vxy[h1][i][k] + pml_coeff->a_y_half[h1] * vxy;
						vxy = vxy / pml_coeff->K_y_half[h1] + pml_wfd->psi_vxy[h1][i][k];
						pml_wfd->psi_vyy[h1][i][k] = pml_coeff->b_y[h1] * pml_wfd->psi_vyy[h1][i][k] + pml_coeff->a_y[h1] * vyy;
						vyy = vyy / pml_coeff->K_y[h1] + pml_wfd->psi_vyy[h1][i][k];
						pml_wfd->psi_vzy[h1][i][k] = pml_coeff->b_y_half[h1] * pml_wfd->psi_vzy[h1][i][k] + pml_coeff->a_y_half[h1] * vzy;
						vzy = vzy / pml_coeff->K_y_half[h1] + pml_wfd->psi_vzy[h1][i][k]; }


					if((POS[3]==0) && (k<=FW)){

						pml_wfd->psi_vxz[j][i][k] = pml_coeff->b_z_half[k] * pml_wfd->psi_vxz[j][i][k] + pml_coeff->a_z_half[k] * vxz;
						vxz = vxz / pml_coeff->K_y_half[k] + pml_wfd->psi_vxz[j][i][k];
						pml_wfd->psi_vyz[j][i][k] = pml_coeff->b_z_half[k] * pml_wfd->psi_vyz[j][i][k] + pml_coeff->a_z_half[k] * vyz;
						vyz = vyz / pml_coeff->K_y_half[k] + pml_wfd->psi_vyz[j][i][k];
						pml_wfd->psi_vzz[j][i][k] = pml_coeff->b_z[k] * pml_wfd->psi_vzz[j][i][k] + pml_coeff->a_z[k] * vzz;
						vzz = vzz / pml_coeff->K_y[k] + pml_wfd->psi_vzz[j][i][k];}


					if((POS[3]==NPROCZ-1) && (k>=nz2+1)){

						h1 = (k-nz2+FW);

						pml_wfd->psi_vxz[j][i][h1] = pml_coeff->b_z_half[h1] * pml_wfd->psi_vxz[j][i][h1] + pml_coeff->a_z_half[h1] * vxz;
						vxz = vxz / pml_coeff->K_z_half[h1] + pml_wfd->psi_vxz[j][i][h1];
						pml_wfd->psi_vyz[j][i][h1] = pml_coeff->b_z_half[h1] * pml_wfd->psi_vyz[j][i][h1] + pml_coeff->a_z_half[h1] * vyz;
						vyz = vyz / pml_coeff->K_z_half[h1] + pml_wfd->psi_vyz[j][i][h1];
						pml_wfd->psi_vzz[j][i][h1] = pml_coeff->b_z[h1] * pml_wfd->psi_vzz[j][i][h1] + pml_coeff->a_z[h1] * vzz;
						vzz = vzz / pml_coeff->K_z[h1] + pml_wfd->psi_vzz[j][i][h1];}




					fipjp=mod_av->uipjp[j][i][k]*DT;
					fjpkp=mod_av->ujpkp[j][i][k]*DT;
					fipkp=mod_av->uipkp[j][i][k]*DT;
					g=mod->pi[j][i][k];
					f=2.0*mod->u[j][i][k];

					vxyyx=vxy+vyx;
					vyzzy=vyz+vzy;
					vxzzx=vxz+vzx;
					vxxyyzz=vxx+vyy+vzz;
					vyyzz=vyy+vzz;
					vxxzz=vxx+vzz;
					vxxyy=vxx+vyy;

					stress->sxy[j][i][k]+=(fipjp*vxyyx);
					stress->syz[j][i][k]+=(fjpkp*vyzzy);
					stress->sxz[j][i][k]+=(fipkp*vxzzx);
					stress->sxx[j][i][k]+=DT*((g*vxxyyzz)-(f*vyyzz));
					stress->syy[j][i][k]+=DT*((g*vxxyyzz)-(f*vxxzz));
					stress->szz[j][i][k]+=DT*((g*vxxyyzz)-(f*vxxyy));


				}
			}
		}
	}

	if((POS[2]==0 && FREE_SURF==0)){
		for (j=1;j<=FW;j++){
			for (i=nx1;i<=nx2;i++){
				for (k=1;k<=NZ;k++){

					vxx = (b1*(vel->vx[j][i][k]-vel->vx[j][i-1][k])+b2*(vel->vx[j][i+1][k]-vel->vx[j][i-2][k]))/DX;
					vxy = (b1*(vel->vx[j+1][i][k]-vel->vx[j][i][k])+b2*(vel->vx[j+2][i][k]-vel->vx[j-1][i][k]))/DY;
					vxz = (b1*(vel->vx[j][i][k+1]-vel->vx[j][i][k])+b2*(vel->vx[j][i][k+2]-vel->vx[j][i][k-1]))/DZ;
					vyx = (b1*(vel->vy[j][i+1][k]-vel->vy[j][i][k])+b2*(vel->vy[j][i+2][k]-vel->vy[j][i-1][k]))/DX;
					vyy = (b1*(vel->vy[j][i][k]-vel->vy[j-1][i][k])+b2*(vel->vy[j+1][i][k]-vel->vy[j-2][i][k]))/DY;
					vyz = (b1*(vel->vy[j][i][k+1]-vel->vy[j][i][k])+b2*(vel->vy[j][i][k+2]-vel->vy[j][i][k-1]))/DZ;
					vzx = (b1*(vel->vz[j][i+1][k]-vel->vz[j][i][k])+b2*(vel->vz[j][i+2][k]-vel->vz[j][i-1][k]))/DX;
					vzy = (b1*(vel->vz[j+1][i][k]-vel->vz[j][i][k])+b2*(vel->vz[j+2][i][k]-vel->vz[j-1][i][k]))/DY;
					vzz = (b1*(vel->vz[j][i][k]-vel->vz[j][i][k-1])+b2*(vel->vz[j][i][k+1]-vel->vz[j][i][k-2]))/DZ;

					pml_wfd->psi_vxy[j][i][k] = pml_coeff->b_y_half[j] * pml_wfd->psi_vxy[j][i][k] + pml_coeff->a_y_half[j] * vxy;
					vxy = vxy / pml_coeff->K_y_half[j] + pml_wfd->psi_vxy[j][i][k];
					pml_wfd->psi_vyy[j][i][k] = pml_coeff->b_y[j] * pml_wfd->psi_vyy[j][i][k] + pml_coeff->a_y[j] * vyy;
					vyy = vyy / pml_coeff->K_y[j] + pml_wfd->psi_vyy[j][i][k];
					pml_wfd->psi_vzy[j][i][k] = pml_coeff->b_y_half[j] * pml_wfd->psi_vzy[j][i][k] + pml_coeff->a_y_half[j] * vzy;
					vzy = vzy / pml_coeff->K_y_half[j] + pml_wfd->psi_vzy[j][i][k];

					if((POS[3]==0) && (k<=FW)){

						pml_wfd->psi_vxz[j][i][k] = pml_coeff->b_z_half[k] * pml_wfd->psi_vxz[j][i][k] + pml_coeff->a_z_half[k] * vxz;
						vxz = vxz / pml_coeff->K_y_half[k] + pml_wfd->psi_vxz[j][i][k];
						pml_wfd->psi_vyz[j][i][k] = pml_coeff->b_z_half[k] * pml_wfd->psi_vyz[j][i][k] + pml_coeff->a_z_half[k] * vyz;
						vyz = vyz / pml_coeff->K_y_half[k] + pml_wfd->psi_vyz[j][i][k];
						pml_wfd->psi_vzz[j][i][k] = pml_coeff->b_z[k] * pml_wfd->psi_vzz[j][i][k] + pml_coeff->a_z[k] * vzz;
						vzz = vzz / pml_coeff->K_y[k] + pml_wfd->psi_vzz[j][i][k];}


					if((POS[3]==NPROCZ-1) && (k>=nz2+1)){

						h1 = (k-nz2+FW);

						pml_wfd->psi_vxz[j][i][h1] = pml_coeff->b_z_half[h1] * pml_wfd->psi_vxz[j][i][h1] + pml_coeff->a_z_half[h1] * vxz;
						vxz = vxz / pml_coeff->K_z_half[h1] + pml_wfd->psi_vxz[j][i][h1];
						pml_wfd->psi_vyz[j][i][h1] = pml_coeff->b_z_half[h1] * pml_wfd->psi_vyz[j][i][h1] + pml_coeff->a_z_half[h1] * vyz;
						vyz = vyz / pml_coeff->K_z_half[h1] + pml_wfd->psi_vyz[j][i][h1];
						pml_wfd->psi_vzz[j][i][h1] = pml_coeff->b_z[h1] * pml_wfd->psi_vzz[j][i][h1] + pml_coeff->a_z[h1] * vzz;
						vzz = vzz / pml_coeff->K_z[h1] + pml_wfd->psi_vzz[j][i][h1];}


					fipjp=mod_av->uipjp[j][i][k]*DT;
					fjpkp=mod_av->ujpkp[j][i][k]*DT;
					fipkp=mod_av->uipkp[j][i][k]*DT;
					g=mod->pi[j][i][k];
					f=2.0*mod->u[j][i][k];

					vxyyx=vxy+vyx;
					vyzzy=vyz+vzy;
					vxzzx=vxz+vzx;
					vxxyyzz=vxx+vyy+vzz;
					vyyzz=vyy+vzz;
					vxxzz=vxx+vzz;
					vxxyy=vxx+vyy;

					stress->sxy[j][i][k]+=(fipjp*vxyyx);
					stress->syz[j][i][k]+=(fjpkp*vyzzy);
					stress->sxz[j][i][k]+=(fipkp*vxzzx);
					stress->sxx[j][i][k]+=DT*((g*vxxyyzz)-(f*vyyzz));
					stress->syy[j][i][k]+=DT*((g*vxxyyzz)-(f*vxxzz));
					stress->szz[j][i][k]+=DT*((g*vxxyyzz)-(f*vxxyy));


				}
			}
		}
	}

	if(POS[2]==NPROCY-1){
		for (j=ny2+1;j<=ny2+FW;j++){
			for (i=nx1;i<=nx2;i++){
				for (k=1;k<=NZ;k++){

					vxx = (b1*(vel->vx[j][i][k]-vel->vx[j][i-1][k])+b2*(vel->vx[j][i+1][k]-vel->vx[j][i-2][k]))/DX;
					vxy = (b1*(vel->vx[j+1][i][k]-vel->vx[j][i][k])+b2*(vel->vx[j+2][i][k]-vel->vx[j-1][i][k]))/DY;
					vxz = (b1*(vel->vx[j][i][k+1]-vel->vx[j][i][k])+b2*(vel->vx[j][i][k+2]-vel->vx[j][i][k-1]))/DZ;
					vyx = (b1*(vel->vy[j][i+1][k]-vel->vy[j][i][k])+b2*(vel->vy[j][i+2][k]-vel->vy[j][i-1][k]))/DX;
					vyy = (b1*(vel->vy[j][i][k]-vel->vy[j-1][i][k])+b2*(vel->vy[j+1][i][k]-vel->vy[j-2][i][k]))/DY;
					vyz = (b1*(vel->vy[j][i][k+1]-vel->vy[j][i][k])+b2*(vel->vy[j][i][k+2]-vel->vy[j][i][k-1]))/DZ;
					vzx = (b1*(vel->vz[j][i+1][k]-vel->vz[j][i][k])+b2*(vel->vz[j][i+2][k]-vel->vz[j][i-1][k]))/DX;
					vzy = (b1*(vel->vz[j+1][i][k]-vel->vz[j][i][k])+b2*(vel->vz[j+2][i][k]-vel->vz[j-1][i][k]))/DY;
					vzz = (b1*(vel->vz[j][i][k]-vel->vz[j][i][k-1])+b2*(vel->vz[j][i][k+1]-vel->vz[j][i][k-2]))/DZ;

					h1 = (j-ny2+FW);

					pml_wfd->psi_vxy[h1][i][k] = pml_coeff->b_y_half[h1] * pml_wfd->psi_vxy[h1][i][k] + pml_coeff->a_y_half[h1] * vxy;
					vxy = vxy / pml_coeff->K_y_half[h1] + pml_wfd->psi_vxy[h1][i][k];
					pml_wfd->psi_vyy[h1][i][k] = pml_coeff->b_y[h1] * pml_wfd->psi_vyy[h1][i][k] + pml_coeff->a_y[h1] * vyy;
					vyy = vyy / pml_coeff->K_y[h1] + pml_wfd->psi_vyy[h1][i][k];
					pml_wfd->psi_vzy[h1][i][k] = pml_coeff->b_y_half[h1] * pml_wfd->psi_vzy[h1][i][k] + pml_coeff->a_y_half[h1] * vzy;
					vzy = vzy / pml_coeff->K_y_half[h1] + pml_wfd->psi_vzy[h1][i][k];

					if((POS[3]==0) && (k<=FW)){

						pml_wfd->psi_vxz[j][i][k] = pml_coeff->b_z_half[k] * pml_wfd->psi_vxz[j][i][k] + pml_coeff->a_z_half[k] * vxz;
						vxz = vxz / pml_coeff->K_y_half[k] + pml_wfd->psi_vxz[j][i][k];
						pml_wfd->psi_vyz[j][i][k] = pml_coeff->b_z_half[k] * pml_wfd->psi_vyz[j][i][k] + pml_coeff->a_z_half[k] * vyz;
						vyz = vyz / pml_coeff->K_y_half[k] + pml_wfd->psi_vyz[j][i][k];
						pml_wfd->psi_vzz[j][i][k] = pml_coeff->b_z[k] * pml_wfd->psi_vzz[j][i][k] + pml_coeff->a_z[k] * vzz;
						vzz = vzz / pml_coeff->K_y[k] + pml_wfd->psi_vzz[j][i][k];}


					if((POS[3]==NPROCZ-1) && (k>=nz2+1)){

						h1 = (k-nz2+FW);

						pml_wfd->psi_vxz[j][i][h1] = pml_coeff->b_z_half[h1] * pml_wfd->psi_vxz[j][i][h1] + pml_coeff->a_z_half[h1] * vxz;
						vxz = vxz / pml_coeff->K_z_half[h1] + pml_wfd->psi_vxz[j][i][h1];
						pml_wfd->psi_vyz[j][i][h1] = pml_coeff->b_z_half[h1] * pml_wfd->psi_vyz[j][i][h1] + pml_coeff->a_z_half[h1] * vyz;
						vyz = vyz / pml_coeff->K_z_half[h1] + pml_wfd->psi_vyz[j][i][h1];
						pml_wfd->psi_vzz[j][i][h1] = pml_coeff->b_z[h1] * pml_wfd->psi_vzz[j][i][h1] + pml_coeff->a_z[h1] * vzz;
						vzz = vzz / pml_coeff->K_z[h1] + pml_wfd->psi_vzz[j][i][h1];}


					fipjp=mod_av->uipjp[j][i][k]*DT;
					fjpkp=mod_av->ujpkp[j][i][k]*DT;
					fipkp=mod_av->uipkp[j][i][k]*DT;
					g=mod->pi[j][i][k];
					f=2.0*mod->u[j][i][k];

					vxyyx=vxy+vyx;
					vyzzy=vyz+vzy;
					vxzzx=vxz+vzx;
					vxxyyzz=vxx+vyy+vzz;
					vyyzz=vyy+vzz;
					vxxzz=vxx+vzz;
					vxxyy=vxx+vyy;

					stress->sxy[j][i][k]+=(fipjp*vxyyx);
					stress->syz[j][i][k]+=(fjpkp*vyzzy);
					stress->sxz[j][i][k]+=(fipkp*vxzzx);
					stress->sxx[j][i][k]+=DT*((g*vxxyyzz)-(f*vyyzz));
					stress->syy[j][i][k]+=DT*((g*vxxyyzz)-(f*vxxzz));
					stress->szz[j][i][k]+=DT*((g*vxxyyzz)-(f*vxxyy));


				}
			}
		}
	}


	if(POS[3]==0){
		for (j=ny1;j<=ny2;j++){
			for (i=nx1;i<=nx2;i++){
				for (k=1;k<=FW;k++){

					vxx = (b1*(vel->vx[j][i][k]-vel->vx[j][i-1][k])+b2*(vel->vx[j][i+1][k]-vel->vx[j][i-2][k]))/DX;
					vxy = (b1*(vel->vx[j+1][i][k]-vel->vx[j][i][k])+b2*(vel->vx[j+2][i][k]-vel->vx[j-1][i][k]))/DY;
					vxz = (b1*(vel->vx[j][i][k+1]-vel->vx[j][i][k])+b2*(vel->vx[j][i][k+2]-vel->vx[j][i][k-1]))/DZ;
					vyx = (b1*(vel->vy[j][i+1][k]-vel->vy[j][i][k])+b2*(vel->vy[j][i+2][k]-vel->vy[j][i-1][k]))/DX;
					vyy = (b1*(vel->vy[j][i][k]-vel->vy[j-1][i][k])+b2*(vel->vy[j+1][i][k]-vel->vy[j-2][i][k]))/DY;
					vyz = (b1*(vel->vy[j][i][k+1]-vel->vy[j][i][k])+b2*(vel->vy[j][i][k+2]-vel->vy[j][i][k-1]))/DZ;
					vzx = (b1*(vel->vz[j][i+1][k]-vel->vz[j][i][k])+b2*(vel->vz[j][i+2][k]-vel->vz[j][i-1][k]))/DX;
					vzy = (b1*(vel->vz[j+1][i][k]-vel->vz[j][i][k])+b2*(vel->vz[j+2][i][k]-vel->vz[j-1][i][k]))/DY;
					vzz = (b1*(vel->vz[j][i][k]-vel->vz[j][i][k-1])+b2*(vel->vz[j][i][k+1]-vel->vz[j][i][k-2]))/DZ;

					pml_wfd->psi_vxz[j][i][k] = pml_coeff->b_z_half[k] * pml_wfd->psi_vxz[j][i][k] + pml_coeff->a_z_half[k] * vxz;
					vxz = vxz / pml_coeff->K_y_half[k] + pml_wfd->psi_vxz[j][i][k];
					pml_wfd->psi_vyz[j][i][k] = pml_coeff->b_z_half[k] * pml_wfd->psi_vyz[j][i][k] + pml_coeff->a_z_half[k] * vyz;
					vyz = vyz / pml_coeff->K_y_half[k] + pml_wfd->psi_vyz[j][i][k];
					pml_wfd->psi_vzz[j][i][k] = pml_coeff->b_z[k] * pml_wfd->psi_vzz[j][i][k] + pml_coeff->a_z[k] * vzz;
					vzz = vzz / pml_coeff->K_y[k] + pml_wfd->psi_vzz[j][i][k];



					fipjp=mod_av->uipjp[j][i][k]*DT;
					fjpkp=mod_av->ujpkp[j][i][k]*DT;
					fipkp=mod_av->uipkp[j][i][k]*DT;
					g=mod->pi[j][i][k];
					f=2.0*mod->u[j][i][k];

					vxyyx=vxy+vyx;
					vyzzy=vyz+vzy;
					vxzzx=vxz+vzx;
					vxxyyzz=vxx+vyy+vzz;
					vyyzz=vyy+vzz;
					vxxzz=vxx+vzz;
					vxxyy=vxx+vyy;

					stress->sxy[j][i][k]+=(fipjp*vxyyx);
					stress->syz[j][i][k]+=(fjpkp*vyzzy);
					stress->sxz[j][i][k]+=(fipkp*vxzzx);
					stress->sxx[j][i][k]+=DT*((g*vxxyyzz)-(f*vyyzz));
					stress->syy[j][i][k]+=DT*((g*vxxyyzz)-(f*vxxzz));
					stress->szz[j][i][k]+=DT*((g*vxxyyzz)-(f*vxxyy));

				}
			}
		}
	}


	if(POS[3]==NPROCZ-1){		
		for (j=ny1;j<=ny2;j++){
			for (i=nx1;i<=nx2;i++){
				for (k=nz2+1;k<=nz2+FW;k++){

					vxx = (b1*(vel->vx[j][i][k]-vel->vx[j][i-1][k])+b2*(vel->vx[j][i+1][k]-vel->vx[j][i-2][k]))/DX;
					vxy = (b1*(vel->vx[j+1][i][k]-vel->vx[j][i][k])+b2*(vel->vx[j+2][i][k]-vel->vx[j-1][i][k]))/DY;
					vxz = (b1*(vel->vx[j][i][k+1]-vel->vx[j][i][k])+b2*(vel->vx[j][i][k+2]-vel->vx[j][i][k-1]))/DZ;
					vyx = (b1*(vel->vy[j][i+1][k]-vel->vy[j][i][k])+b2*(vel->vy[j][i+2][k]-vel->vy[j][i-1][k]))/DX;
					vyy = (b1*(vel->vy[j][i][k]-vel->vy[j-1][i][k])+b2*(vel->vy[j+1][i][k]-vel->vy[j-2][i][k]))/DY;
					vyz = (b1*(vel->vy[j][i][k+1]-vel->vy[j][i][k])+b2*(vel->vy[j][i][k+2]-vel->vy[j][i][k-1]))/DZ;
					vzx = (b1*(vel->vz[j][i+1][k]-vel->vz[j][i][k])+b2*(vel->vz[j][i+2][k]-vel->vz[j][i-1][k]))/DX;
					vzy = (b1*(vel->vz[j+1][i][k]-vel->vz[j][i][k])+b2*(vel->vz[j+2][i][k]-vel->vz[j-1][i][k]))/DY;
					vzz = (b1*(vel->vz[j][i][k]-vel->vz[j][i][k-1])+b2*(vel->vz[j][i][k+1]-vel->vz[j][i][k-2]))/DZ;

					h1 = (k-nz2+FW);

					pml_wfd->psi_vxz[j][i][h1] = pml_coeff->b_z_half[h1] * pml_wfd->psi_vxz[j][i][h1] + pml_coeff->a_z_half[h1] * vxz;
					vxz = vxz / pml_coeff->K_z_half[h1] + pml_wfd->psi_vxz[j][i][h1];
					pml_wfd->psi_vyz[j][i][h1] = pml_coeff->b_z_half[h1] * pml_wfd->psi_vyz[j][i][h1] + pml_coeff->a_z_half[h1] * vyz;
					vyz = vyz / pml_coeff->K_z_half[h1] + pml_wfd->psi_vyz[j][i][h1];
					pml_wfd->psi_vzz[j][i][h1] = pml_coeff->b_z[h1] * pml_wfd->psi_vzz[j][i][h1] + pml_coeff->a_z[h1] * vzz;
					vzz = vzz / pml_coeff->K_z[h1] + pml_wfd->psi_vzz[j][i][h1];





					fipjp=mod_av->uipjp[j][i][k]*DT;
					fjpkp=mod_av->ujpkp[j][i][k]*DT;
					fipkp=mod_av->uipkp[j][i][k]*DT;
					g=mod->pi[j][i][k];
					f=2.0*mod->u[j][i][k];

					vxyyx=vxy+vyx;
					vyzzy=vyz+vzy;
					vxzzx=vxz+vzx;
					vxxyyzz=vxx+vyy+vzz;
					vyyzz=vyy+vzz;
					vxxzz=vxx+vzz;
					vxxyy=vxx+vyy;

					stress->sxy[j][i][k]+=(fipjp*vxyyx);
					stress->syz[j][i][k]+=(fjpkp*vyzzy);
					stress->sxz[j][i][k]+=(fipkp*vxzzx);
					stress->sxx[j][i][k]+=DT*((g*vxxyyzz)-(f*vyyzz));
					stress->syy[j][i][k]+=DT*((g*vxxyyzz)-(f*vxxzz));
					stress->szz[j][i][k]+=DT*((g*vxxyyzz)-(f*vxxyy));

				}
			}
		}
	}


	/*if (LOG)
		if (MYID==0){
			time2=MPI_Wtime();
			time=time2-time1;
			fprintf(FP," Real time for CPML stress tensor update: \t %4.2f s.\n",time);
		}*/
	return time;

}
