/*------------------------------------------------------------------------
 * Copyright (C) 2015 For the list of authors, see file AUTHORS.
 *
 * This file is part of IFOS3D.
 * 
 * IFOS3D is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 2.0 of the License only.
 * 
 * IFOS3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with IFOS3D. See file COPYING and/or 
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
--------------------------------------------------------------------------*/

/* -----------------------------------------------------------------------
 * Initialise wavefield with zero
 -------------------------------------------------------------------------*/

#include "fd.h"
void zero_wavefield( int NX, int NY, int NZ, st_velocity *vel,st_stress *stress,  st_visc_mem *mem, st_pml_wfd *pml_wfd){

	
    extern int FDORDER, ABS_TYPE, FW, POS[4],L;
    int nx1, ny1, nz1, nx2, ny2, nz2, a,b,l, i, j, k;

    l=1;
    if(ABS_TYPE==1 && FDORDER==2){l=2;}
	
	if(POS[2]==0){
	  a=0;
	  b=1;}
	else{
	  a=1;
	  b=l;}
	

    ny1=a-b*FDORDER/2;
    ny2=NY+l*FDORDER/2;
    nx1=1-l*FDORDER/2;
    nx2=NX+l*FDORDER/2;
    nz1=1-l*FDORDER/2;
    nz2=NZ+l*FDORDER/2;

	  

    for (j=ny1;j<=ny2;j++){
      for (i=nx1;i<=nx2;i++){
	for (k=nz1;k<=nz2;k++){
	  vel->vx[j][i][k]=0.0;
	  vel->vy[j][i][k]=0.0;
	  vel->vz[j][i][k]=0.0;
	  stress->sxy[j][i][k]=0.0;
	  stress->syz[j][i][k]=0.0;
	}
      }
    }	
	
    ny1=1-l*FDORDER/2;
   
    for (j=ny1;j<=ny2;j++){
      for (i=nx1;i<=nx2;i++){
	for (k=nz1;k<=nz2;k++){	  
	  stress->sxx[j][i][k]=0.0;
	  stress->sxz[j][i][k]=0.0;
	  stress->syy[j][i][k]=0.0;
	  stress->szz[j][i][k]=0.0;
	}
      }
    }		
	
	
	
	
    if(L){	
	for (j=1;j<=NY;j++){
	  for (i=1;i<=NX;i++){
	    for (k=1;k<=NZ;k++){
	      mem->rxx[j][i][k]=0.0;
	      mem->rxy[j][i][k]=0.0;
	      mem->rxz[j][i][k]=0.0;
	      mem->ryy[j][i][k]=0.0;
	      mem->ryz[j][i][k]=0.0;
	      mem->rzz[j][i][k]=0.0;
	    }
	  }
	}
    }
    
    if(ABS_TYPE==1){
    for (j=1;j<=NY;j++){
      for (i=1;i<=NX;i++){
	for (k=1;k<=2*FW;k++){
	 pml_wfd->psi_sxz_z[j][i][k]=0.0;
	 pml_wfd->psi_syz_z[j][i][k]=0.0;
	 pml_wfd->psi_szz_z[j][i][k]=0.0;
	 pml_wfd->psi_vxz[j][i][k]=0.0;
	 pml_wfd->psi_vyz[j][i][k]=0.0;
	 pml_wfd->psi_vzz[j][i][k]=0.0;
	}
      }
    }

     for (j=1;j<=NY;j++){
      for (i=1;i<=2*FW;i++){
	for (k=1;k<=NZ;k++){
	 pml_wfd->psi_sxx_x[j][i][k]=0.0;
	 pml_wfd->psi_sxy_x[j][i][k]=0.0;
	 pml_wfd->psi_sxz_x[j][i][k]=0.0;
	 pml_wfd->psi_vxx[j][i][k]=0.0;
	 pml_wfd->psi_vyx[j][i][k]=0.0;
	 pml_wfd->psi_vzx[j][i][k]=0.0;
	}
      }
    }   

for (j=1;j<=2*FW;j++){
      for (i=1;i<=NX;i++){
	for (k=1;k<=NZ;k++){
	 pml_wfd->psi_sxy_y[j][i][k]=0.0;
	 pml_wfd->psi_syy_y[j][i][k]=0.0;
	 pml_wfd->psi_syz_y[j][i][k]=0.0;
	 pml_wfd->psi_vxy[j][i][k]=0.0;
	 pml_wfd->psi_vyy[j][i][k]=0.0;
	 pml_wfd->psi_vzy[j][i][k]=0.0;
	}
      }
    }   
}
}